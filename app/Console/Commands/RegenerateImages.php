<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;

class RegenerateImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:regen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerates the product images using the original image';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('max_execution_time', 3600);

        $products = Product::whereHas('images')->get();
        foreach ($products as $product) {
            $product->regenerateImages();
        }
    }
}
