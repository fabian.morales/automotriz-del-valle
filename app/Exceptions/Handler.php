<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\UnauthorizedException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

	public function render($request, \Throwable $exception)
    {
        if ($exception instanceof UnauthorizedException) {
            return \Illuminate\Support\Facades\Redirect::back()
                ->withErrors(['message_error' => $exception->getMessage()])
                ->with(['message_error' => $exception->getMessage()]);
        }

        return parent::render($request, $exception);
    }
}
