<?php 

namespace App\Helpers;

use Illuminate\Support\Facades\Request;

class CaptchaHelper{
    public static function renderJS() {
        $siteKey = config('custom.google.sitekey');
        return view('captcha.js', compact('siteKey'));
    }

    public static function renderButton() {
        $siteKey = config('custom.google.sitekey');
        return view('captcha.html', compact('siteKey'));
    }
}
