<?php 

namespace App\Helpers;

use App\Models\Cart;
use Illuminate\Support\Facades\Request;

class CartHelper{
    public static function getKey() {
        return 'cart.key';
    }

    public static function calculateTotalCount() {
        $count = 0;
        $cartKey = session(self::getKey());
        if (!empty($cartKey)) {
            $count = Cart::where('key', $cartKey)->sum('quantity');
        }

        return $count;
    }

    public static function getTotalCount() {
        return (int)session('cart.count', 0);
    }

    public static function saveTotalCount($count) {
        session(['cart.count' => $count]);
    }
}
