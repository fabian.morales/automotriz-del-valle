<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Attribute;
use Illuminate\Support\Facades\Validator;
use App\Models\AttributeValue;

class AttributeController extends Controller
{
    public function list() {
        $attributes = Attribute::all();
        return view("backoffice.attribute.list", ["attributes" => $attributes]);
    }

    public function showForm(Attribute $attribute) {
        if (empty($attribute)) {
            $attribute = new Attribute();
        }

        return view("backoffice.attribute.form", ["attribute" => $attribute]);
    }

    public function edit($id) {
        $attribute = Attribute::find($id);
        return $this->showForm($attribute);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $id = $request->get('id');
        $attribute = Attribute::findOrNew($id);

        $attribute->fill($request->all());
        if ($attribute->save()) {
            return redirect()
                ->route('admin::attribute::list')
                ->with(['message_success' => 'Atributo guardado exitosamente']);
        }
        else{
            return back()
                ->with(['message_error' => 'No se pudo guardar el atributo']);
        }
    }

    public function delete($id) {
        $attribute = Attribute::find($id);
        if (empty($attribute) || $attribute == null) {
            return back()
                ->with(['message_error' => 'El atributo solicitado no existe']);
        }

        $attribute->values()->delete();

        /**
         * TO-DO: borrado de los valores de atributos asociados a productos
         */
        //$atributo->valoresProductos()->detach();

        if ($attribute->delete()) {
            return redirect()
                ->route('admin::attribute::list')
                ->with(['message_success' => 'Atributo borrado exitosamente']);
        }
        else{
            return back()
                ->with(['message_error' => 'No se pudo borrar el atributo']);
        }
    }

    public function showValue($id) {
        $value = AttributeValue::find($id);
        $ret = ['ok' => 0];
        if (!empty($value) && $value != null) {
            $ret = ['ok' => 1, 'data' => $value];
        }

        return(json_encode($ret));
    }

    public function getValuesList($id) {
        $values = AttributeValue::where('attribute_id', $id)->get();
        $ret = "";
        if (sizeof($values)) {
            $ret = $values->toJson();
        }

        return $ret;
    }

    public function saveValue(Request $request) {
        $id = $request->get('id');
        $value = AttributeValue::findOrNew($id);
        $value->fill($request->all());

        $ret = ['ok' => 0];

        if ($value->save()) {
            $values = AttributeValue::where('attribute_id', $value->attribute_id)->get();
            $ret = [
                'ok' => 1,
                'data' => $values,
            ];
        }

        return json_encode($ret);
    }

    public function deleteValue($id) {
        $value = AttributeValue::find($id);
        $ret = ['ok' => 0];

        if (empty($value) || $value == null) {
            return json_encode($ret);
        }

        $attributeId = $value->attribute_id;

        /**
         * TO-DO
         * borrar valores de atributo por producto
         */

        if ($value->delete()) {
            $values = AttributeValue::where('attribute_id', $attributeId)->get();
            $ret = [
                'ok' => 1,
                'data' => $values
            ];
        }

        return json_encode($ret);
    }
}
