<?php

namespace App\Http\Controllers\Admin;

use App\Models\Bulletin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BulletinController extends Controller
{
    public function list() {
        $items = Bulletin::all();
        return view("backoffice.bulletin.list", ["items" => $items]);
    }

    public function downloadList() {
        $items = Bulletin::all();

        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=boletines_" . date('Ymd_His_') . "_export_data.csv",
            "Pragma" => "no-cache",
            "Content-Encoding" => "UTF-8",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];
        $columnHeaders = ['ID', 'Correo', 'Fecha'];
        $callback = function() use ($items, $columnHeaders)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columnHeaders);
            foreach($items as $bulletin) {        
                $data = [$bulletin->id, $bulletin->email, $bulletin->created_at];
                fputcsv($file, $data);
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
