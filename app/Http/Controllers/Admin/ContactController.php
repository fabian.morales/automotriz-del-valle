<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contact;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function list() {
        $contacts = Contact::all();
        return view("backoffice.contact.list", ["contacts" => $contacts]);
    }

    public function downloadList() {
        $contacts = Contact::all();

        $headers = [
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=contacto_" . date('Ymd_His_') . "_export_data.csv",
            "Pragma" => "no-cache",
            "Content-Encoding" => "UTF-8",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ];
        $columnHeaders = ['ID', 'Nombre', 'Correo', 'Asunto', 'Mensaje', 'Fecha'];
        $callback = function() use ($contacts, $columnHeaders)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columnHeaders);
            foreach($contacts as $contact) {        
                $data = [$contact->id, $contact->name, $contact->email, $contact->subject, $contact->message, $contact->created_at];
                array_walk($data, function(&$value, $key){
                    $value = iconv('UTF-8', 'Windows-1252', $value);
                });
                fputcsv($file, $data);
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
