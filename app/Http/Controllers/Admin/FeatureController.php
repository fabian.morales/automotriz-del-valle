<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Feature;
use Illuminate\Support\Facades\Validator;

class FeatureController extends Controller
{
    public function list() {
        $features = Feature::all();
        return view("backoffice.feature.list", ["features" => $features]);
    }

    public function showForm(Feature $feature) {
        if (empty($feature)) {
            $feature = new Feature();
        }

        return view("backoffice.feature.form", ["feature" => $feature]);
    }

    public function edit($id) {
        $feature = Feature::find($id);
        return $this->showForm($feature);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $id = $request->get('id');
        $feature = Feature::findOrNew($id);

        $feature->fill($request->all());
        if ($feature->save()) {
            return redirect()
                ->route('admin::feature::list')
                ->with(['message_success' => 'Característica guardada exitosamente']);
        }
        else{
            return back()
                ->with(['message_error' => 'No se pudo guardar la característica']);
        }
    }

    public function delete($id) {
        $feature = Feature::find($id);
        if (empty($feature) || $feature == null) {
            return back()
                ->with(['message_error' => 'La característica solicitada no existe']);
        }

        /**
         * TO-DO: borrado de los valores de características asociados a productos
         */
        //$caracteristica->valoresProductos()->detach();

        if ($feature->delete()) {
            return redirect()
                ->route('admin::feature::list')
                ->with(['message_success' => 'Característica borrada exitosamente']);
        }
        else{
            return back()
                ->with(['message_error' => 'No se pudo borrar la característica']);
        }
    }
}
