<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use \Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\Request;
use App\Models\Gallery;
use App\Helpers\LocaleHelper;
use App\Models\GalleryPhoto;

class GalleryController extends Controller {

    public function list() {
        $galleries = Gallery::orderBy("id")->get();
        
        return view('backoffice.gallery.list', array("galleries" => $galleries));
    }

    public function showForm(Gallery $gallery) {
        if (empty($gallery)) {
            $gallery = new Gallery();
        }

        return view("backoffice.gallery.form", ["gallery" => $gallery]);
    }

    public function edit($id) {
        $gallery = Gallery::with("photos")->where("id", $id)->first();
        if (empty($gallery)) {
            return back()
                ->with(['message_error' => 'No se pudo encontrar la galería']);
        }

        return $this->showForm($gallery);
    }
    
    public function save(Request $request) {
        $id = $request->get("id");

        $gallery = Gallery::find($id);
        if (empty($gallery)) {
            $gallery = new Gallery();
        }

        $gallery->fill($request->all());
        
        if (empty($gallery->title)){
            return back()->with(["message_error" => "Debe ingresar el nombre de la galería"]);
        }

        $gallery->slug = LocaleHelper::hyphenize($gallery->title, false);

        if ($gallery->save()) {
            if ($request->has('save_stay')) {
                return redirect()
                    ->route('admin::gallery::edit', ["id" => $gallery->id])
                    ->with(["message_success" => "Galería guardada exitosamente"]);
            }
            else{
                return redirect()
                    ->route('admin::gallery::list', ["id" => $gallery->id])
                    ->with(["message_success" => "Galería guardada exitosamente"]);
            }
        } 
        else {
            return back()->with(["message_error" => "No se pudo guardar la galería"]);
        }
    }
    
    function delete($id){
        $gallery = Gallery::with("photos")->where("id", $id)->first();
        
        if (empty($gallery)){
            return back()->with(["message_error" => "Galería no encontrada"]);
        }
        
        $photos = $gallery->photos();
        
        if (!$gallery->photos()->delete()){
            return back()->with(["message_error" => "No se pudo borrar las fotos de la galería"]);
        }
        
        if ($gallery->delete()){
            foreach($photos as $photo) {
                $path = public_path('storage/images/galleries/' . $photo->gallery_id . '/' . $photo->file);
                @unlink($path);
            }
            
            @rmdir(public_path('storage/images/galleries/' . $gallery->id));
            
            return redirect()->route('admin::gallery::list')->with(["message_success" => "Galería borrada exitosamente"]);
        }
        else{
            return back()->with(["message_error" => "No se pudo borrar la galería"]);
        }
    }
    
    public function editPhoto($id){
        $photo = GalleryPhoto::find($id);
        if (empty($photo)) {
            $photo = new GalleryPhoto();
        }
        
        return $photo->toJson();
    }

    public function savePhoto(Request $request) {
        $id = $request->get("id");

        $photo = GalleryPhoto::find($id);
        if (empty($photo)) {
            $photo = new GalleryPhoto();
        }

        $photo->fill($request->all());
        $photo->description = $photo->description ?? '';

        if (empty($photo->file)) {
            $photo->file = "";
        }
        
        if (empty($photo->title)){
            return json_encode(["ok" => 0, "error" => "Debe ingresar el nombre de la foto"]);
        }
        
        $image = $request->file("image");
    
        if (!$photo->id && (empty($image) || !$image->isValid())) {
            return json_encode(["ok" => 0, "error" => "Debe ingresar la imagen"]);
        }

        if ($photo->save()) {
            
            if (!empty($image) && $image->isValid()){
                $path = public_path('storage/images/galleries/'.$photo->gallery_id.'/');

                if (!is_dir($path)) {
                    $parts = ['storage', 'images', 'galleries', $photo->gallery_id];
                    $subpath = public_path() . '/';
                    foreach ($parts as $p) {
                        $subpath .= $p . '/';

                        if (!is_dir($subpath)) {
                            mkdir($subpath);
                        }
                    }
                }

                $filename = $photo->id . "." . $image->extension(); 
                $image->move($path, $filename);
                $photo->file = $filename;
                $photo->save();
            }

            return json_encode(["ok" => 1, "message" => "Foto guardada exitosamente", "photos" => $photo->gallery()->first()->photos()->get()]);
        } 
        else {
            return json_encode(["ok" => 0, "error" => "No se pudo guardar la foto"]);
        }
    }
    
    function deletePhoto($id){
        $photo = GalleryPhoto::find($id);
        
        if (empty($photo)){
            return json_encode(["ok" => 0, "error" => "Foto no encontrada"]);
        }
        
        $path = public_path('storage/images/galleries/'.$photo->gallery.'/'.$photo->file);
        
        if ($photo->delete()){
            @unlink($path);
            return json_encode(["ok" => 1, "message" => "Foto eliminada exitosamente", "photos" => $photo->gallery()->first()->photos()->get()]);
        }
        else{
            return json_encode(["ok" => 0, "error" => "No se pudo eliminar la foto"]);
        }
    }
}
