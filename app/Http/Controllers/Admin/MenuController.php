<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BlogCategory;
use App\Models\BlogContent;
use App\Models\Menu;
use App\Models\PageContent;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MenuController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->moduleName = 'menus';
    }

    public function list() {
        $this->checkPermission('view');

        $menus = Menu::with(['parent', 'link'])->orderBy('weight', 'asc')->get();
        $types = Menu::getTypes();
        return view('backoffice.menu.list', compact('menus', 'types'));
    }

    public function dataTable() {
        return \DataTables::of(Menu::with(['parent', 'link']))->orderBy('weight', 'asc')->make(true);
    }

    private function showForm(Menu $menu, $edit = false) {
        if (empty($menu)) {
            $menu = new Menu();
        }

        $posts = BlogContent::active()->orderBy('title')->get();
        $blogCategories = BlogCategory::orderBy('name')->get();
        $productCategories = ProductCategory::orderBy('name')->get();
        $pages = PageContent::active()->orderBy('title')->get();
        $types = Menu::getTypes();
        $menus = Menu::orderBy('title')->get();

        return view('backoffice.menu.form', compact('menu', 'edit', 'menus', 'posts', 'blogCategories', 'productCategories', 'pages', 'types'));
    }

    public function create() {
        $this->checkPermission('create');

        return $this->showForm(new Menu());
    }

    public function edit($id) {
        $this->checkPermission('edit');

        $menu = Menu::find($id);
        if (empty($menu)) {
            return back()
                ->with(['message_error' => __("Menú no encontrada")]);
        }

        return $this->showForm($menu, true);
    }

    public function store(Request $request) {
        $this->checkPermission('create');

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'link_type' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $menu = new Menu();
        $menu->fill($request->all());
        $menu->display = $menu->display ?: 'N';

        switch ($menu->link_type) {
            case 'page_content':
                $menu->link_id = $request->get('page_content_id');
                break;
            case 'product_category':
                $menu->link_id = $request->get('product_category_id');
                break;
            case 'blog_content':
                $menu->link_id = $request->get('blog_content_id');
                break;
            case 'blog_category':
                $menu->link_id = $request->get('blog_category_id');
                break;
            default:
                $menu->link_id = null;
        }

        if ($menu->save()) {
            return redirect()
                ->route('admin::menu::list')
                ->with(['message_success' => __("Menú guardado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo guardar el menú")]);
        }
    }

    public function update(Request $request) {
        $this->checkPermission('edit');

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'link_type' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $menu = Menu::find($request->get('id'));
        if (empty($menu)) {
            return back()
                ->with(['message_error' => __("Menú no encontrado")]);
        }

        $menu->fill($request->all());
        $menu->display = $menu->display ?: 'N';

        switch ($menu->link_type) {
            case 'page_content':
                $menu->link_id = $request->get('page_content_id');
                break;
            case 'product_category':
                $menu->link_id = $request->get('product_category_id');
                break;
            case 'blog_content':
                $menu->link_id = $request->get('blog_content_id');
                break;
            case 'blog_category':
                $menu->link_id = $request->get('blog_category_id');
                break;
            default:
                $menu->link_id = null;
        }

        if ($menu->save()) {
            return redirect()
                ->route('admin::menu::list')
                ->with(['message_success' => __("Menú actualizado exitosamente")]);
        }
        else{
            return back()
                ->withInput()
                ->with(['message_error' => __("No se pudo actualizar el menú")]);
        }
    }

    public function delete($id) {
        $this->checkPermission('delete');

        DB::beginTransaction();

        $menu = Menu::find($id);
        if (empty($menu)) {
            return back()
                ->with(['message_error' => __("Menú no encontrado")]);
        }

        if ($menu->delete()) {
            DB::commit();
            return redirect()
                ->route('admin::menu::list')
                ->with(['message_success' => __("Menú borrado exitosamente")]);
        }
        else{
            DB::rollBack();
            return back()
                ->with(['message_error' => __("No se pudo borrar el menú")]);
        }
    }
}
