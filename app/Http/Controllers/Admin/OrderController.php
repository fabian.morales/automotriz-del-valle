<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;

class OrderController extends Controller
{
    public function list() {
        $orders = Order::orderBy('created_at', 'desc')->get();
        return view("backoffice.order.list", ["orders" => $orders]);
    }

    public function edit($id) {
        $order = Order::with(['details.product.images', 'details.attributeValues.attribute'])
            ->where('id', $id)
            ->first();

        if (empty($order)) {
            return back()
                ->with(['message_error' => 'La cotización no existe']);
        }

        return view("backoffice.order.form", ["order" => $order]);
    }
}
