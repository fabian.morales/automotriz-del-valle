<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\LocaleHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PageContent;
use Illuminate\Support\Facades\Validator;

class PageContentController extends Controller
{
    public function list() {
        $pages = PageContent::all();
        return view("backoffice.page_content.list", ["pages" => $pages]);
    }

    public function showForm(PageContent $page) {
        if (empty($page)) {
            $page = new PageContent();
        }

        return view("backoffice.page_content.form", ["page" => $page]);
    }

    public function edit($id) {
        $page = PageContent::find($id);
        return $this->showForm($page);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $id = $request->get('id');
        $page = PageContent::findOrNew($id);

        $page->fill($request->all());
        $page->type = 'page';
        $page->user_id = \Auth::user()->id;
        $page->slug = LocaleHelper::hyphenize($page->title);

        if (empty($page->home)) {
            $page->home = 'N';
        }

        if (empty($page->status)) {
            $page->status = 0;
        }

        if (empty($page->weight)) {
            $page->weight = 0;
        }

        if ($page->save()) {
            if ($request->has('save_stay')) {
                return redirect()
                    ->route('admin::page_content::edit', ['id' => $page->id])
                    ->with(['message_success' => 'Página guardada exitosamente']);
            }
            else{
                return redirect()
                    ->route('admin::page_content::list')
                    ->with(['message_success' => 'Página guardada exitosamente']);
            }
        }
        else{
            return back()
                ->with(['message_error' => 'No se pudo guardar la página']);
        }
    }

    public function delete($id) {
        $page = PageContent::find($id);
        if (empty($page) || $page == null) {
            return back()
                ->with(['message_error' => 'La página solicitada no existe']);
        }

        if ($page->delete()) {
            return redirect()
                ->route('admin::page_content::list')
                ->with(['message_success' => 'Página borrada exitosamente']);
        }
        else{
            return back()
                ->with(['message_error' => 'No se pudo borrar la página']);
        }
    }
}
