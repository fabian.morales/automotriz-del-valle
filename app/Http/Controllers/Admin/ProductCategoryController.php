<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Helpers\LocaleHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductCategoryController extends Controller
{
    public function list($id = "") {
        $categories = [];

        if (!empty($id)) {
            $category = ProductCategory::find($id);
            if (empty($category) || $category == null) {
                return back()
                ->with(['message_error' => 'La categoría no existe']);
            }

            $categories = ProductCategory::where('product_category_id', $id)->orderBy('name')->get();
        }
        else{
            $categories = ProductCategory::whereNull('product_category_id')->get();
        }
        
        return view("backoffice.product_category.list", ["categories" => $categories]);
    }

    public function showForm(ProductCategory $category) {
        if (empty($category)) {
            $category = new ProductCategory();
        }

        $categories = ProductCategory::where('id', '!=', $category->id)->orderBy('name')->get();

        return view("backoffice.product_category.form", ["category" => $category, "categories" => $categories]);
    }

    public function edit($id) {
        $category = ProductCategory::find($id);
        return $this->showForm($category);
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $id = $request->get('id');
        $category = ProductCategory::findOrNew($id);
        $category->fill($request->all());
        $category->slug = LocaleHelper::hyphenize($category->name);
        
        if ($category->save()) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = uniqid() . '.' . $file->extension();
        
                $path = ['storage', 'images', 'product_categories', $category->id];
                $dir = public_path();
                foreach ($path as $p) {
                    $dir .= '/' . $p;
                    if (!is_dir($dir)) {
                        mkdir($dir);
                    }
                }

                $file->move($dir, $filename);
                $category->image = $filename;
                $category->save();

                ImageHelper::makeImage($category->getOriginalImagePath(), $category->getImagePath('thumbnail'), 370, 220);
            }

            return redirect()
                ->route('admin::product_category::list')
                ->with(['message_success' => 'Categoría guardada exitosamente']);
        }
        else{
            return back()
                ->with(['message_error' => 'No se pudo guardar la categoría']);
        }
    }

    public function delete($id) {
        $category = Category::find($id);
        if (empty($category) || $category == null) {
            return back()
                ->with(['message_error' => 'La categoría solicitado no existe']);
        }

        if ($categoria->children()->count() > 0) {
            return back()
                ->with(['message_error' => 'La categoría no se puede borrar porque contiene categorías hijas']);
        }

        DB::beginTransaction();

        /**
         * TO-DO: borrado de las categorias asociadas a productos
         */
        $category->products()->detach();

        if ($category->delete()) {
            DB::commit();
            return redirect()
                ->route('admin::product_category::list')
                ->with(['message_success' => 'Categoría borrada exitosamente']);
        }
        else{
            DB::rollBack();
            return back()
                ->with(['message_error' => 'No se pudo borrar la categoría']);
        }
    }
}
