<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\LocaleHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\Feature;
use App\Models\ProductFeature;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductCombination;
use App\Models\ProductCombinationAtribute;
use Illuminate\Support\Collection;
use App\Models\ProductImage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\File\File as FileObject;

class ProductController extends Controller
{
    public function list() {
        $products = Product::with('images')->get();
        return view("backoffice.product.list", ["products" => $products]);
    }

    public function showForm(Product $product, Collection $categories = null, Collection $features = null) {
        if (empty($product)) {
            $product = new Product();
        }

        if ($categories == null) {
            $categories = ProductCategory::with('children.children.children')->get();
        }

        if ($features == null) {
            $features = Feature::all();
        }

        $attributes = Attribute::with('values')->get();
        $main = null;

        return view("backoffice.product.form", compact("product", "categories", "features", "attributes", "main"));
    }

    public function edit($id) {
        $product = Product::where('id', $id)->with(['images'])->first();
        if (empty($product) || $product == null) {
            return redirect()
                ->route('admin::product::list')
                ->with(['message_error' => 'El producto solicitado no existe']);
        }

        $categories = ProductCategory::with(['products' => function($q) use ($id) {
            $q->where('product_id', $id);
        }])->get();
        $main = $categories->first(function($value) {
            return !empty($value->products) && sizeof($value->products) && $value->products[0]->pivot->main == 1;
        });
        $features = Feature::with(['values' => function($q) use($id) {
            $q->where('product_id', $id);
        }])->get();
        $attributes = Attribute::with('values')->get();
        return view("backoffice.product.form", compact("product", "categories", "features", "attributes", "main"));
    }

    public function save(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'sku' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $id = $request->get('id');
        $product = Product::findOrNew($id);

        $product->fill($request->all());
        $product->slug = LocaleHelper::hyphenize($product->name);

        if (empty($product->featured) || !$request->has('featured')) {
            $product->featured = 'N';
        }

        if (empty($product->offer) || !$request->has('offer')) {
            $product->offer = 'N';
        }

        if (empty($product->status) || !$request->has('status')) {
            $product->status = 0;
        }

        if ($product->save()) {            
            $mainId = $request->get("main_category");
            $categories = array_flip($request->get('categories'));
            array_walk($categories, function(&$value, $key) use($mainId) {
                $value = [
                    'main' => $key == $mainId ? 1 : null,
                ];
            });
            $product->categories()->sync($categories);

            foreach ($request->get('features') ?? [] as $index => $value) {
                if (empty($value)) {
                    ProductFeature::where('product_id', $product->id)
                        ->where('feature_id', $index)
                        ->delete();
                        
                    continue;
                }

                $feature = ProductFeature::where('product_id', $product->id)
                    ->where('feature_id', $index)
                    ->firstOrNew(['product_id' => $product->id, 'feature_id' => $index]);

                $feature->value = $value;
                $feature->save();
            }

            if ($request->has('offer_file')) {
                $file = $request->file('offer_file');
                $filename = uniqid() . '.' . $file->extension();
        
                $path = ['storage', 'images', 'products', $id, 'offer'];
                $dir = public_path();
                foreach ($path as $p) {
                    $dir .= '/' . $p;
                    if (!is_dir($dir)) {
                        mkdir($dir);
                    }
                }

                $file->move($dir, $filename);
                $product->offer_file = $filename;
                $product->save();
            }

            if ($request->has('save_stay')) {
                return redirect()
                    ->route('admin::product::edit', ['id' => $product->id])
                    ->with(['message_success' => 'Producto guardado exitosamente']);
            }
            else{
                return redirect()
                    ->route('admin::product::list')
                    ->with(['message_success' => 'Producto guardado exitosamente']);
            }
        }
        else{
            return back()
                ->with(['message_error' => 'No se pudo guardar el producto']);
        }
    }

    public function delete($id) {
        $product = Product::find($id);
        if (empty($product) || $product == null) {
            return back()
                ->with(['message_error' => 'El producto solicitado no existe']);
        }

        $product->categories()->detach();
        foreach ($producto->images ?? '' as $image) {
            if (is_file($image->getPath())) {
                @unlink($image->getPath());
                @unlink($image->getImagePath('thumbnail'));
                @unlink($image->getImagePath('medium'));
            }

            $image->delete();
        }
        //$producto->images()->delete();

        $producto->attributes()->detach();
        $producto->productFeatures()->delete();
        $producto->combinations()->delete();

        /**
         * TO-DO: borrado de las ofertas asociadas a productos
         */
        //$producto->offers()->delete();

        /**
         * TO-DO: borrado de los tags asociados a productos
         */
        //$producto->tags()->delete();

        if ($product->delete()) {
            return redirect()
                ->route('admin::product::list')
                ->with(['message_success' => 'Producto borrado exitosamente']);
        }
        else{
            return back()
                ->with(['message_error' => 'No se pudo borrar el producto']);
        }
    }

    public function saveImage(Request $request, $id) {
        $file = $request->file('file');
        $originalFilename = storage_path("app/{$file->getClientOriginalName()}");
        $file->move(storage_path('app'), $file->getClientOriginalName());
        $product = Product::find($id);
        $product->saveImage(new FileObject($originalFilename));
        return $product->images()->get()->toJson();
    }

    public function deleteImages(Request $request, $id) {
        $ids = $request->get('delete_img');
        foreach ($ids as $i) {
            $image = ProductImage::find($i);
            if (!empty($image) && $image != null && $image->product_id == $id) {
                if (is_file($image->getImagePath())) {
                    @unlink($image->getImagePath());
                    @unlink($image->getImagePath('thumbnail'));
                    @unlink($image->getImagePath('medium'));
                }

                $image->delete();
            }
        }

        $product = Product::find($id);
        return $product->images()->get()->toJson();
    }

    public function getCombinationList($id) {
        $product = Product::where('id', $id)->with('combinations.attributeValues.attribute')->first();
        $ret = "";
        if (!empty($product) && !empty($product->combinations)) {
            $ret = $product->combinations->toJson();
        }

        return $ret;
    }

    public function saveCombination(Request $request) {
        $productId = $request->get('product_id');
        $values = $request->get('values');

        $keys = array_values($values);
        $hash = collect($keys)->sort()->implode('-');

        $combination = ProductCombination::where('product_id', $productId)->where('hash', $hash)->first();

        $ret = ['ok' => 0, 'message' => ''];
        if (empty($combination)) {
            DB::beginTransaction();
            try {
                $combination = new ProductCombination(['product_id' => $productId, 'hash' => $hash]);
                $combination->save();
                
                foreach ($values as $attributeId => $valueId) {
                    $combinationValue = new ProductCombinationAtribute([
                        'product_combination_id' => $combination->id, 
                        'attribute_value_id' => $valueId, 
                        'attribute_id' => $attributeId, 
                        'product_id' => $productId, 
                        'created_at' => date('Y-m-d H:i:s'), 
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                    
                    $combinationValue->save();
                }
                
                $product = Product::where('id', $productId)->with('combinations.attributeValues.attribute')->first();
                $values = $product->combinations;
                $ret = [
                    'ok' => 1,
                    'data' => $values,
                ];
    
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
                $ret['message'] = $th->getMessage();
            }
        }
        else {
            $ret['message'] = 'Ya existe esta combinación de atributos en el producto';
        }

        return json_encode($ret);
    }

    public function deleteCombination($id) {
        $ret = ['ok' => 0];

        $combination = ProductCombination::find($id);

        if (!empty($combination)) {
            DB::beginTransaction();
            try {
                $productId = $combination->product_id;
                $combination->combinationAttributes()->delete();
                if ($combination->delete()) {
                    $product = Product::where('id', $productId)->with('combinations.attributeValues.attribute')->first();
                    $values = $product->combinations;
                    $ret = [
                        'ok' => 1,
                        'data' => $values
                    ];

                    DB::commit();
                }
                else {
                    throw new \Exception("No se pudo borrar la combinación");
                }
            } catch (\Throwable $th) {
                Db::rollBack();
                $ret['message'] = $th->getMessage();
            }
        }

        return json_encode($ret);
    }

    public function generateProductImages($id) {
        $product = Product::with('images')->where('id', $id)->first();
        if (empty($product) || $product == null) {
            return back()
                ->with(['message_error' => 'El producto solicitado no existe']);
        }

        $product->regenerateImages();

        return back()
            ->with(['message_success' => 'Imagenes generadas']);
    }

    public function generateAllProductsImages() {
        ini_set('max_execution_time', 3600);

        $products = Product::whereHas('images')->get();
        foreach ($products as $product) {
            $product->regenerateImages();
        }

        return back()
            ->with(['message_success' => 'Imagenes generadas']);
    }

    public function showMassiveUploadForm() {
        return view("backoffice.product.massive_form");
    }

    public function massiveProductUpload(Request $request) {
        ini_set('max_execution_time', 1200);
        $handle = fopen($request->file('products')->path(), 'r');
        $separator = $request->get('separator');

        $errors = [];
        $isHeader = true;
        while (!feof($handle)) {
            $data = fgetcsv($handle, 0, $separator);
            if ($isHeader) {
                $isHeader = false;
                continue;
            }

            if (empty($data) || !sizeof($data)) {
                continue;
            }
            
            $_sku = $data[0];
            $_name = ucwords(mb_strtolower($data[2]));
            $_category = ucfirst(mb_trtolower($data[4]));
            $_subcategory = ucfirst(mb_strtolower($data[5]));
            $_description = $data[6];

            DB::beginTransaction();

            try{
                $categoryIds = [];
                if (!empty($_category)) {
                    $category = ProductCategory::firstOrCreate([
                        'name' => $_category,
                        'slug' => LocaleHelper::hyphenize($_category),
                    ]);

                    $categoryIds[] = $category->id;
                }

                if (!empty($_subcategory)) {
                    $subcategory = ProductCategory::firstOrCreate([
                        'name' => $_subcategory,
                        'slug' => LocaleHelper::hyphenize($_subcategory),
                    ]);

                    $categoryIds[] = $subcategory->id;
                }

                $product = Product::firstOrNew([
                    'sku' => $_sku,
                ]);

                $product->name = $_name;
                $product->slug = LocaleHelper::hyphenize($_name);
                $product->description = $_description;
                $product->featured = 'N';
                $product->offer = 'N';
                $product->views = 0;
                $product->status = 1;

                $product->save();

                if (count($categoryIds)) {
                    $product->categories()->sync($categoryIds);
                }

                $images = File::glob(storage_path("app/catalog/{$_sku}.*"));

                if (!empty($images)) {
                    $image = reset($images);
                    $imageFile = new FileObject($image);
                    $product->saveImage($imageFile);
                }

                DB::commit();
            }
            catch(\Exception $ex) {
                DB::rollBack();
                $errors[] = [
                    'data' => $data,
                    'error' => $ex->getMessage(),
                ];
            }
        }

        if (sizeof($errors)) {
            print_r($errors);
            die();
        }

        return redirect()
            ->route('admin::product::list')
            ->with(['message_success' => 'Producto guardado exitosamente']);
    }
}
