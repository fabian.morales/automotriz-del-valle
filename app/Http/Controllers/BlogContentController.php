<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\BlogContent;
use App\Models\CategoriaBlog;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;

class BlogContentController extends Controller
{
    public function showHome() {
        $posts = BlogContent::with(['categories', 'author'])
            ->active()
            ->where('type', 'blog')
            ->orderBy('created_at', 'desc')
            ->paginate(6);
        $categories = BlogCategory::orderBy('name')->get();
        $popularPosts = BlogContent::orderBy('views', 'desc')->take(5)->get();
        return view('frontoffice.blog.home', compact("posts", "categories", "popularPosts"));
    }

    public function showCategory($id, $slug) {
        $posts = BlogContent::with(['categories', 'author'])
            ->active()
            ->where('type', 'blog')
            ->whereHas('categories', function($q) use ($id) {
                $q->where('blog_category_id', $id);
            })
            ->orderBy('created_at', 'desc')
            ->paginate(6);

        $category = BlogCategory::where('id', $id)->where('slug', $slug)->first();
        $popularPosts = BlogContent::orderBy('views', 'desc')->take(5)->get();
        $categories = BlogCategory::orderBy('name')->get();
        if (empty($category)) {
            return back()
                ->with(['message_error' => 'La categoría no existe']);
        }

        $category->views += 1;
        $category->save();

        return view('frontoffice.blog.category', compact('category', 'posts', 'categories', 'popularPosts'));
    }

    public function showPost($id, $slug) {
        $post = BlogContent::where('id', $id)
            ->active()
            ->where('slug', $slug)
            ->where('type', 'blog')
            ->with(['categories', 'author'])
            ->first();

        $popularPosts = BlogContent::where('id', '!=', $id)
            ->active()
            ->orderBy('views', 'desc')
            ->take(5)
            ->get();
        $categories = BlogCategory::orderBy('name')->get();
        if (empty($post)) {
            return back()
                ->with(['message_error' => 'El artículo no existe']);
        }

        $desc = explode(" ", strip_tags($post->body));
        $desc = implode(" ", array_slice($desc, 0, 20));

        SEOMeta::setTitle($post->title);
        SEOMeta::setDescription($desc);
        //SEOMeta::setCanonical('https://codecasts.com.br/lesson');

        OpenGraph::setDescription($desc);
        OpenGraph::setTitle($post->title);
        OpenGraph::setUrl(url()->current());
        OpenGraph::addProperty('type', 'articles');

        TwitterCard::setTitle($post->title);
        //TwitterCard::setSite('@LuizVinicius73');

        $content = $post->processVariables();

        $post->views += 1;
        $post->save();

        return view('frontoffice.blog.post', compact('post', 'content', 'categories', 'popularPosts'));
    }
}
