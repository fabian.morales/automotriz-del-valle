<?php

namespace App\Http\Controllers;

use App\Boletin;
use App\Models\Bulletin;
use App\Rules\Recaptcha;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;

class BulletinController extends Controller
{
    public function save(Request $request) {
        try {
            $rules = [
                'email' => ['required', 'string', 'email', 'max:255',],
            ];
    
            if (App::environment(['staging', 'production'])) {
                $rules['g-recaptcha-response'] = ['required', new Recaptcha];
            }

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                throw new \Exception("Debes completar tus datos");
            }
            
            $email = $request->get("email");
            $bulletin = Bulletin::where('email', $email)->first();
            if (empty($bulletin)) {
                $bulletin = new Bulletin(['email' => $email]);
                $bulletin->save();
            }

            return json_encode(['ok' => 1]);
        } 
        catch (\Exception $th) {
            return json_encode(['ok' => 0, 'message' => $th->getMessage()]);
        }   
    }
}
