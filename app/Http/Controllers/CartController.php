<?php

namespace App\Http\Controllers;

use App\Helpers\CartHelper;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function add(Request $request, $plus = true) {
        $cartKey = session(CartHelper::getKey(), uniqid());
        session([CartHelper::getKey() => $cartKey]);

        $productId = $request->get('product_id', 0);
        $product = Product::findOrFail($productId);
        $quantity = $request->get('quantity', 1);

        $hash = $request->get('combination_hash');
        $combinationId = null;

        if (!empty($hash)) {
            $combination = ProductCombination::where('hash', $hash)->firstOrFail();
            $combinationId = $combination->id;
        }

        $cart = Cart::firstOrCreate([
            'key' => $cartKey,
            'product_id' => $product->id,
            'product_combination_id' => $combinationId,
        ]);

        $cart->quantity = $plus ? $cart->quantity + $quantity : $quantity;

        //TO-DO: fill fields with price

        if ($cart->save()) {
            $count = CartHelper::calculateTotalCount();
            CartHelper::saveTotalCount($count);
            return ["ok" => 1, "items" => $count];
        }
        else {
            return ["ok" => 0, "message" => "No se pudo añadir el producto al carrito"];
        }
    }

    public function update(Request $request) {
        $details = $request->get('detail');
        $cartKey = session(CartHelper::getKey(), uniqid());
        
        array_walk($details, function(&$value, $key) use($cartKey) {
            $value = ['id' => $key, 'key' => $cartKey, 'quantity' => $value];
        });

        Cart::upsert($details, ['id', 'key'], ['quantity']);
        Cart::where('quantity', '<=', 0)->delete();

        $count = CartHelper::calculateTotalCount();
        CartHelper::saveTotalCount($count);

        return ["ok" => 1, "items" => $count];
    }

    public function delete(Request $request, $id) {
        $cart = Cart::findOrFail($id);
        if ($cart->delete()) {
            $count = CartHelper::calculateTotalCount();
            CartHelper::saveTotalCount($count);

            return ["ok" => 1, "items" => $count];
        }
        else {
            return ["ok" => 0, "message" => "No se pudo quitar el producto del carrito"];
        }
    }

    public function list() {
        $cartKey = session(CartHelper::getKey(), uniqid());
        session([CartHelper::getKey() => $cartKey]);

        $cart = Cart::with(['product.images', 'product.categories'])->where('key', $cartKey)->get();
        return view('frontoffice.cart.list', compact('cart'));
    }
}
