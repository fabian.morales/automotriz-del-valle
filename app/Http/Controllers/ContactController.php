<?php

namespace App\Http\Controllers;

use App\Mail\NewContactAdmin;
use App\Mail\NewContactUser;
use App\Models\Contact;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller {
    
    public function showForm(){
        return view('frontoffice.static_pages.contact');
    }

    public function save(Request $request) {
        try {
            $rules = [
                'name' => 'required',
                'email' => ['required', 'string', 'email', 'max:255',],
                'subject' => 'required',
                'message' => 'required',
            ];

            if (App::environment(['staging', 'production'])) {
                $rules['g-recaptcha-response'] = ['required', new Recaptcha];
            }

            $validator = Validator::make($request->all(), $rules);
    
            if ($validator->fails()) {
                throw new \Exception("Debes completar tus datos");
            }

            $email = $request->get('email');
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new \Exception("El correo no es válido");
            }

            $contact = new Contact();
            $contact->fill($request->all());
            $contact->save();

            $mailsTo = explode(",", config('custom.mail.admin'));
            Mail::to($mailsTo)->send(new NewContactAdmin($contact));
            Mail::to($contact->email)->send(new NewContactUser($contact));

            return json_encode(['ok' => 1]);
        } 
        catch (\Exception $th) {
            return json_encode(['ok' => 0, 'message' => $th->getMessage()]);
        }
    }
}
