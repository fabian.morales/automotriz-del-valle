<?php

namespace App\Http\Controllers;

use App\Models\BlogContent;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $featuredProducts = Product::active()->where('featured', 'Y')->with(['categories', 'images'])->inRandomOrder()->take(8)->get();
        $popularProducts = Product::active()->with(['categories', 'images'])->orderBy('views', 'desc')->take(8)->get();
        $posts = BlogContent::with(['categories', 'author'])->active()->where('type', 'blog')->orderBy('created_at', 'desc')->take(3)->get();
        $productCategories = ProductCategory::orderBy("name")->get();
        //$productCategories = ProductCategory::whereNull("product_category_id")->orderBy("name")->get();
        $popularProductCategories = ProductCategory::orderBy('views', 'desc')->take(5)->get();
        return view('frontoffice.pages.index', compact("featuredProducts", "popularProducts", "posts", "productCategories", "popularProductCategories"));
    }
}
