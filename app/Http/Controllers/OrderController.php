<?php

namespace App\Http\Controllers;

use App\Helpers\CartHelper;
use App\Mail\NewOrderAdmin;
use App\Mail\NewOrderUser;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderProductAttributeValue;
use App\Models\OrderDetail;
use App\Models\ProductCombination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function registerOrder(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'city_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required',
            'accept_terms' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(["ok" => 0, "message" => "Datos incompletos"]);
        }

        $order = new Order();
        $order->fill($request->all());
        $order->status = 'N';

        if (empty($order->whatsapp)) {
            $order->whatsapp = 'N';
        }

        if (empty($order->accept_terms)) {
            $cotizacion->accept_terms = 'N';
        }

        $ret = ["ok" => 0];
        DB::beginTransaction();

        try {
            if (!$order->save()) {
                throw new \Exception("No se pudo guardar la cotización");
            }

            $cartKey = session(CartHelper::getKey(), uniqid());
    
            $cart = Cart::where('key', $cartKey)->get();
            if (empty($cart)) {
                throw new \Exception("No hay productos en el carrito");
            }

            foreach ($cart as $det) {
                $detail = new OrderDetail();
                $detail->order_id = $order->id;
                $detail->product_combination_id = $det->combination_id;
                $detail->product_id = $det->product_id;
                $detail->quantity = $det->quantity;
                $detail->price = $det->price;
                $detail->unit_price = $det->unit_price;
                $detail->discount_percent = $det->discount_percent;
                $detail->discount = $det->discount;
                $detail->tax_percent = $det->tax_percent;
                $detail->tax = $det->tax;
    
                if (!$detail->save()) {
                    throw new \Exception("No se pudo guardar los detalles de la cotización");
                }
            }

            Cart::where('key', $cartKey)->update(['order_id' => $order->id]);
            session([CartHelper::getKey() => uniqid()]);
            CartHelper::saveTotalCount(0);
            DB::commit();
            
            $emailOrder = Order::with(['details.product', 'details.attributeValues.attribute'])->where('id', $order->id)->first();
            $mailsTo = explode(",", config('custom.mail.admin'));
            Mail::to($mailsTo)->send(new NewOrderAdmin($emailOrder));
            Mail::to($emailOrder->email)->send(new NewOrderUser($emailOrder));

            $ret = ["ok" => 1];
        } catch (\Throwable $th) {
            DB::rollBack();
            $ret = ["ok" => 0, "message" => $th->getMessage()];
        }

        return $ret;
    }
}
