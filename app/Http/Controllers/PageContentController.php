<?php

namespace App\Http\Controllers;

use App\Models\PageContent;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;

class PageContentController extends Controller
{
    //use \App\Traits\ContentTrait;

    public function showPage(Request $request, $key, $id = ''){
        if (empty($key)){
            return redirect()->route('front::home');
        }
        
        $page = PageContent::where("slug", $key)->where('type', 'page')->first();
        
        if (empty($page)){
            return redirect()->route('front::home')->with(["message_error" => "Página no encontrada"]);
        }
                
        $content = $page->processVariables();
        
        $template = $request->ajax() ? "frontoffice.page_content.raw" : "frontoffice.page_content.page";        
        return view($template, compact("content", "page"));
    }
}