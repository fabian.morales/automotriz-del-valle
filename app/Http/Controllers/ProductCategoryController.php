<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    public function index(Request $request) {
        $categories = ProductCategory::all();
        return $categories->toJson();
    }

    public function showProducts(Request $request, $id, $slug) {
        $category = ProductCategory::where('id', $id)->where('slug', $slug)->first();
        
        if (empty($category)) {
            return back()
                ->with(['message_error' => 'La categoría no existe']);
        }

        $products = Product::active()->with([
                'categories', 
                'images', 
                'productFeatures.feature'
            ])
            ->whereHas('categories', function($q) use ($category) {
                $q->where('product_category_product.product_category_id', $category->id);
            })
            ->orderBy('created_at', 'desc')
            ->paginate(12);
        $categories = ProductCategory::orderBy("name")->get();

        $category->views += 1;
        $category->save();

        if ($request->wantsJson() || $request->is('api/*')) {
            return $products->toJson();
        }

        return view('frontoffice.products.category', compact("category", "categories", "products"));
    }
}
