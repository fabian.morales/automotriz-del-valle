<?php

namespace App\Http\Controllers;

use App\Models\BlogContent;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;

class ProductController extends Controller
{
    public function index(Request $request) {
        $products = Product::with(['categories', 'images'])->paginate(20);
        return $products->toJson();
    }

    public function showProduct(Request $request, $categoryId, $categorySlug, $id, $slug) {
        $product = Product::where('id', $id)
            ->active()
            ->where('slug', $slug)
            ->whereHas('categories', function (Builder $query) use($categoryId, $categorySlug) {
                $query->where('product_category.id', $categoryId)->where('product_category.slug', $categorySlug);
            })
            ->with(['categories', 'images', 'productFeatures.feature', 'attributeValues', 'attributes.productValues' => function($q) use ($id) {
                $q->where('product_id', $id);
            }])
            ->first();

        if (empty($product)) {
            return back()
                ->with(['message_error' => 'El producto no existe']);
        }

        $desc = explode(" ", strip_tags($product->description));
        $desc = implode(" ", array_slice($desc, 0, 20));

        SEOMeta::setTitle($product->name);
        SEOMeta::setDescription($desc);
        //SEOMeta::setCanonical('https://codecasts.com.br/lesson');

        OpenGraph::setDescription($desc);
        OpenGraph::setTitle($product->name);
        OpenGraph::setUrl(url()->current());
        OpenGraph::addProperty('type', 'products');

        TwitterCard::setTitle($product->type);
        //TwitterCard::setSite('@LuizVinicius73');

        $product->views += 1;
        $product->save();

        $categories = ProductCategory::orderBy('name')->get();
        $popularPosts = BlogContent::orderBy('views', 'desc')->take(5)->get();
        $popularProducts = Product::active()->where('id', '!=', $id)->with(['categories', 'images'])->orderBy('views', 'desc')->take(8)->get();

        return view('frontoffice.products.detail', compact("product", "categories", "popularProducts", "popularPosts"));
    }
}
