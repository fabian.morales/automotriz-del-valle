<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    private function processText($text) {
        $excludedWords = ["el", "la", "los", "las", "a", "o", "y", "e", "u", "al", "de", "del", "para", "con", "en", "su", "sus"];
        $text = strtolower($text);
        $text = preg_replace("/[^a-z0-9áéíóúüñ\-]/i", " ", $text);
        $words = explode(" ", $text);
        $words = array_unique(array_map(function($value) use($excludedWords) {
            return !in_array(trim($value), $excludedWords) ? trim($value) : "";
        }, $words));

        $searchWords = array_map(function($value) use($excludedWords) {
            return "*" . $value . "*";
        }, $words);

        $text = str_replace("**", "", implode(" ", $searchWords));

        return [$words, $text];
    }

    public function productNames() {
        $products = Product::select("name")
            ->orderBy("name")
            ->get()
            ->pluck('name');
        return $products->toJson();
    }

    public function search(Request $request) {
        $products = Product::active()->with([
                'categories', 
                'images', 
                'productFeatures.feature'
            ])
            ->orderBy('name', 'asc')
            ->orderBy('created_at', 'desc');
        
        $textSearch = $request->get("text_search");

        if (!empty($textSearch)) {
            if (substr($textSearch, 0, 1) == '"' && substr($textSearch, -1, 1) == '"') {
                $text = str_replace('"', '', $textSearch);
                $products = $products->where("name", $text);
            }
            else {
                list($words, $text) = $this->processText($textSearch);
                $products = $products->where(function ($q0) use($text) {
                    $q0->whereRaw("MATCH (name) AGAINST (? IN BOOLEAN MODE)", [$text]);
                });
            }
        }

        $products = $products->paginate(12)->appends(['text_search' => $textSearch]);

        return view('frontoffice.products.search', compact("products", "textSearch"));
    }
}
