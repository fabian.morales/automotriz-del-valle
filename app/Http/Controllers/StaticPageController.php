<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticPageController extends Controller
{
    public function showAboutUs(){
        return view('frontoffice.static_pages.about_us');
    }

    public function showTerms(){
        return view('frontoffice.static_pages.terms');
    }

    public function showPrivacyPolicy(){
        return view('frontoffice.static_pages.privacy_policy');
    }
}
