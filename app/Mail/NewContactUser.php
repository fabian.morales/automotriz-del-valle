<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewContactUser extends Mailable
{
    use Queueable, SerializesModels;

    private $contact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Models\Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $supportMails = explode(",", config('custom.mail.support'));
        return $this
            ->bcc($supportMails)
            ->subject('Solicitud de contacto recibida')
            ->view('emails.contact.user')
            ->with(['contact' => $this->contact]);
    }
}
