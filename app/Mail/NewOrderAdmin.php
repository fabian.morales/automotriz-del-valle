<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewOrderAdmin extends Mailable
{
    use Queueable, SerializesModels;
    
    private $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Models\Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $supportMails = explode(",", config('custom.mail.support'));
        return $this
            ->bcc($supportMails)
            ->subject('Solicitud nueva de cotización')
            ->view('emails.order.admin')
            ->with(['order' => $this->order]);
    }
}
