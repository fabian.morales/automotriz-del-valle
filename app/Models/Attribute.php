<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

    protected $table = 'attribute';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type',
    ];

    public function values() {
        return $this->hasMany(AttributeValue::class);
    }

    public function productValues() {
        return $this->hasManyDeep(AttributeValue::class, [ProductCombinationAtribute::class], ['attribute_id', 'id'], ['id', 'attribute_value_id'])->distinct();
    }

    public function products() {
        return $this->hasManyDeep(Product::class, [ProductCombinationAtribute::class]);
    }

    public static function getTypes(){
        return [
            'list' => 'Lista desplegable',
            'radio' => 'Opciones de radio',
            'color' => 'Selector de colores',
        ];
    }
}
