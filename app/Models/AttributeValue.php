<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeValue extends Model
{
    protected $table = 'attribute_value';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label', 'value', 'attribute_id'
    ];

    public function attribute() {
        return $this->belongsTo(Attribute::class);
    }

    public function product() {
        return $this->belongsToMany(Product::class, 'product_attribute_value')->withPivot(['product_id', 'attribute_value_id']);
    }
}
