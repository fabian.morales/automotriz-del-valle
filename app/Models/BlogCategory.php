<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class BlogCategory extends Model
{
    protected $table = 'blog_category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug'
    ];

    public function posts() {
        return $this->belongsToMany(BlogContent::class, 'blog_content_blog_category');
    }

    public function links() {
        return $this->morphMany(Menu::class, 'link');
    }
}
