<?php

namespace App\Models;

class Gallery extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'gallery';
    protected $fillable = [
        'title', 'slug', 'type'
    ];
    
    public function photos(){
        return $this->hasMany(GalleryPhoto::class);
    }
}
