<?php

namespace App\Models;

use App\Traits\ImageTrait;

class GalleryPhoto extends \Illuminate\Database\Eloquent\Model
{
    use ImageTrait;

    protected $table = 'gallery_photo';
    protected $fillable = [
        'gallery_id', 'title', 'description', 'file'
    ];

    protected $folderName = 'galleries';
    protected $idField = "gallery_id";
    protected $fileField = "file";
    
    public function gallery(){
        return $this->belongsTo(Gallery::class);
    }
}
