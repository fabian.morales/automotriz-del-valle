<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Relations\Relation;

Relation::morphMap([
    'page_content' => 'App\Models\PageContent',
    'product_category' => 'App\Models\ProductCategory',
    'blog_content' => 'App\Models\BlogContent',
    'blog_category' => 'App\Models\BlogCategory',

    'product_category_base' => 'App\Models\Menu',
    'blog_category_base' => 'App\Models\Menu',
    'external_url' => 'App\Models\Menu',
    'internal_url' => 'App\Models\Menu',
]);

class Menu extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'menu';
    
    protected $fillable = [
        'title', 'weight', 'url', 'display', 'description', 'meta', 'menu_id', 'link_type', 'link_id'
    ];
    
    public function parent(){
        return $this->belongsTo(Menu::class, 'menu_id');
    }
    
    public function children(){
        return $this->hasMany(Menu::class);
    }

    public function link() {
        return $this->morphTo();
    }

    public static function getTypes() {
        return [
            'page_content' => 'Página personalizada',
            'product_category' => 'Categoría de productos',
            'product_category_base' => 'Todas las categorías de productos',
            'blog_content' => 'Post del blog',
            'blog_category' => 'Categoría del blog',
            'blog_category_base' => 'Todas las categorías del blog',
            'external_url' => 'URL Externa',
            'internal_url' => 'URL Interna',
        ];
    }
}