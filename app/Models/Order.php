<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'message', 'whatsapp', 'city_name', 'accept_terms', 'response', 'status'
    ];

    protected $table = 'order';

    public function details() {
        return $this->hasMany(OrderDetail::class);
    }
}
