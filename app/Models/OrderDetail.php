<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

    protected $table = 'order_detail';
    protected $fillable = [
        'order_id', 'product_combination_id', 'product_id', 'quantity', 'price', 'unit_price', 'discount_percent', 'discount', 'tax_percent', 'tax',
    ];

    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function attributeValues() {
        return $this->hasManyDeep(AttributeValue::class, [ProductCombinationAtribute::class], ['product_combination_id', 'id'], ['product_combination_id', 'attribute_value_id']);
    }
}
