<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageContent extends Model
{
    use SoftDeletes;
    use \App\Traits\ContentTrait;
    
    protected $table = 'page_content';
    protected $fillable = [
        'slug', 'title', 'type', 'body', 'weight', 'home', 'status', 'mobile'
    ];

    public function scopeHome($query) {
        return $query->where('home', 1);
    }

    public function scopeMobile($query) {
        return $query->where('mobile', 1);
    }

    public function scopeActive($query) {
        return $query->where('status', 1);
    }

    public function links() {
        return $this->morphMany(Menu::class, 'link');
    }
}
