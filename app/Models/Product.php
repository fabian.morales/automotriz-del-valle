<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\File as File;

class Product extends Model
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    protected $table = 'product';
    private $link;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'sku', 'description', 'featured', 'offer', 'offer_description', 'offer_file', 'status'
    ];

    public function scopeActive($query) {
        return $query->where('status', 1);
    }

    public function categories() {
        return $this
            ->belongsToMany(ProductCategory::class, 'product_category_product')
            ->withPivot('product_category_id', 'main');
    }

    public function images() {
        return $this->hasMany(ProductImage::class);
    }

    public function attributes(){
        return $this->hasManyDeep(Attribute::class, [ProductCombinationAtribute::class], ['product_id', 'id'], ['id', 'attribute_id'])->distinct();
    }

    public function attributeValues() {
        return $this->hasManyDeep(AttributeValue::class, [ProductCombinationAtribute::class], ['product_id', 'id'], ['id', 'attribute_value_id'])->distinct();
    }

    public function productFeatures() {
        return $this->hasMany(ProductFeature::class);
    }

    public function combinations() {
        return $this->hasMany(ProductCombination::class);
    }

    public function getMainImage($sizeName = "") {
        $image = new ProductImage();
        if (!empty($this->images) && sizeof($this->images)) {
            $image = $this->images[0];
        }

        return $image->getImageUrl($sizeName);
    }

    public function regenerateImages() {
        if (!empty($this->images) && sizeof($this->images)) {
            foreach ($this->images as $image) {
                $image->generateImages();
            }
        }
    }

    public function saveImage(File $file) {
        $image = new ProductImage();
        $image->product_id = $this->id;
        $filename = uniqid() . '.' . $file->getExtension();

        $path = ['storage', 'images', 'products', $this->id];
        $dir = public_path();
        foreach ($path as $p) {
            $dir .= '/' . $p;
            if (!is_dir($dir)) {
                mkdir($dir);
            }
        }

        //$resp = $file->storeAs('storage/imagenes/productos/' . $id, $filename);
        $file->move($dir, $filename);
        $image->file = $filename;
        $image->save();

        $image->generateImages();
    }

    public function getLink() {
        if (empty($this->link)) {
            $categorySlug = '';
            $categoryId = '';
            if (sizeof($this->categories)) {
                $category = $this->categories[0];

                if (sizeof($this->categories) > 1) {
                    $category = $this->categories->first(function($value) {
                        return $value->pivot->main == 1;
                    }) ?? $category;
                }

                $categorySlug = $category->slug;
                $categoryId = $category->id;
            }
    
            $options = [
                'id' => $this->id,
                'slug' => $this->slug,
                'categoryId' => $categoryId, 
                'categorySlug' => $categorySlug
            ];

            $this->link = route('front::product', $options);
        }

        return $this->link;
    }
}
