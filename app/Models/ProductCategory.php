<?php

namespace App\Models;

use App\Traits\ImageTrait;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use ImageTrait;
    
    protected $table = 'product_category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'product_category_id'
    ];

    protected $folderName = 'product_categories';
    protected $idField = "id";
    protected $fileField = "image";

    public function category() {
        return $this->belongsTo(ProductCategory::class);
    }

    public function children() {
        return $this->hasMany(ProductCategory::class);
    }

    public function products() {
        return $this
            ->belongsToMany(Product::class, 'product_category_product')
            ->withPivot('product_id', 'main');
    }

    public function links() {
        return $this->morphMany(Menu::class, 'link');
    }

    public function getDefaultImage() {
        return config('custom.images.category_default');
    }
}
