<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCombination extends Model
{
    use HasFactory;
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;

    protected $table = 'product_combination';

    protected $fillable = [
        'product_id', 'hash'
    ];

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function attributes() {
        return $this->hasManyDeep(Attribute::class, [ProductCombinationAtribute::class], ['product_combination_id', 'id'], ['id', 'attribute_id']);
    }

    public function attributeValues() {
        return $this->hasManyDeep(AttributeValue::class, [ProductCombinationAtribute::class], ['product_combination_id', 'id'], ['id', 'attribute_value_id']);
    }

    public function combinationAttributes() {
        return $this->hasMany(ProductCombinationAtribute::class);
    }
}
