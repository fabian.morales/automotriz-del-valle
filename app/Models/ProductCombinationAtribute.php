<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCombinationAtribute extends Model
{
    use HasFactory;
    use \Staudenmeir\EloquentHasManyDeep\HasTableAlias;

    protected $table = 'product_combination_attribute';

    protected $fillable = [
        'product_id', 'attribute_id', 'attribute_value_id', 'product_combination_id'
    ];

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function attribute() {
        return $this->belongsTo(Attribute::class);
    }

    public function attributeValue() {
        return $this->belongsTo(Attribute::class);
    }

    public function combination() {
        return $this->belongsTo(ProductCombination::class);
    }
}
