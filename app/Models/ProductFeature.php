<?php

namespace App\Models;

use App\Models\Feature;
use Illuminate\Database\Eloquent\Model;

class ProductFeature extends Model
{
    protected $table = 'product_feature';

    protected $fillable = [
        'product_id', 'feature_id', 'value',
    ];

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function feature() {
        return $this->belongsTo(Feature::class);
    }
}
