<?php

namespace App\Models;

use App\Helpers\ImageHelper;
use App\Traits\ImageTrait;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use ImageTrait;

    protected $table = 'product_image';

    protected $folderName = 'products';
    protected $idField = "product_id";
    protected $fileField = "file";

    protected $appends = ['imageUrls'];

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function getDefaultImage() {
        return config('custom.images.product_default');
    }

    public function generateImages() {
        ImageHelper::makeImage($this->getOriginalImagePath(), $this->getImagePath('thumbnail'), 263, 316, true);
        ImageHelper::makeImage($this->getOriginalImagePath(), $this->getImagePath('medium'), 650, 650, true);
    }
}
