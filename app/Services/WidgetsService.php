<?php
namespace App\Services;

use App\Http\Controllers\Admin\BlogCategoryController;
use App\Models\BlogCategory;
use App\Models\Gallery;
use App\Models\Menu;
use App\Models\ProductCategory;

class WidgetsService {
    private function mapCategoryMenu($categories, $route) {
        $ret = [];

        if (!empty($categories) && sizeof($categories)) {
            foreach($categories as $category) {
                $childItem = new \stdClass();
                $childItem->title = $category->name;
                $childItem->link = route($route, ['id' => $category->id, 'slug' => $category->slug]);
    
                if (isset($category->children) && sizeof($category->children)) {
                    $childItem->children = $this->mapCategoryMenu($category->children, $route);
                }
    
                $ret[] = $childItem;
            }
        }

        return $ret;
    }

    public function renderGallery($name, $tpl = "") {
        $tpl = !empty($tpl) ? $tpl : "gallery_mosaic";
        $gallery = Gallery::with("photos")->where("slug", $name)->first();
        return view('frontoffice.components.' . $tpl, ["gallery" => $gallery])->render();
    }

    public function makeMenu($menus = null) {
        if (empty($menus)) {
            $menus = Menu::with('children.link')->where('display', 'Y')->whereNull('menu_id')->orderBy('weight', 'asc')->get();
        }
        
        $ret = [];
        foreach ($menus as $menu) {
            $item = new \stdClass();
            $item->title = $menu->title;
            $item->link = '';
            $item->children = [];

            switch ($menu->link_type) {
                case 'page_content':
                    $item->link = url($menu->link->slug);
                    break;

                case 'product_category':
                    $item->link = route('front::product::category', ['id' => $menu->link_id, 'slug' => $menu->link->slug]);
                    $categories = $menu->link->children()->with('children')->get();
                    $item->children = $this->mapCategoryMenu($categories, 'front::product::category');    
                    break;

                case 'product_category_base':
                    $item->link = '#';
                    $categories = ProductCategory::with('children')->whereNull('product_category_id')->orderBy('name')->get();
                    $item->children = $this->mapCategoryMenu($categories, 'front::product::category');
                    break;

                case 'blog_content':
                    $item->link = route('front::blog::post', ['id' => $menu->link_id, 'slug' => $menu->link->slug]);
                    break;

                case 'blog_category':
                    $item->link = route('front::blog::category', ['id' => $menu->link_id, 'slug' => $menu->link->slug]);
                    break;
                    
                case 'blog_category_base':
                    $item->link = '#';
                    $categories = BlogCategory::orderBy('name')->get();
                    $item->children = $this->mapCategoryMenu($categories, 'front::blog::category');
                    break;
                    
                case 'external_url':
                    $item->link = $menu->url;
                    break;

                case 'internal_url':
                    if (strpos($menu->url, "::") !== FALSE) {
                        $item->link = route($menu->url);
                    }
                    else{
                        $item->link = url($menu->url);
                    }
                    break;
            }

            if (!empty($menu->children) && sizeof($menu->children)) {
                $newChildren = $this->makeMenu($menu->children);
                $item->children = array_merge($item->children, $newChildren);
            }
    
            $ret[] = $item;
        }
        
        return $ret;
    }

    public function getBlogCategories() {
        $categories = BlogCategory::orderBy('name')->get();
        $ret = [];
        if (!empty($categories)) {
            $ret = $categories;
        }

        return $ret;
    }
}