<?php

namespace App\Traits;

use App\Models\Gallery;

trait ContentTrait{
    /*public function getHomeSections(){
        $sections = PageContent::where("type", "section")->where("home", "Y")->orderBy("weight")->get();
        $ret = [];
        foreach ($sections as $section){
            $ret[] = ['html' => $this->getHtmlSection($section), 'mobile' => $s->movil];
        }
        
        return $ret;
    }*/
    
    public function processVariables(){
        $galleries = [];
        $galPattern = "/\\\$gallery\\|(.[^\\s\\!]*)/";
        $content = $this->body;
        
        if (strpos($content, "{!! \$gallery|") !== false){
            $output = [];
            preg_match_all($galPattern, $content, $output);
            
            if (count($output) >= 2){
                foreach($output[1] as $s){
                    $galleries["gallery_".$s] = '';
                }
                
                $gals = Gallery::with("photos")->whereIn("slug", $output[1])->get();
                foreach ($gals as $g){
                    $photosTmp = view('frontoffice.components.gallery_mosaic', ["gallery" => $g])->render();
                    $galleries["gallery_".$g->slug] = $photosTmp;
                }
            }
        }
              
        $vars = [];
        $body = $content;
        
        if (sizeof($galleries)){
            $vars = array_merge($vars, $galleries);
            $body = preg_replace($galPattern, "\$gallery_$1", $body);    
        }

        $html = \Blade::compileString($body);
        return $this->renderHtml($html, $vars);
    }
    
    /*public function getHtmlSection($section, $sectionId = null){
        if (empty($section)){
            $section = PageContent::find($sectionId);
        }
        
        return $this->processVariables($section->body);
    }*/
    
    function renderHtml($__php, $__data)
    {
        $obLevel = ob_get_level();
        ob_start();
        extract($__data, EXTR_SKIP);
        try {
            eval('?' . '>' . $__php);
        } catch (Exception $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw $e;
        } catch (Throwable $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw new FatalThrowableError($e);
        }
        $ret = ob_get_clean();
        return $ret;
    }
}