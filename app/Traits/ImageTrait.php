<?php

namespace App\Traits;

trait ImageTrait{
    private $sizes = ['thumbnail', 'medium'];

    private function getBasePath() {
        return config('custom.images.base_path');
    }

    public function getDefaultImage() {
        return config('custom.images.global_default');
    }

    public function getOriginalImageUrl() {
        return $this->getImageUrl();
    }

    public function getOriginalImagePath() {
        return $this->getImagePath();
    }

    public function getImageUrlsAttribute() {
        $ret = [asset($this->getOriginalImageUrl())];
        foreach ($this->sizes as $size) {
            $ret[] = asset($this->getImageUrl($size));
        }

        return $ret;
    }

    public function getImagePath($sizeName = "") {
        if (!empty($sizeName)) {
            $sizeName .= "_";
        }

        $ret = public_path($this->getBasePath() . $this->folderName);
        $idField = $this->idField;
        $fileField = $this->fileField;
        if (!empty($this->$idField) && (int)$this->$idField > 0) {
            $ret .= '/' . $this->$idField . '/' . $sizeName . $this->$fileField;
        }

        return $ret;
    }

    public function getImageUrl($sizeName = "") {
        $path = $this->getImagePath($sizeName);
        if (!is_file($path)) {
            return $this->getDefaultImage();
        }

        if (!empty($sizeName)) {
            $sizeName .= "_";
        }

        $ret = $this->getBasePath() . $this->folderName;
        $idField = $this->idField;
        $fileField = $this->fileField;

        if (!empty($this->$idField) && (int)$this->$idField > 0) {
            $ret .= '/' . $this->$idField . '/' . $sizeName . $this->$fileField;
        }

        return $ret;
    }
}