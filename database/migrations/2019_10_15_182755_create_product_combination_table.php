<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCombinationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_combination', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id');
            $table->string('hash');
            //$table->unsignedBigInteger('attribute_id');
            //$table->unsignedBigInteger('attribute_value_id');
            $table->foreign('product_id')->references('id')->on('product');
            $table->index('hash');
            //$table->foreign('attribute_id')->references('id')->on('attribute');
            //$table->foreign('attribute_value_id')->references('id')->on('attribute_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atributo_producto');
    }
}
