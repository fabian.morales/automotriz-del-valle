<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('email', 200);
            $table->string('phone', 50);
            $table->string('city_name', 100)->nullable(false);
            $table->string('message', 300);
            $table->string('whatsapp', 1);
            $table->string('response', 500)->nullable();
            $table->string('accept_terms')->nullable(true);
            $table->string('status', 1)->default('N')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
