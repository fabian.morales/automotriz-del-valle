<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('product_combination_id')->nullable();
            $table->unsignedBigInteger('product_id')->nullable();
            $table->unsignedInteger('quantity')->nullable();
            $table->unsignedDouble('price', 15, 3)->nullable();
            $table->unsignedDouble('unit_price', 15, 3)->nullable();
            $table->unsignedDouble('discount_percent', 15, 3)->nullable();
            $table->unsignedDouble('discount', 15, 3)->nullable();
            $table->unsignedDouble('tax_percent', 15, 3)->nullable();
            $table->unsignedDouble('tax', 15, 3)->nullable();

            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('order');
            $table->foreign('product_combination_id')->references('id')->on('product_combination');
            $table->foreign('product_id')->references('id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product_attribute_value');
    }
}
