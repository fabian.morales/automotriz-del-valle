<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_content', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('type');
            $table->string('slug');
            $table->longText('body');
            $table->string('image')->nullable(true);
            $table->string('weight');
            $table->unsignedBigInteger('user_id')->nullable(true);
            $table->integer('views')->default(0);
            $table->tinyInteger('status');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content');
    }
}
