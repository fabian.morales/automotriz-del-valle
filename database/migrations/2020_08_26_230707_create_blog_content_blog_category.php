<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogContentBlogCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_content_blog_category', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('blog_content_id');
            $table->unsignedBigInteger('blog_category_id');
            $table->foreign('blog_content_id')->references('id')->on('blog_content');
            $table->foreign('blog_category_id')->references('id')->on('blog_category');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_blog_category');
    }
}
