<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCombinationAttribute extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_combination_attribute', function (Blueprint $table) {
            $table->bigIncrements('id');
            //$table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('attribute_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('attribute_value_id');
            $table->unsignedBigInteger('product_combination_id');
            //$table->foreign('product_id')->references('id')->on('product');
            $table->foreign('attribute_id')->references('id')->on('attribute');
            $table->foreign('product_id', 'product_id_fk')->references('id')->on('product');
            $table->foreign('attribute_value_id', 'attribute_value_fk')->references('id')->on('attribute_value');
            $table->foreign('product_combination_id', 'product_combination_id_fk')->references('id')->on('product_combination');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_product_attribute_value_attribute');
    }
}
