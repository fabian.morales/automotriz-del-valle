<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key');
            $table->unsignedBigInteger('order_id')->nullable();
            $table->unsignedBigInteger('product_combination_id')->nullable();
            $table->unsignedBigInteger('product_id')->nullable();
            $table->unsignedInteger('quantity')->nullable();
            $table->unsignedDouble('price', 15, 3)->nullable();
            $table->unsignedDouble('unit_price', 15, 3)->nullable();
            $table->unsignedDouble('discount_percent', 15, 3)->nullable();
            $table->unsignedDouble('discount', 15, 3)->nullable();
            $table->unsignedDouble('tax_percent', 15, 3)->nullable();
            $table->unsignedDouble('tax', 15, 3)->nullable();

            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('order');
            $table->foreign('product_combination_id')->references('id')->on('product_combination');
            $table->foreign('product_id')->references('id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
