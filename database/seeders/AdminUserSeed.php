<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminUserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cnt = DB::table('users')->where('name', 'Administrador')->count();
        if ($cnt == 0) {
            DB::table('users')->insert([
                'name' => 'Administrador',
                'email' =>'admin@automotriz.com',
                'password' => Hash::make('123456'),
                'email_verified_at' => Carbon::now(),
                'created_at' => Carbon::now(),
            ]);
        }
    }
}
