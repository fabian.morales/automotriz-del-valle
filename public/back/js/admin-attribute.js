((w, $) => {
    $(() => {
        let hookLinks = () => {
            $('a[rel=lnk-delete-value]').off('click');
            $('a[rel=lnk-delete-value]').on('click', (e) => {
                e.preventDefault();
                let $this = $(e.currentTarget);
                if (!confirm($this.attr('data-del-msg'))){
                    return;
                }

                $.ajax({
                    url: $this.attr('href'),
                    method: 'get',
                    dataType: 'json'
                })
                .done((data) => {
                    if (data.ok === 1) {
                        showData(data.data);
                        hookLinks();
                        alert('Valor de atributo borrado exitosamente');
                    }
                    else{
                        alert('No se pudo borrar el valor de atributo');
                    }
                });
            });

            $('a[rel=lnk-edit-value]').off('click');
            $('a[rel=lnk-edit-value]').on('click', (e) => {
                e.preventDefault();
                let $this = $(e.currentTarget);
                $.ajax({
                    url: $this.attr('href'),
                    method: 'get',
                    dataType: 'json'
                })
                .done((response) => {
                    if (data.ok === 1) {
                        $('#value_id').val(response.data.id);
                        $('#label').val(response.data.label);
                        $('#value').val(response.data.value);
                    }
                    else{
                        alert('No se pudo obtener los datos del valor de atributo');
                    }
                });
            });
        }

        $('#btnSaveValue').on('click', (e) => {
            e.preventDefault();
            if ($('#label').val() === '') {
                alert('El nombre del valor es obligatorio');
                return;
            }

            if ($('#value').val() === '') {
                alert('El valor es obligatorio');
                return;
            }

            $.ajax({
                url:  w.frontier_admin.base_url + '/admin/attribute/values/save',
                data: {
                    'id' : $('#value_id').val(),
                    'label' : $('#label').val(),
                    'value' : $('#value').val(),
                    'attribute_id': $('#id').val(),
                    '_token': $('input[name="_token"]').val()
                },
                method: 'post',
                dataType: 'json'
            })
            .done((response) => {
                if (response.ok === 1) {
                    $('#value_id').val('');
                    $('#label').val('');
                    $('#value').val('');
                    showData(response.data);
                    hookLinks();
                    alert('Valor de atributo guardado exitosamente');
                }
                else{
                    alert('No se pudo guardar el valor de atributo');
                } 
            });
        });

        let showData = (data) => {
            let $trBase = $('#tr-values').clone();
            let $container = $('#tr-values').parent()
            $container.html('').append($trBase);
            $.each(data, (i, o) => {
                let $tr = $trBase.clone();
                $tr.attr('id', 'tr_' + Math.random());
                $tr.find('td[data-id=id]').text(o.id);
                $tr.find('td[data-id=label]').text(o.label);
                $tr.find('td[data-id=value]').text(o.value);
                let $lnkDelete = $tr.find('a[rel=lnk-delete-value]');
                $lnkDelete.attr('href', $lnkDelete.attr('href').replace('X', o.id));
                let $lnkedit = $tr.find('a[rel=lnk-edit-value]');
                $lnkedit.attr('href', $lnkedit.attr('href').replace('X', o.id));
                $tr.removeClass('d-none');
                $('#tr-values').parent().append($tr);
            });
        }

        let getValues = () => {
            if ($('#id').val() !== '') {
                $.ajax({
                    url:  w.frontier_admin.base_url + '/admin/attribute/' + $('#id').val() + '/values',
                    method: 'get',
                    dataType: 'json'
                })
                .done((data) => {
                    showData(data);
                    hookLinks();
                });
            }
        }

        getValues();
    });
})(window, jQuery);