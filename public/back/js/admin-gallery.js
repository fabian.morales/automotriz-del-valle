((w, $) => {
    let hookLinks = () => {
        $("a[rel='btnEditPhoto']").on('click', (e) => {
            e.preventDefault();
            let $this = $(e.currentTarget);
            $.ajax({
                url: $this.attr('href'),
                type: 'get',
                dataType: 'json'
            })
            .done((response) => {
                $("#form_photo")[0].reset();
                $("#photo_id").val(response.id);
                $("#photo_title").val(response.title);
                $("#photo_description").val(response.description);
                $("#modal_photo").modal();
            });
        });

        $("a[rel='btnDeletePhoto']").on('click', (e) => {
            e.preventDefault();
            if (!confirm('¿Está seguro de eliminar esta foto?')){
                return;
            }

            let $this = $(e.currentTarget);

            $.ajax({
                url: $this.attr('href'),
                type: 'get',
                dataType: 'json'
            })
            .done(function(response) {
                if (response.ok == 1) {
                    showGallery(response.photos);
                    hookLinks();
                }
                else{
                    alert(response.message);
                }
            });
        });
    }

    let showGallery = (photos) => {
        $("#gallery_photos").html('');
        $.each(photos, (i, o) => {
            let url = $("#item-source").attr("data-url");
            let $parent = $("#item-source > div").clone();
            $parent.find('img').attr('src', url + '/' + o.file);
            let $lnkDelete = $parent.find("a[rel='btnDeletePhoto']");
            let hrefPrev = $lnkDelete.attr('href');
            $lnkDelete.attr('href', hrefPrev.replace('xxx', o.id));
            let $lnkEdit = $parent.find("a[rel='btnEditPhoto']");
            hrefPrev = $lnkEdit.attr('href');
            $lnkEdit.attr('href', hrefPrev.replace('xxx', o.id));
            $("#gallery_photos").append($parent);
        });
    }

    $(() => {
        $("#btnCreatePhoto").on('click', (e) => {
            e.preventDefault();
            $("#form_photo")[0].reset();
            $("#photo_id").val('');
            $("#modal_photo").modal();
        });
        
        $("#btnSavePhoto").on('click', (e) => {
            e.preventDefault();
            var $form = $("#form_photo");
            var formData = new FormData(document.getElementById("form_photo"));
            $.ajax({
                url: $form.attr("action"),
                type: "post",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
	            processData: false
            })
            .done(function(response){
                if (response.ok == 1) {
                    showGallery(response.photos);
                    hookLinks();
                    $("#modal_photo").modal('hide');
                }
                else{
                    alert(response.message);
                }
            });
        });

        hookLinks();
    });
})(window, jQuery);