((w, $) => {
    $(() => {
        $("#link_type").on('change', (e) => {
            e.preventDefault();
            let $this = $(e.currentTarget);
            $("div[rel=link-type]").addClass('d-none');
            if ($this.val() === 'page_content') {
                $("#page_content_container").removeClass('d-none');
            }
            if ($this.val() === 'blog_content') {
                $("#blog_content_container").removeClass('d-none');
            }
            if ($this.val() === 'blog_category') {
                $("#blog_category_container").removeClass('d-none');
            }
            if ($this.val() === 'product_category') {
                $("#product_category_container").removeClass('d-none');
            }
            if ($this.val() === 'internal_url' || $this.val() === 'external_url') {
                $("#url_container").removeClass('d-none');
            }
        });

        $("#link_type").change();
    });
})(window, jQuery);