((w, $) => {
    $(() => {
        let hookImages = () => {
            $('#images_gallery .img-responsive').on('click', (e) => {
                e.preventDefault();
                let $this = $(e.currentTarget);
                let $check = $this.parent().find('input[type="checkbox"]');
                $check.prop('checked', !$check.prop('checked'));
            });
        }
        
        let populateImages = (json) => {
            $("#images_gallery").html('');
            $.each(json, (i, o) => {
                var $div = $('#item_img').clone();
                $div.attr('id', 'item_img_' + o.id);
                $div.show();
                let $check = $div.find('input[type=checkbox]');
                $check.attr('id', 'delete_img_' + o.id);
                $check.val(o.id);
                let $img = $div.find('img');
                $img.attr('src', w.frontier_admin.base_url + '/storage/images/products/' + o.product_id + '/' + o.file)
                $('#images_gallery').append($div);
            });

            hookImages();
        }

        let dropzone = $("#drop_images").dropzone({ 
            url:  w.frontier_admin.base_url + "/admin/product/images/" + $("#id").val(),
            params: {
                "_token": $("input[name='_token']").val()
            },
            acceptedFiles: 'image/*',
            complete: (data) => {
                populateImages(JSON.parse(data.xhr.response));
            }
        });

        $("#btnDeleteImage").on('click', (e) => {
            e.preventDefault();
            if (confirm('¿Está seguro de borrar las imágenes seleccionadas?')) {
                $.ajax({
                    url: w.frontier_admin.base_url + '/admin/product/images/' + $("#id").val() + '/delete',
                    method: 'post',
                    data: $('#images_gallery input[type="checkbox"]:checked, input[name="_token"]'),
                    dataType: 'json'
                })
                .done((data) => {
                    populateImages(data);
                });
            }
        });

        let hookCombinationLinks = () => {
            $('a[rel=lnk-delete-combination]').off('click');
            $('a[rel=lnk-delete-combination]').on('click', (e) => {
                e.preventDefault();
                let $this = $(e.currentTarget);
                if (!confirm($this.attr('data-del-msg'))){
                    return;
                }

                $.ajax({
                    url: $this.attr('href'),
                    method: 'get',
                    dataType: 'json'
                })
                .done((response) => {
                    if (response.ok === 1) {
                        showCombinations(response.data);
                        hookCombinationLinks();
                        alert('Atributo borrado exitosamente');
                    }
                    else{
                        alert('No se pudo borrar el atributo');
                    }
                });
            });
        }

        $('#btnSaveCombination').on('click', (e) => {
            e.preventDefault();

            if ($('input[rel="combination"]:checked').length === 0) {
                alert('Debe seleccionar un valor');
                return;
            }

            let valueId = {};
            $.each($('input[rel="combination"]:checked'), (i, o) => {
                valueId[$(o).attr('data-attribute')] = o.value;
            });
            //let attributeId = $('input[rel="combination"]:checked').attr('data-attribute');

            $.ajax({
                url:  w.frontier_admin.base_url + '/admin/product/combinations/save',
                data: {
                    'values' : valueId,
                    'product_id': $('#id').val(),
                    '_token': $('input[name="_token"]').val()
                },
                method: 'post',
                dataType: 'json'
            })
            .done((response) => {
                if (response.ok === 1) {
                    showCombinations(response.data);
                    hookCombinationLinks();
                    alert('Combinación guardada exitosamente');
                }
                else{
                    alert(response.message !== '' ? response.message : 'No se pudo guardar la combinación de atributos');
                } 
            });
        });

        let showCombinations = (data) => {
            let $trBase = $('#tr-values').clone();
            let $container = $('#tr-values').parent()
            $container.html('').append($trBase);
            $.each(data, (i, o) => {
                let $tr = $trBase.clone();
                $tr.attr('id', 'tr_' + Math.random());
                $tr.find('td[data-id=id]').text(o.id);
                let $atributes = $("<ul style='margin-bottom: 0'></ul>");
                
                $.each(o.attribute_values, (iav, oav) => {
                    //console.log(oav);
                    let $li = $("<li></li>");
                    $li.html(oav.attribute.name + ": " + oav.label);
                    $atributes.append($li);
                });
                
                $tr.find('td[data-id=attribute]').append($atributes);
                //$tr.find('td[data-id=value]').text(o.value);
                let $linkDelete = $tr.find('a[rel=lnk-delete-combination]');
                $linkDelete.attr('href', $linkDelete.attr('href').replace('X', o.id));
                $tr.removeClass('d-none');
                $('#tr-values').parent().append($tr);
            });
        }

        let getCombinations = () => {
            if ($('#id').val() !== '') {
                $.ajax({
                    url:  w.frontier_admin.base_url + '/admin/product/' + $('#id').val() + '/combinations',
                    method: 'get',
                    dataType: 'json'
                })
                .done((data) => {
                    showCombinations(data);
                    hookCombinationLinks();
                });
            }
        }

        let insertSelectedCategories = () => {
            $("#main_category").html('');
            $("input[name='categories[]']").filter((i, o) => o.checked).each((i, o) => {
                let $option = $(`<option value="${$(o).val()}">${$(o).attr('data-name')}</option>`);
                $option.prop("selected", $(o).attr("data-main") == 1);
                $("#main_category").append($option);
            });
        };

        $("input[name='categories[]']").on('change', () => {
            insertSelectedCategories();
        });

        insertSelectedCategories();
        getCombinations();
        hookImages();
    });
})(window, jQuery);