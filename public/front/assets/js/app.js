(($, w) => {
    let formErrorSub = () => {
        $(".newsletter-form").addClass("animated shake");
        setTimeout(() => {
            $(".newsletter-form").removeClass("animated shake");
        }, 1000);
    };

    let showModal = (message, title, onCloseCallback = null) => {
        $("#modal-title").html(title || "Aviso");
        $("#modal-message").html(message);
        if (onCloseCallback !== null && typeof onCloseCallback === 'function') {
            $("#modal-container").on('hidden.bs.modal', onCloseCallback);
        }
        else {
            $("#modal-container").off('hidden.bs.modal');
        }
        $("#modal-container").modal();
    };

    let showQuoteModal = () => {
        $("#modal-quote-container").modal();
    };

    let closeQuoteModal = () => {
        $("#modal-quote-container").modal('toggle');
    };

    $(() => {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".newsletter-form").on('submit', (e) => {
            e.preventDefault();

            let $form = $(e.currentTarget);

            if ($("#bulletin-email").val() === "") {
                showModal("Debes ingresar el correo");
                formErrorSub();
                return;
            }

            $("#btnBulletinSend").prop('disabled', true);
            $("#btnBulletinSend").addClass('btn-loading');

            $.ajax({
                url: $form.attr("action"),
                method: "post",
                dataType: "json",
                data: $form.serialize()
            })
            .done(response => {
                if (response.ok == 1) {
                    //document.getElementById("quote").reset();
                    $form[0].reset();
                    showModal("Su mensaje ha sido enviado exitosamente, nos pondremos en contacto lo m&aacute;s pronto posible.", "&iexcl;Gracias!");
                }
                else {
                    showModal(response.message);
                }
            })
            .fail(() => {
                showModal("Ocurrió un error al recibir su información, por favor intente nuevamente");
            })
            .always(() => {
                $("#btnBulletinSend").prop('disabled', false);
                $("#btnBulletinSend").removeClass('btn-loading');
            });
        });

        $("#contactForm").on('submit', (e) => {
            e.preventDefault();

            if ($("#name").val() === '') {
                showModal($("#name").attr("data-error"));
                return;
            }

            if ($("#subject").val() === '') {
                showModal($("#subject").attr("data-error"));
                return;
            }

            if ($("#phone").val() === '') {
                showModal($("#phone").attr("data-error"));
                return;
            }

            if ($("#email").val() === '') {
                showModal($("#email").attr("data-error"));
                return;
            }

            if ($("#message").val() === '') {
                showModal($("#message").attr("data-error"));
                return;
            }

            $("#btnSendContact").prop('disabled', true);
            $("#btnSendContact").addClass('btn-loading');

            $.ajax({
                url: $("#contactForm").attr("action"),
                method: "post",
                dataType: "json",
                data: $("#contactForm").serialize()
            })
            .done(response => {
                if (response.ok == 1) {
                    document.getElementById("contactForm").reset();
                    showModal("Su mensaje ha sido enviado exitosamente, nos pondremos en contacto lo m&aacute;s pronto posible.", "&iexcl;Gracias!");
                }
                else {
                    showModal(response.message);
                }
            })
            .fail(() => {
                showModal("Ocurrió un error al recibir su información, por favor intente nuevamente");
            })
            .always(() => {
                $("#btnSendContact").prop('disabled', false);
                $("#btnSendContact").removeClass('btn-loading');
            });
        });

        $("a[rel=color-switch]").on('click', (e) => {
            e.preventDefault();
            let $this = $(e.currentTarget);
            let $target = $("#" + $this.attr("data-input"));
            $target.val($this.attr('data-id'));
            $("a[data-input=" + $this.attr("data-input") + "]").removeClass("active");
            $this.addClass("active");
        });

        $("#btnQuote").on('click', (e) => {
            let hash = [];
            $.each($("[name^=attribute_value]"), (i, o) => {
                hash.push(o.value);
            });

            hash.sort();
            $("#combination_hash").val(hash.join('-'));

            e.preventDefault();

            $("#btnQuote").prop('disabled', true);
            $("#btnQuote").addClass('btn-loading');

            $.ajax({
                url: $("#quote_form").attr("action"),
                method: "post",
                dataType: "json",
                data: $("#quote_form").serialize()
            })
            .done((response) => {
                if (response.ok == 1) {
                    $("#quote_form")[0].reset();
                    $("#count-cart").html(response.items);
                    showModal("Producto agregado correctamente a su pedido.");
                }
                else {
                    showModal(response.message);
                }
            })
            .fail((response) => {
                showModal(response.message);
            })
            .always(() => {
                $("#btnQuote").prop('disabled', false);
                $("#btnQuote").removeClass('btn-loading');
            });
        });

        $("#btnSendQuote").on('click', (e) => {
            e.preventDefault();

            if ($("#name").val() === '') {
                showModal($("#name").attr("data-error"));
                return;
            }

            if ($("#phone").val() === '') {
                showModal($("#phone").attr("data-error"));
                return;
            }

            if ($("#email").val() === '') {
                showModal($("#email").attr("data-error"));
                return;
            }

            if ($("#city_name").val() === '') {
                showModal($("#email").attr("data-error"));
                return;
            }

            if (!$("#accept_terms").is(':checked')) {
                showModal($("#message").attr("data-error"));
                return;
            }

            $("#btnSendQuote").prop('disabled', true);
            $("#btnSendQuote").addClass('btn-loading');

            $.ajax({
                url: $("#quote_form").attr("action"),
                method: "post",
                dataType: "json",
                data: $("#quote_form").serialize()
            })
            .done((response) => {
                if (response.ok == 1) {
                    $("#quote_form")[0].reset();
                    //closeQuoteModal();
                    showModal("Hemos recibido tu solicitud de cotizacón. En breve nos pondremos en contacto contigo.", null, () => window.location.reload());
                }
                else {
                    showModal(response.message);
                }
            })
            .fail((response) => {
                showModal(response.message);
            })
            .always(() => {
                $("#btnSendQuote").prop('disabled', false);
                $("#btnSendQuote").removeClass('btn-loading');
            });
        });

        $(".cart-table a.remove").on('click', (e) => {
            e.preventDefault();
            let $this = $(e.currentTarget);
            $.ajax({
                url: $this.attr("href"),
                method: "post",
                dataType: "json",
            })
            .done((response) => {
                if (response.ok == 1) {
                    showModal("Producto eliminado de su pedido", null, () => window.location.reload());
                }
                else {
                    showModal(response.message);
                }
            })
            .fail((response) => {
                showModal(response.message);
            })
            .always(() => {

            });
        });

        $("#btnUpdateCart").on('click', (e) => {
            e.preventDefault();
            
            $.ajax({
                url: $("#cart-list").attr("action"),
                method: "post",
                dataType: "json",
                data: $("#cart-list").serialize(),
            })
            .done((response) => {
                if (response.ok == 1) {
                    showModal("Su pedido ha sido actualizado", null, () => window.location.reload());
                }
                else {
                    showModal(response.message);
                }
            })
            .fail((response) => {
                showModal(response.message);
            })
            .always(() => {

            });
        });

        let products = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: $('#search-products').attr('data-autocomplete')
        });

        $('#search-products').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'products',
            source: products
        });

        $('#search-products').on('typeahead:select', (e, suggestion) => {
            $(e.currentTarget).typeahead('val', '"' + suggestion + '"');
            $("#search-form").submit();
        });
    });
})(jQuery, window);