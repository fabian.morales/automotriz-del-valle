@extends('backoffice.layouts.base')

@section('css')
<link href="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Gesti&oacute;n de atributos de productos</h1>
<p class="mb-4">
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datos</h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('admin::attribute::save') }}" class="form-row">
            <input type="hidden" id="id" name="id" value="{{ $attribute->id }}" />
            @csrf

            <div class="form-group col-md-6">
                <label>Nombre</label>
                <input type="text" name="name" id="name" class="form-control" value="{{ old('name') ? old('name') : $attribute->name }}" required="required" />
                @error('nombre')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Tipo</label>
                @php
                $currentType = old('type') ? old('type') : $attribute->type
                @endphp
                <select name="type" id="type" class="form-control" required="required">
                    @foreach (App\Models\Attribute::getTypes() as $index => $type)
                    <option value="{{ $index }}" @if($currentType == $index) selected @endif>{{ $type }}</option>
                    @endforeach
                </select>
                @error('type')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>
            <div class="col-md-6">
                <a href="{{ route('admin::attribute::list') }}" class="btn btn-danger btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Regresar</a>
                </a>
                <div class="float-left">&nbsp;</div>
                <button id="btnGuardar" type="submit" class="btn btn-primary btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</a>
                </button>
            </div>
        </form>
    </div>
</div>
@if($attribute->id != 0)
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Valores del atributo</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="form-group col-md-3">
                <label>Nombre</label>
                <input type="text" name="label" id="label" class="form-control" value="" required="required" />
            </div>
            <div class="form-group col-md-3">
                <label>Valor</label>
                <input type="text" name="value" id="value" class="form-control" value="" required="required" />
            </div>
            <div class="form-group col-md-3">
                <br />
                <input type="hidden" id="value_id" name="value_id" value="" />
                <button class="btn btn-primary" id="btnSaveValue">Guardar</button>
            </div>
        </div>
        <table class="table table-bordered" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Valor</th>
                    <th class="no-sort">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <tr id="tr-values" class="d-none">
                    <td data-id="id"></td>
                    <td data-id="label"></td>
                    <td data-id="value"></td>
                    <td>
                        <a href="{{ route('admin::attribute_value::show', ['id' => 'X']) }}" class="display-inline" rel="lnk-edit-value" data-toggle="tooltip" data-placement="top" title="Editar valor de atributo">
                            <i class="fas fa-fw fa-edit"></i>
                        </a>
                        <a href="{{ route('admin::attribute_value::delete', ['id' => 'X']) }}" class="display-inline" rel="lnk-delete-value" data-del-msg="&iquest;Est&aacute; seguro de borrar este valor de atributo?" data-toggle="tooltip" data-placement="top" title="Borrar valor de atributo">
                            <i class="fas fa-fw fa-trash"></i>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endif

@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('back/js/admin-attribute.js') }}"></script>
@stop
