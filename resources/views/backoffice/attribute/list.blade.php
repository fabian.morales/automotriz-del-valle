@extends('backoffice.layouts.base')

@section('css')
<link href="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Atributos de productos</h1>
<p class="mb-4">
    <a href="{{ route('admin::attribute::create') }}" class="btn btn-primary float-right"><i class="fas fa-fw fa-plus-square"></i> Crear atributo</a>
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Atributos registrados</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th class="no-sort">Acciones</th>
                    </tr>
                </thead>
                @php
                /*<tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>*/
                @endphp
                <tbody>
                    @foreach($attributes as $attribute)
                    <tr>
                        <td>{{ $attribute->id }}</td>
                        <td>{{ $attribute->name }}</td>
                        <td>{{ $attribute->type }}</td>
                        <td>
                            <a href="{{ route('admin::attribute::edit', ['id' => $attribute->id]) }}" class="display-inline" data-toggle="tooltip" data-placement="top" title="Editar atributo">
                                <i class="fas fa-fw fa-edit"></i>
                            </a>

                            <a href="{{ route('admin::attribute::delete', ['id' => $attribute->id]) }}" class="display-inline" rel="lnk-borrar" data-del-msg="&iquest;Est&aacute; seguro de borrar este atributo?" data-toggle="tooltip" data-placement="top" title="Borrar atributo">
                                <i class="fas fa-fw fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
@stop
