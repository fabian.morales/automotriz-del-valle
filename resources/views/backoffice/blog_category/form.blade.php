@extends('backoffice.layouts.base')

@section('css')
<link href="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Gesti&oacute;n de categor&iacute;as del blog</h1>
<p class="mb-4">
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datos</h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ $edit ? route('admin::blog_category::update', ['id' => $category->id]) : route('admin::blog_category::store') }}" class="form-row" enctype="multipart/form-data">
            <input type="hidden" id="id" name="id" value="{{ $category->id }}" />
            @csrf

            <div class="form-group col-md-6">
                <label>Nombre</label>
                <input type="text" name="name" id="name" class="form-control" value="{{ old('name') ? old('name') : $category->name }}" required="required" />
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Ruta</label>
                <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug') ? old('slug') : $category->slug }}" readonly />
                @error('slug')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="col-md-6">
                <a href="{{ route('admin::blog_post::list') }}" class="btn btn-danger btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Regresar</a>
                </a>
                <div class="float-left">&nbsp;</div>
                <button id="btnGuardar" type="submit" class="btn btn-primary btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</a>
                </button>
            </div>
        </form>
    </div>
</div>

@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('back/vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('back/vendor/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('back/js/admin-blog-content.js') }}"></script>
@stop
