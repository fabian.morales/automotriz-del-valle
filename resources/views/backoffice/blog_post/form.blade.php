@extends('backoffice.layouts.base')

@section('css')
<link href="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Gesti&oacute;n de posts</h1>
<p class="mb-4">
    &nbsp;
</p>

<!-- DataTales Example -->
<form method="post" action="{{ $edit ? route('admin::blog_post::update', ['id' => $post->id]) : route('admin::blog_post::store') }}" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $post->id }}" />
    @csrf
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Datos</h6>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>T&iacute;titulo</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ old('title') ? old('title') : $post->title }}" required="required" />
                    @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-6">&nbsp;</div>

                <div class="form-group col-md-6">
                    <label>Ruta</label>
                    <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug') ? old('slug') : $post->slug }}" readonly />
                    @error('slug')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-6">&nbsp;</div>

                <div class="form-group col-md-12">
                    <label>Cuerpo</label>
                    <textarea rows="10" name="body" id="body" class="form-control" required="required">
                        {{ old('body') ? old('body') : $post->body }}
                    </textarea>
                    @error('body')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group col-md-12">
                    <label>
                        <input type="checkbox" name="status" id="status" value="1" @if($post->status == 1) checked @endif />
                        Activo
                    </label>                
                </div>
            </div>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Categor&iacute;as</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-sm-12">
                    <p>Seleccione las categorías por asignar a este post</p>
                    <br />
                </div>
                @foreach($categories as $category)
                <div class="form-group col-md-4 com-sm-6">
                    <label>
                        <input type="checkbox" name="categories[]" id="category_{{ $category->id }}" value="{{ $category->id }}" @if(sizeof($category->posts) && ($category->posts[0]->id == $post->id)) checked @endif />
                        {{ $category->name }}
                    </label>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Imagen</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Imagen</label>
                    <input type="file" name="image" id="image" />
                </div>
                <div class="col-md-6">
                    @if(!empty($post->image))
                        <img width="200" src="{{ asset($post->getOriginalImageUrl()) }}" alt="Imagen" />
                    @else
                        &nbsp;
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('admin::blog_post::list') }}" class="btn btn-danger btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Regresar</a>
            </a>
            <div class="float-left">&nbsp;</div>
            <button type="submit" class="btn btn-success btn-icon-split float-left" name="save_stay">
                <span class="icon text-white-50">
                    <i class="fas fa-save"></i>
                </span>
                <span class="text">Guardar y permanecer</a>
            </button>
            <div class="float-left">&nbsp;</div>
            <button id="btnGuardar" type="submit" class="btn btn-primary btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-save"></i>
                </span>
                <span class="text">Guardar</a>
            </button>
        </div>
    </div>
</form>
@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('back/vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('back/vendor/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('back/js/admin-blog-content.js') }}"></script>
@stop
