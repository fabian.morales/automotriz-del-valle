@extends('backoffice.layouts.base')

@section('css')
<link href="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Solicitudes de contacto</h1>
<p class="mb-4">
    <a href="{{ route('admin::contact::download') }}" class="btn btn-primary float-right"><i class="fas fa-fw fa-download"></i> Descargar listado</a>
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Registros encontrados</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Asunto</th>
                        <th>Fecha</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($contacts as $contact)
                    <tr>
                        <td>{{ $contact->id }}</td>
                        <td>{{ $contact->name }}</td>
                        <td>{{ $contact->email }}</td>
                        <td>{{ $contact->subject }}</td>
                        <td>{{ $contact->created_at }}</td>
                        <td>
                            <span data-toggle="tooltip" data-placement="top" title="Ver mensaje">
                                <a href="#" data-toggle="modal" data-target="#contact_message_{{ $contact->id }}">
                                    <i class="fas fa-fw fa-sticky-note"></i>
                                </a>
                            </span>
                            <div class="modal fade" id="contact_message_{{ $contact->id }}" tabindex="-1" role="dialog" aria-labelledby="message_{{ $contact->id }}" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content contact-form">
                                        
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="message_{{ $contact->id }}" style="float: left; font-size: 2rem; color: #888;">{{ $contact->subject }}</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Cerrar">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            {{ $contact->message }}
                                                
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
@stop
