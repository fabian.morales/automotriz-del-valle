@extends('backoffice.layouts.base')

@section('css')
<link href="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Datos de la galería</h1>
<p class="mb-4">
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datos principales</h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('admin::gallery::save') }}" class="form-row">
            <input type="hidden" id="id" name="id" value="{{ $gallery->id }}" />
            @csrf

            <div class="form-group col-md-6">
                <label>Nombre</label>
                <input type="text" name="title" id="title" class="form-control" value="{{ old('title') ? old('title') : $gallery->title }}" required="required" />
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Llave</label>
                <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug') ? old('slug') : $gallery->slug }}" readonly />
                @error('slug')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Tipo</label>
                <select name="type" id="type" class="form-control" required="required">
                    <option value="banner" @if($gallery->tipo == 'banner') selected @endif>Banner</option>
                    <option value="mosaic" @if($gallery->tipo == 'mosaic') selected @endif>Mosaico</option>
                </select>
                @error('type')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>
            
            <div class="col-md-6">
                <a href="{{ route('admin::gallery::list') }}" class="btn btn-danger btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Regresar</a>
                </a>
                <div class="float-left">&nbsp;</div>
                <button type="submit" class="btn btn-success btn-icon-split float-left" name="save_stay">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar y permanecer</a>
                </button>
                <div class="float-left">&nbsp;</div>
                <button id="btnGuardar" type="submit" class="btn btn-primary btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</a>
                </button>
            </div>
        </form>
    </div>
</div>

@if($gallery->id)
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Imagenes</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <a id="btnCreatePhoto" class="btn btn-primary float-right" href="">
                    <i class="fas fa-fw fa-plus-square"></i> Agregar foto
                </a>
                <br />
            </div>
        </div>

        <div id="gallery_photos" class="row">
        @foreach($gallery->photos as $photo)
            <div class="col-md-3">
                <div class="item-galeria relativo">
                    <img class="img-responsive" src="{{ asset($photo->getOriginalImageUrl()) }}" alt="{{ $photo->title }}" title="{{ $photo->title }}" />
                    <div class="acciones">
                        <a rel="btnDeletePhoto" href="{{ route('admin::gallery_photo::delete', ['id' => $photo->id]) }}"><i class="fas fa-fw fa-trash"></i></a>
                        <a rel="btnEditPhoto" href="{{ route('admin::gallery_photo::edit', ['id' => $photo->id]) }}"><i class="fas fa-fw fa-edit"></i></a>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</div>

<div class="fade" id="item-source" data-url="{{ url('storage/images/galleries/' . $gallery->id) }}">
    <div class="col-md-3">
        <div class="item-galeria relativo">
            <img class="img-responsive" src="" alt="" title="" />
            <div class="acciones">
                <a rel="btnDeletePhoto" href="{{ route('admin::gallery_photo::delete', ['id' => 'xxx']) }}"><i class="fas fa-fw fa-trash"></i></a>
                <a rel="btnEditPhoto" href="{{ route('admin::gallery_photo::edit', ['id' => 'xxx']) }}"><i class="fas fa-fw fa-edit"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_photo" tabindex="-1" role="dialog" aria-labelledby="modal_photo_label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_photo_label">Agregar una nueva imagen</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_photo" name="form_photo" class="form-row" action="{{ route('admin::gallery_photo::save') }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" id="photo_id" name="id" value="" />
                    <input type="hidden" id="gallery_id" name="gallery_id" value="{{ $gallery->id }}" />
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                    <div class="col-sm-12 form-group">
                        <label for="titulo">Nombre</label>
                        <input type="text" name="title" id="photo_title" class="form-control" value="" />
                    </div>

                    <div class="col-sm-12 form-group">
                        <label for="photo_description">Descripci&oacute;n</label>
                        <textarea id='photo_description' name='description' class="form-control" rows="3"></textarea>
                    </div>

                    <div class="col-sm-12 form-group">
                        <label for="image">Archivo de imagen</label>
                        <input type="file" id="image" name="image" class="form-control" />
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
                <a id="btnSavePhoto" class="btn btn-primary btn-icon-split float-left" href="">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</a>
                </a>
            </div>
        </div>
    </div>
</div>
@endif

@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('back/vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('back/vendor/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('back/js/admin-gallery.js') }}"></script>
@stop
