@extends('backoffice.layouts.base')

@section('css')
<link href="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Gesti&oacute;n de men&uacute;s</h1>
<p class="mb-4">
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datos</h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ $edit ? route('admin::menu::update', ['id' => $menu->id]) : route('admin::menu::store') }}" class="form-row" enctype="multipart/form-data">
            <input type="hidden" id="id" name="id" value="{{ $menu->id }}" />
            @csrf

            <div class="form-group col-md-6">
                <label>Nombre</label>
                <input type="text" name="title" id="title" class="form-control" value="{{ old('title') ? old('title') : $menu->title }}" required="required" />
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Peso</label>
                <input type="text" name="weight" id="weight" class="form-control" value="{{ old('weight') ? old('weight') : $menu->weight }}" required="required" />
                @error('weight')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Descripci&oacute;n</label>
                <textarea name="description" id="description" class="form-control" required="required">{{ old('description') ? old('description') : $menu->description }}</textarea>
                @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group col-md-6">
                <label>Meta</label>
                <textarea name="meta" id="meta" class="form-control" required="required">{{ old('meta') ? old('meta') : $menu->meta }}</textarea>
                @error('meta')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group col-md-6">
                <label>Padre</label>
                <select name="menu_id" id="menu_id" class="form-control">
                    <option value="">---</option>
                    @foreach ($menus as $m)
                    <option value="{{ $m->id }}" @if($m->id == $menu->menu_id) selected @endif>{{ $m->title }}</option>
                    @endforeach
                </select>
                @error('menu_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Tipo</label>
                <select name="link_type" id="link_type" class="form-control">
                    @foreach ($types as $key => $type)
                    <option value="{{ $key }}" @if($key == $menu->link_type) selected @endif>{{ $type }}</option>
                    @endforeach
                </select>
                @error('link_type')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div id="blog_category_container" class="form-group col-md-6 d-none" rel="link-type">
                <label>Categoría de blog</label>
                <select name="blog_category_id" id="blog_category_id" class="form-control">
                    @foreach ($blogCategories as $category)
                    <option value="{{ $category->id }}" @if($category->id == $menu->link_id) selected @endif>{{ $category->name }}</option>
                    @endforeach
                </select>
                @error('blog_category_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            
            <div id="product_category_container" class="form-group col-md-6 d-none" rel="link-type">
                <label>Categoría de productos</label>
                <select name="product_category_id" id="product_category_id" class="form-control">
                    @foreach ($productCategories as $category)
                    <option value="{{ $category->id }}" @if($category->id == $menu->link_id) selected @endif>{{ $category->name }}</option>
                    @endforeach
                </select>
                @error('product_category_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div id="blog_content_container" class="form-group col-md-6 d-none" rel="link-type">
                <label>Post del blog</label>
                <select name="blog_content_id" id="blog_content_id" class="form-control">
                    @foreach ($posts as $post)
                    <option value="{{ $post->id }}" @if($post->id == $menu->link_id) selected @endif>{{ $post->title }}</option>
                    @endforeach
                </select>
                @error('blog_content_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div id="page_content_container" class="form-group col-md-6 d-none" rel="link-type">
                <label>Pagina personalizada</label>
                <select name="page_content_id" id="page_content_id" class="form-control">
                    @foreach ($pages as $page)
                    <option value="{{ $page->id }}" @if($page->id == $menu->link_id) selected @endif>{{ $page->title }}</option>
                    @endforeach
                </select>
                @error('page_content_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div id="url_container" class="form-group col-md-6 d-none" rel="link-type">
                <label>URL</label>
                <input type="text" name="url" id="url" class="form-control" value="{{ old('url') ? old('url') : $menu->url }}" />
                @error('url')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-12">
                <label>
                    <input type="checkbox" name="display" id="display" value="Y" @if($menu->display == 'Y') checked @endif />
                    Visible
                </label>                
            </div> 

            <div class="col-md-6">
                <a href="{{ route('admin::menu::list') }}" class="btn btn-danger btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Regresar</a>
                </a>
                <div class="float-left">&nbsp;</div>
                <button id="btnGuardar" type="submit" class="btn btn-primary btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</a>
                </button>
            </div>
        </form>
    </div>
</div>

@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('back/vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('back/vendor/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('back/js/admin-menu.js') }}"></script>
@stop
