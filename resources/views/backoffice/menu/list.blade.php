@extends('backoffice.layouts.base')

@section('css')
<link href="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Men&uacute;s</h1>
<p class="mb-4">
    <a href="{{ route('admin::menu::create') }}" class="btn btn-primary float-right"><i class="fas fa-fw fa-plus-square"></i> Crear men&uacute;</a>
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Men&uacute;s registrados</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Padre</th>
                        <th>Tipo</th>
                        <th>Url</th>
                        <th>Entidad Asociada</th>
                        <th class="no-sort">Acciones</th>
                    </tr>
                </thead>
                @php
                /*<tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>*/
                @endphp
                <tbody>
                    @foreach($menus as $menu)
                    <tr>
                        <td>{{ $menu->id }}</td>
                        <td>{{ $menu->title }}</td>
                        <td>@if(!empty($menu->parent)) {{ $menu->parent->title }} @else --- @endif</td>
                        <td>{{ $types[$menu->link_type] }}</td>
                        <td>{{ $menu->url }}</td>
                        <td>@if(!empty($menu->link)){{ $menu->link->title ?? $menu->link->name }}@endif</td>
                        <td>
                            <a href="{{ route('admin::menu::edit', ['id' => $menu->id]) }}" class="display-inline" data-toggle="tooltip" data-placement="top" title="Editar men&uacute;">
                                <i class="fas fa-fw fa-edit"></i>
                            </a>

                            <a href="{{ route('admin::menu::delete', ['id' => $menu->id]) }}" class="display-inline" rel="lnk-borrar" data-del-msg="&iquest;Est&aacute; seguro de borrar este men&uacute;?" data-toggle="tooltip" data-placement="top" title="Borrar men&uacute;">
                                <i class="fas fa-fw fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
@stop
