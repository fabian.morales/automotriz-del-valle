@extends('backoffice.layouts.base')

@section('css')
<link href="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Ver cotizaci&oacute;n</h1>
<p class="mb-4">
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datos del cliente</h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('admin::order::save') }}" class="form-row">
            <input type="hidden" id="id" name="id" value="{{ $order->id }}" />
            @csrf

            <div class="form-group col-md-6">
                <label>Fecha</label>
                <input type="text" name="fecha" id="fecha" class="form-control" value="{{ $order->created_at }}" required="required" readonly="readonly" />
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Nombre</label>
                <input type="text" name="name" id="name" class="form-control" value="{{ $order->name }}" required="required" readonly="readonly" />
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Correo</label>
                <input type="email" name="email" id="email" class="form-control" value="{{ $order->email }}" required="required" readonly="readonly" />
            </div>
            <div class="form-group col-md-6">
                <label>Celular</label>
                <input type="text" name="phone" id="phone" class="form-control" value="{{ $order->phone }}" required="required" readonly="readonly" />
            </div>

            <div class="form-group col-md-6">
                <label>Ciudad</label>
                <input type="text" name="city_name" id="city_name" class="form-control" value="{{ $order->city_name }}" required="required" readonly="readonly" />
            </div>
            <div class="form-group col-md-6">
                <label>Whatsapp</label>
                <input type="text" name="whatsapp" id="whatsapp" class="form-control" value="{{ $order->whatsapp == 'Y' ? 'Sí': 'No' }}" required="required" readonly="readonly" />
            </div>
            
            <div class="col-md-6">
                <a href="{{ route('admin::order::list') }}" class="btn btn-danger btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Regresar</a>
                </a>
                <div class="float-left">&nbsp;</div>
                <button type="submit" class="btn btn-success btn-icon-split float-left fade" name="save_stay">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar y permanecer</a>
                </button>
                <div class="float-left">&nbsp;</div>
                <button id="btnGuardar" type="submit" class="btn btn-primary btn-icon-split float-left fade">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</a>
                </button>
            </div>
        </form>
    </div>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Detalles</h6>
    </div>
    <div class="card-body">
        @foreach($order->details as $detail)
        <div class="row">
            <div class="col-sm-6 col-md-2">
                <img src="{{ asset($detail->product->getMainImage('thumbnail')) }}" class="img-fluid" alt="" />
            </div>
            <div class="col-sm-6 col-md-10">
                <h4>{{ $detail->product->name }}</h4>
                <hr />
                @if (sizeof($detail->attributeValues))
                    <h5>Atributos</h5>
                    <ul>
                        @foreach($detail->attributeValues as $value)
                        <li>{{ $value->attribute->name }}: {{ $value->label }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Mensajes</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <h5>Mensaje del cliente</h5>
                <textarea readonly="readonly" rows="5" class="form-control">{{ $order->message }}</textarea>
            </div>
            <div class="col-sm-12 fade">
                <hr />
                <h5>Responder</h5>
                <textarea class="form-control" rows="5"></textarea>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('back/vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('back/vendor/tinymce/jquery.tinymce.min.js') }}"></script>
@stop
