@extends('backoffice.layouts.base')

@section('css')
<link href="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Gesti&oacute;n de contenidos</h1>
<p class="mb-4">
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datos</h6>
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('admin::page_content::save') }}" class="form-row">
            <input type="hidden" id="id" name="id" value="{{ $page->id }}" />
            @csrf

            <div class="form-group col-md-6">
                <label>T&iacute;titulo</label>
                <input type="text" name="title" id="title" class="form-control" value="{{ old('title') ? old('title') : $page->title }}" required="required" />
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Ruta</label>
                <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug') ? old('slug') : $page->slug }}" readonly />
                @error('slug')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-12">
                <label>Cuerpo</label>
                <textarea rows="10" name="body" id="body" class="form-control" required="required">
                    {{ old('body') ? old('body') : $page->body }}
                </textarea>
                @error('body')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group col-md-1">
                <label>Peso</label>
                <input type="text" name="weight" id="weight" class="form-control" value="{{ old('weight') ? old('weight') : $page->weight }}" />
                @error('weight')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-11">&nbsp;</div>

            <div class="form-group col-md-12">
                <label>
                    <input type="checkbox" name="home" id="home" value="Y" @if($page->home == 'Y') checked @endif />
                    Mostrar en el inicio
                </label>
            </div>

            <div class="form-group col-md-12">
                <label>
                    <input type="checkbox" name="status" id="status" value="1" @if($page->status == 1) checked @endif />
                    Activo
                </label>                
            </div>
            
            <div class="col-md-6">
                <a href="{{ route('admin::page_content::list') }}" class="btn btn-danger btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Regresar</a>
                </a>
                <div class="float-left">&nbsp;</div>
                <button type="submit" class="btn btn-success btn-icon-split float-left" name="save_stay">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar y permanecer</a>
                </button>
                <div class="float-left">&nbsp;</div>
                <button id="btnGuardar" type="submit" class="btn btn-primary btn-icon-split float-left">
                    <span class="icon text-white-50">
                        <i class="fas fa-save"></i>
                    </span>
                    <span class="text">Guardar</a>
                </button>
            </div>
        </form>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('back/vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('back/vendor/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('back/js/admin-page-content.js') }}"></script>
@stop
