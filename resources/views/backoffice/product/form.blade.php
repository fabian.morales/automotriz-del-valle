@extends('backoffice.layouts.base')

@section('css')
<link href="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Gesti&oacute;n de productos</h1>
<p class="mb-4">
    &nbsp;
</p>

<form method="post" action="{{ route('admin::product::save') }}" class="" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $product->id }}" />
    @csrf

    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-data-tab" data-toggle="tab" href="#nav-data" role="tab" aria-controls="nav-data" aria-selected="true">Datos b&aacute;sicos</a>
            <a class="nav-item nav-link" id="nav-categories-tab" data-toggle="tab" href="#nav-categories" role="tab" aria-controls="nav-categories" aria-selected="false">Categor&iacute;as</a>
            <a class="nav-item nav-link" id="nav-images-tab" data-toggle="tab" href="#nav-images" role="tab" aria-controls="nav-images" aria-selected="false">Im&aacute;genes</a>
            <a class="nav-item nav-link" id="nav-attributes-tab" data-toggle="tab" href="#nav-attributes" role="tab" aria-controls="nav-attributes" aria-selected="false">Atributos</a>
            <a class="nav-item nav-link" id="nav-sheet-tab" data-toggle="tab" href="#nav-sheet" role="tab" aria-controls="nav-sheet" aria-selected="false">Ficha t&eacute;cnica</a>
            <a class="nav-item nav-link" id="nav-offer-tab" data-toggle="tab" href="#nav-offer" role="tab" aria-controls="nav-offer" aria-selected="false">Oferta</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-data" role="tabpanel" aria-labelledby="nav-data-tab">
            @include('backoffice.product.form_data', ['product' => $product])
        </div>
        <div class="tab-pane fade" id="nav-categories" role="tabpanel" aria-labelledby="nav-categories-tab">
            @include('backoffice.product.form_categories', ['product' => $product, 'categories' => $categories])
        </div>
        <div class="tab-pane fade" id="nav-images" role="tabpanel" aria-labelledby="nav-images-tab">
            @include('backoffice.product.form_images', ['product' => $product])
        </div>
        <div class="tab-pane fade" id="nav-attributes" role="tabpanel" aria-labelledby="nav-attributes-tab">
            @include('backoffice.product.form_combinations', ['product' => $product, 'attributes' => $attributes])
        </div>
        <div class="tab-pane fade" id="nav-sheet" role="tabpanel" aria-labelledby="nav-sheet-tab">
            @include('backoffice.product.form_sheet', ['product' => $product, 'features' => $features])
        </div>
        <div class="tab-pane fade" id="nav-offer" role="tabpanel" aria-labelledby="nav-offer-tab">
            @include('backoffice.product.form_offer', ['product' => $product])
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('admin::product::list') }}" class="btn btn-danger btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Regresar</a>
            </a>
            <div class="float-left">&nbsp;</div>
            <button type="submit" class="btn btn-success btn-icon-split float-left" name="save_stay">
                <span class="icon text-white-50">
                    <i class="fas fa-save"></i>
                </span>
                <span class="text">Guardar y permanecer</a>
            </button>
            <div class="float-left">&nbsp;</div>
            <button type="submit" class="btn btn-primary btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-save"></i>
                </span>
                <span class="text">Guardar</a>
            </button>
        </div>
    </div>
</form>
@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('back/vendor/dropzone.js') }}"></script>
<script src="{{ asset('back/js/admin-product.js') }}"></script>
@stop
