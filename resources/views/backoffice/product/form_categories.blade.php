<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Categor&iacute;as</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="col-sm-12">
                <p>Seleccione las categor&iacute;as por asignar a este producto</p>
                <br />
            </div>
            @foreach($categories as $category)
            <div class="form-group col-md-4 com-sm-6">
                <label>
                    <input type="checkbox" name="categories[]" id="category_{{ $category->id }}" value="{{ $category->id }}" @if($category->relationLoaded('products') && sizeof($category->products)) checked @endif data-name="{{ $category->name }}" data-main="{{ (int)(isset($main) && !empty($main) && $main->id == $category->id) }}" />
                    {{ $category->name }}
                </label>
            </div>
            @endforeach
        </div>
        <div class="form-row">
            <div class="col-sm-6 col-md-3">
                <p>Seleccione la categor&iacute;a principal</p>
                <br />
            </div>
            <div class="form-group col-md-3 com-sm-6">
                <label>
                    <select id="main_category" name="main_category">
                    </select>
                </label>
            </div>
        </div>
    </div>
</div>