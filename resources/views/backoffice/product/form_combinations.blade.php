<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Atributos</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="col-sm-12">
                <p>Seleccione la combinaci&oacute;n de atributos que desee a&ntilde;adir</p>
            </div>
            @foreach($attributes as $attribute)
            <div class="form-group col-md-4 com-sm-6">
                <fieldset>
                    <legend>{{ $attribute->name }}</legend>
                    <ul class="list">
                        @foreach($attribute->values as $value)
                        <li>
                            <label>
                                <input rel="combination" type="radio" value="{{ $value->id }}" id="attribute_value_{{ $value->id }}" name="attribute_value[{{ $attribute->id }}]" data-attribute="{{ $attribute->id }}" /> {{ $value->label }}
                            </label>
                        </li>
                        @endforeach
                    </ul>
                </fieldset>
            </div>
            @endforeach
            <div class="col-md-12">
                <button id="btnSaveCombination" class="btn btn-primary">A&ntilde;adir combinaci&oacute;n</button>
            </div>
        </div>
    </div>
</div>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Combinaciones de atributos del producto</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <table class="table table-bordered" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Atributos</th>
                        <th class="no-sort">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="tr-values" class="d-none">
                        <td data-id="id"></td>
                        <td data-id="attribute"></td>
                        <td>
                            <a href="{{ route('admin::product_combination::delete', ['id' => 'X']) }}" class="display-inline" rel="lnk-delete-combination" data-del-msg="&iquest;Est&aacute; seguro de borrar esta combinación de atributos del producto?" data-toggle="tooltip" data-placement="top" title="Borrar combinación">
                                <i class="fas fa-fw fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>