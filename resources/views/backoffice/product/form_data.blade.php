<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Datos</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Nombre</label>
                <input type="text" name="name" id="name" class="form-control" value="{{ old('name') ? old('name') : $product->name }}" required="required" />
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>C&oacute;digo</label>
                <input type="text" name="sku" id="sku" class="form-control" value="{{ old('sku') ? old('sku') : $product->sku }}" required="required" />
                @error('sku')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Ruta</label>
                <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug') ? old('slug') : $product->slug }}" readonly />
                @error('slug')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Descripci&oacute;n</label>
                <textarea name="description" id="description" class="form-control" required="required" rows="5">{{ old('description') ? old('description') : $product->description }}</textarea>
                @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-12">
                <label>
                    <input type="checkbox" name="featured" id="featured" value="Y" @if($product->featured == 'Y') checked @endif />
                    Destacado
                </label>
            </div>

            <div class="form-group col-md-12">
                <label>
                    <input type="checkbox" name="status" id="status" value="1" @if($product->status == 1) checked @endif />
                    Activo
                </label>                
            </div>            
        </div>
    </div>
</div>