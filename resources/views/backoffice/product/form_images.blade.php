<div id="drop_images">
    <div class="dz-message">Arrastra aqu&iacute; las im&aacute;genes o haz clic</div>
</div>

@if (!empty($product->images))
<div class="col-sm-6 col-md-3 col-lg-2 item_img_adm" id="item_img" style="display: none;">
    <input type="checkbox" name="delete_img[]" id="delete_img" value="" />
    <img src="" alt="{{ $product->name }}" class="img-responsive" />
</div>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Im&aacute;genes asignadas</h6>
    </div>
    <div class="card-body">
        <div id="images_gallery" class="row">
            @foreach($product->images as $image)
            <div class="col-sm-6 col-md-3 col-lg-2 item_img_adm">
                <input type="checkbox" name="delete_img[]" id="delete_img_{{ $image->id }}" value="{{ $image->id }}" />
                <img src="{{ asset($image->getImageUrl('thumbnail')) }}" alt="{{ $product->name }}" class="img-responsive" />
            </div>
            @endforeach
        </div>
        <button type="submit" class="btn btn-danger btn-icon-split float-left" id="btnDeleteImage">
            <span class="icon text-white-50">
                <i class="fas fa-trash"></i>
            </span>
            <span class="text">Borrar im&aacute;genes seleccionadas</a>
        </button>
    </div>
</div>
<div class="clearfix"></div>
<br />
@else
<p>Debe guardar primero el producto para poder asignar las im&aacute;genes</p>
<br />
@endif