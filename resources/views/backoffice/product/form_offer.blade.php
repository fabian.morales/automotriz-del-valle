<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Oferta</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-12">
                <label>
                    <input type="checkbox" name="offer" id="offer" value="Y" @if($product->offer == 'Y') checked @endif />
                    Habilitar producto como oferta
                </label>
            </div>

            <div class="form-group col-md-6">
                <label>Descripci&oacute;n de la oferta</label>
                <textarea name="offer_description" id="offer_description" class="form-control"  rows="5">{{ $product->offer_description }}</textarea>
            </div>
            <div class="col-md-6">&nbsp;</div>

            <div class="form-group col-md-6">
                <label>Imagen de la oferta</label>
                <input type="file" name="offer_file" id="offer_file" />
            </div>
            <div class="col-md-6">
                @if(!empty($product->offer_file))
                    <img width="200" src="{{ url('storage/images/products/' . $product->id . '/offer/' . $product->offer_file) }}" alt="Oferta" />
                @else
                    &nbsp;
                @endif
            </div>
        </div>
    </div>
</div>