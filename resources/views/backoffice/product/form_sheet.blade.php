<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Ficha t&eacute;cnica</h6>
    </div>
    <div class="card-body">
        <div class="form-row">
            @foreach($features as $feature)
            <div class="col-md-2">
                <label>{{ $feature->name }}</label>
            </div>
            <div class="form-group col-md-6">                
                <input type="text" name="features[{{ $feature->id }}]" id="feature_{{ $feature->id }}" class="form-control" @if($feature->relationLoaded('values') && sizeof($feature->values)) value="{{ $feature->values[0]->value }}" @else value="" @endif />
            </div>
            <div class="col-md-4">&nbsp;</div>
            @endforeach
        </div>
    </div>
</div>