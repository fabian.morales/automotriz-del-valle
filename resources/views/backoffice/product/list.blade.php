@extends('backoffice.layouts.base')

@section('css')
<link href="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Productos</h1>
<p class="mb-4 text-right">
    <a href="{{ route('admin::product::create') }}" class="btn btn-primary display-inline"><i class="fas fa-fw fa-plus-square"></i> Crear producto</a>
    <a href="{{ route('admin::product::all_gen_image') }}" class="btn btn-primary display-inline"><i class="fas fa-images"></i> Generar imágenes</a>
    <a href="{{ route('admin::product::massive') }}" class="btn btn-primary display-inline"><i class="fas fa-upload"></i> Carga masiva</a>
    &nbsp;
</p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Productos registrados</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Imagen</th>
                        <th>Destacado</th>
                        <th>Activo</th>
                        <th class="no-sort">Acciones</th>
                    </tr>
                </thead>
                @php
                /*<tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Destacado</th>
                        <th>Activo</th>
                        <th>Acciones</th>
                    </tr>
                </tfoot>*/
                @endphp
                <tbody>
                    @foreach($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->name }}</td>
                        <td class="text-center">@if(sizeof($product->images) && is_file($product->images[0]->getOriginalImagePath())) <img src="{{ asset($product->images[0]->getImageUrl('thumbnail')) }}" width="35" /> @endif</td>
                        <td>{{ $product->featured == 'Y' ? 'Si' : 'No' }}</td>
                        <td>{{ $product->status == 1 ? 'Si' : 'No' }}</td>
                        <td>
                            <a href="{{ route('admin::product::edit', ['id' => $product->id]) }}" class="display-inline" data-toggle="tooltip" data-placement="top" title="Editar producto">
                                <i class="fas fa-fw fa-edit"></i>
                            </a>

                            <a href="{{ route('admin::product::delete', ['id' => $product->id]) }}" rel="lnk-borrar" data-del-msg="&iquest;Est&aacute; seguro de borrar este producto?" class="display-inline" data-toggle="tooltip" data-placement="top" title="Borrar producto">
                                <i class="fas fa-fw fa-trash"></i>
                            </a>

                            <a href="{{ route('admin::product::gen_image', ['id' => $product->id]) }}" class="display-inline" data-toggle="tooltip" data-placement="top" title="Generar imágenes">
                                <i class="fas fa-images"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
@stop
