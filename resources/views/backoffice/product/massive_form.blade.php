@extends('backoffice.layouts.base')

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Carga masiva de productos</h1>
<p class="mb-4">
    &nbsp;
</p>

<form method="post" action="{{ route('admin::product::massive_upload') }}" class="" enctype="multipart/form-data">
    @csrf

    <div class="row">
        <div class="col-md-6 form-group">
            <label>Seleccione el archivo</label>
            <input type="file" name="products" id="products" class="form-control" />
        </div>
        <div class="col-md-6 form-group">
            <label>Separador</label>
            <input type="text" name="separator" id="separator" class="form-control" value=";" placeholder=";" />
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('admin::product::list') }}" class="btn btn-danger btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Regresar</a>
            </a>
            <div class="float-left">&nbsp;</div>
            <button type="submit" class="btn btn-primary btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-save"></i>
                </span>
                <span class="text">Cargar</a>
            </button>
        </div>
    </div>
</form>
@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('back/vendor/dropzone.js') }}"></script>
<script src="{{ asset('back/js/admin-product.js') }}"></script>
@stop
