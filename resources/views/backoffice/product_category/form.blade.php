@extends('backoffice.layouts.base')

@section('css')
<link href="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Gesti&oacute;n de categor&iacute;as de productos</h1>
<p class="mb-4">
    &nbsp;
</p>

<!-- DataTales Example -->
<form method="post" action="{{ route('admin::product_category::save') }}" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $category->id }}" />
    @csrf

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Datos</h6>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Nombre</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ old('name') ? old('name') : $category->name }}" required="required" />
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-6">&nbsp;</div>

                <div class="form-group col-md-6">
                    <label>Ruta</label>
                    <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug') ? old('slug') : $category->slug }}" readonly />
                    @error('slug')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-md-6">&nbsp;</div>

                <div class="form-group col-md-6">
                    <label>Categor&iacute;a padre</label>
                    <select name="product_category_id" id="product_category_id" class="form-control">
                        <option value="">---</option>
                        @foreach ($categories as $c)
                        <option value="{{ $c->id }}" @if($c->id == $category->product_category_id) selected @endif>{{ $c->name }}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Imagen</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Imagen</label>
                    <input type="file" name="image" id="image" />
                </div>
                <div class="col-md-6">
                    @if(!empty($category->image))
                        <img width="200" src="{{ asset($category->getOriginalImageUrl()) }}" alt="Imagen" />
                    @else
                        &nbsp;
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('admin::product_category::list') }}" class="btn btn-danger btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Regresar</a>
            </a>
            <div class="float-left">&nbsp;</div>
            <button type="submit" class="btn btn-primary btn-icon-split float-left">
                <span class="icon text-white-50">
                    <i class="fas fa-save"></i>
                </span>
                <span class="text">Guardar</a>
            </button>
        </div>
    </div>
</form>
@stop

@section('scripts')
<script src="{{ asset('back/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('back/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
@stop
