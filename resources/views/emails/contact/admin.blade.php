@extends('emails.layouts.admin')

@section('content')
    @component('emails.components.title')
        @slot('text')
            Nueva solicitud de contacto
        @endslot
    @endcomponent

    @component('emails.components.regular_text')
        @slot('text')
            Hola, se ha recibido una nueva solicitud de contacto con los siguientes datos:
        @endslot
    @endcomponent

    <ul>
        <li><b>Nombre</b>: {{ $contact->name }}</li>
        <li><b>Correo</b>: {{ $contact->email }}</li>
        <li><b>Asunto</b>: {{ $contact->subject }}</li>
        <li><b>Mensaje</b>: {{ $contact->message }}</li>
    </ul>
@stop