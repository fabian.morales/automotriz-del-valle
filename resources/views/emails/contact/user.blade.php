@extends('emails.layouts.user')

@section('content')
    @component('emails.components.title')
        @slot('text')
            Solicitud recibida
        @endslot
    @endcomponent

    @component('emails.components.regular_text')
        @slot('text')
            Hola {{ $contact->name }}.
        @endslot
    @endcomponent
    
    @component('emails.components.regular_text')
        @slot('text')
            Hemos recibido tu solicitud de contacto. En breve nos pondremos en contacto contigo.
        @endslot
    @endcomponent
@stop