@extends('emails.layouts.admin')

@section('content')
    @component('emails.components.title')
        @slot('text')
            Nueva solicitud de cotizaci&oacute;n
        @endslot
    @endcomponent

    @component('emails.components.regular_text')
        @slot('text')
            Hola, se ha recibido una solicitud de cotizaci&oacute;n con los siguientes datos:
        @endslot
    @endcomponent
    
    <ul>
        <li><b>Nombre</b>: {{ $order->name }}</li>
        <li><b>Correo</b>: {{ $order->email }}</li>
        <li><b>Celular</b>: {{ $order->phone }}</li>
        <li><b>Ciudad</b>: {{ $order->city_name }}</li>
    </ul>

    @if (!empty($order->details))
    <h5>Detalles</h5>
    <table style="width: 100%; border-left: solid; border-top: solid; border-width: 1px; border-color: #777;" cellspacing="0">
        <thead>
            <tr>
                <th colspan="2" style="text-align: center; border-right: solid; border-bottom: solid; border-width: 1px; border-color: #777; padding: 5px;">Producto</th>
                <th style="text-align: center; border-right: solid; border-bottom: solid; border-width: 1px; border-color: #777; padding: 5px;">Cantidad</th>
            </tr>
        </thead>
        <tbody>
            @foreach($order->details as $detail)
            <tr>
                <td style="border-right: solid; border-bottom: solid; border-width: 1px; border-color: #777; padding: 5px;">{{ $detail->product->sku }}</td>
                <td style="border-right: solid; border-bottom: solid; border-width: 1px; border-color: #777; padding: 5px;">
                    {{ $detail->product->name }} <br />
                    @foreach($detail->attributeValues as $value)
                        {{ $value->attribute->name }}: {{ $value->label }} <br />
                    @endforeach
                </td>
                <td style="border-right: solid; border-bottom: solid; border-width: 1px; border-color: #777; padding: 5px;">{{ $detail->quantity }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
    </p>
@stop