@extends('frontoffice.layouts.app')

@section('content')

@component('frontoffice.components.banner_area')
    @slot('title')
    {{ $category->name }}
    @endslot

    @slot('class')
    blog-category
    @endslot
@endcomponent

<!-- Start Blog Area -->
<section class="blog-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                @include("frontoffice.components.blog_content_list", ["posts" => $posts])

                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="pagination-area">
                            {{ $posts->links() }}
                            <!--a href="#" class="prev page-numbers">
                                <i class='bx bxs-chevron-left'></i>
                            </a>
                            <a href="#" class="page-numbers">1</a>
                            <span class="page-numbers current" aria-current="page">2</span>
                            <a href="#" class="page-numbers">3</a>
                            <a href="#" class="page-numbers">4</a>
                            <a href="#" class="next page-numbers">
                                <i class='bx bxs-chevron-right'></i>
                            </a-->
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-12">
                <aside class="widget-area">
                    @php
                    /*<section class="widget widget_search">
                        <h3 class="widget-title">Search</h3>

                        <form class="search-form">
                            <label>
                                <span class="screen-reader-text">Search for:</span>
                                <input type="search" class="search-field" placeholder="Search...">
                            </label>
                            <button type="submit">
                                <i class='bx bx-search-alt'></i>
                            </button>
                        </form>
                    </section>*/
                    @endphp

                    @if(!empty($categories))
                    <section class="widget widget_categories">
                        <h3 class="widget-title">Categor&iacute;as</h3>

                        <ul>
                            @foreach($categories as $category)
                            <li>
                                <a href="{{ route('front::blog::category', ['id' => $category->id, 'slug' => $category->slug]) }}">{{ $category->name }} <i class='bx bx-chevron-right'></i></a>
                            </li>
                            @endforeach
                        </ul>
                    </section>
                    @endif

                    @if(!empty($popularPosts))
                        @include('frontoffice.components.blog_content_aside_list', ["posts" => $popularPosts])
                    @endif

                    @php
                    /*<section class="widget widget_tag_cloud">
                        <h3 class="widget-title">Tags</h3>

                        <div class="tagcloud">
                            <a href="#">Brake Hoses</a>
                            <a href="#">Air Boxes</a>
                            <a href="#">Grilles</a>
                            <a href="#">Brake Rotors</a>
                            <a href="#">Cars</a>
                            <a href="#">Shop</a>
                            <a href="#">Engine</a>
                            <a href="#">Tayer</a>
                        </div>
                    </section>*/
                    @endphp
                </aside>
            </div>
        </div>
    </div>
</section>
<!-- End Blog Area -->

@stop