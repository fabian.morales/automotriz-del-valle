@extends('frontoffice.layouts.app')

@section('content')

@component('frontoffice.components.banner_area')
    @slot('title')
    {{ $post->title }}
    @endslot

    @slot('class')
    blog-content
    @endslot
@endcomponent

<!-- Start Blog Area -->
<section class="blog-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="blog-details-desc">
                    <div class="article-image">
                        <img src="{{ asset($post->getImageUrl('inner')) }}" alt="{{ $post->title }}">
                    </div>

                    <div class="article-content">
                        <div class="details-content">
                            <div class="post-meta">
                                Creado por<a href="#">{{ $post->author->name }}</a> {{ $post->created_at->format('j F, Y') }}
                            </div>
                            <h3>
                                {{ $post->title }}
                            </h3>
                            {!! $content !!}
                            <!--ul class="share-list">
                                <li>
                                    <span>Share:</span>
                                </li>
                                <li>
                                    <a href="#" target="_blank"><i class='bx bxl-facebook'></i></a>
                                </li>
                                <li>
                                    <a href="#" target="_blank"><i class='bx bxl-twitter'></i></a>
                                </li>
                                <li>
                                    <a href="#" target="_blank"><i class='bx bxl-pinterest-alt'></i></a>
                                </li>
                                <li>
                                    <a href="#" target="_blank"><i class='bx bxl-linkedin'></i></a>
                                </li>
                            </ul-->
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-12">
                <aside class="widget-area">
                    @php
                    /*<section class="widget widget_search">
                        <h3 class="widget-title">Search</h3>

                        <form class="search-form">
                            <label>
                                <span class="screen-reader-text">Search for:</span>
                                <input type="search" class="search-field" placeholder="Search...">
                            </label>
                            <button type="submit">
                                <i class='bx bx-search-alt'></i>
                            </button>
                        </form>
                    </section>*/
                    @endphp

                    @if(!empty($categories))
                    <section class="widget widget_categories">
                        <h3 class="widget-title">Categor&iacute;as</h3>

                        <ul>
                            @foreach($categories as $category)
                            <li>
                                <a href="{{ route('front::blog::category', ['id' => $category->id, 'slug' => $category->slug]) }}">{{ $category->name }} <i class='bx bx-chevron-right'></i></a>
                            </li>
                            @endforeach
                        </ul>
                    </section>
                    @endif

                    @if(!empty($popularPosts))
                        @include('frontoffice.components.blog_content_aside_list', ["posts" => $popularPosts])
                    @endif

                    @php
                    /*<section class="widget widget_tag_cloud">
                        <h3 class="widget-title">Tags</h3>

                        <div class="tagcloud">
                            <a href="#">Brake Hoses</a>
                            <a href="#">Air Boxes</a>
                            <a href="#">Grilles</a>
                            <a href="#">Brake Rotors</a>
                            <a href="#">Cars</a>
                            <a href="#">Shop</a>
                            <a href="#">Engine</a>
                            <a href="#">Tayer</a>
                        </div>
                    </section>*/
                    @endphp
                </aside>
            </div>
        </div>
    </div>
</section>
<!-- End Blog Area -->

@stop