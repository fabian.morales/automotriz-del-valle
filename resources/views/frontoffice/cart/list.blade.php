@extends('frontoffice.layouts.app')

@section('content')

@component('frontoffice.components.banner_area')
    @slot('title')
    Solicitud de pedido
    @endslot

    @slot('class')
    cart
    @endslot
@endcomponent
    <!-- Start Cart Area -->
    <section class="cart-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <form id="cart-list" action="{{ route('front::cart::update') }}" method="post">
                        <div class="cart-table table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Cantidad</th>
                                    </tr>
                                </thead>

                                <tbody>
                                @forelse ($cart as $detail)
									@php
                                    $product = $detail->product;
                                    @endphp

                                    <tr class="top-class">
                                        <td class="product-thumbnail">
                                            <a href="{{ route('front::cart::delete', ['id' => $detail->id]) }}" class="remove" data-id="{{ $detail->id }}"><i class='bx bx-x'></i></a>

                                            <a href="#">
                                                <img src="{{ asset($product->getMainImage('thumbnail')) }}" alt="{{ $product->name }}" />
                                            </a>
                                        </td>
                                        <td class="product-name">
                                            <a href="{{ $product->getLink() }}">{{ $product->name }}</a>
                                        </td>
                                        <td class="product-quantity">
                                            <div class="input-counter">
                                                <span class="minus-btn"><i class='bx bx-minus'></i></span>
                                                <input type="text" name="detail[{{ $detail->id }}]" value="{{ $detail->quantity }}" min="1">
                                                <span class="plus-btn"><i class='bx bx-plus'></i></span>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3" class="text-center">
                                            <strong>
                                                No hay productos agregados al pedido
                                            </strong>
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                        <div class="cart-buttons">
                            <div class="row align-items-center">
                                <div class="col-lg-7 col-sm-7 col-md-7">
                                @php
                                    /*<div class="shopping-coupon-code">
                                        <input type="text" class="form-control" placeholder="Coupon code" name="coupon-code" id="coupon-code">
                                        <button type="submit">Apply Coupon</button>
                                    </div>*/
                                @endphp
                                </div>

                                @if(!empty($cart) && sizeof($cart))
                                <div class="col-lg-5 col-sm-5 col-md-5 text-right">
                                    <a href="#" class="default-btn" id="btnUpdateCart">
                                        Actualizar pedido
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>

                @if(!empty($cart) && sizeof($cart))
                <div class="col-lg-4 col-md-12">
                    <div class="my-account-area">
                        <div class="login-form mb-30">
                            <h2>Solicitar pedido online</h2>
                            <form id="quote_form" action="{{ route('front::order') }}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-12 form-group">
                                        <!--label for="nombre" class="col-form-label-sm">Nombre</label-->
                                        <input type="text" id="name" name="name" placeholder="Nombre" class="form-control" data-error="Debes ingresar tu nombre" required />
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <!--label for="ciudad" class="col-form-label-sm">Ciudad</label-->
                                        <input type="text" id="city_name" name="city_name" placeholder="Ciudad" class="form-control" data-error="Debes ingresar la ciudad" required />
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <!--label for="correo" class="col-form-label-sm">Correo</label-->
                                        <input type="email" id="email" name="email" placeholder="Correo" class="form-control" data-error="Debes ingresar tu correo" required />
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <!--label for="celular" class="col-form-label-sm">Celular</label-->
                                        <input type="text" id="phone" name="phone" placeholder="Celular" class="form-control" data-error="Debes ingresar tu número de celular" required />
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <!--label for="mensaje" class="col-form-label-sm">Mensaje</label-->
                                        <textarea id="message" name="message" placeholder="Mensaje" rows="5" class="form-control"  data-error="Debes ingresar el mensaje" required></textarea>
                                    </div>
                                    <div class="col-sm-12 form-check mb-2">
                                        <label for="whatsapp" class="form-check-label"><input type="checkbox" name="whatsapp" id="whatsapp" value="Y" class="form-check-input" /> &iquest;Usas WhatsApp?</label>
                                    </div>
                                    <div class="col-sm-12 form-check mb-4">
                                        <label for="accept_terms" class="form-check-label"><input type="checkbox" name="accept_terms" id="accept_terms" value="Y" class="form-check-input" data-error="Debes aceptar los términos y condiciones" required /> Aceptar <a href="{{ route('front::tyc') }}" target="_blank">t&eacute;rminos y condiciones</a></label>
                                    </div>
                                    <div class="col-sm-12 form-group text-center">
                                        <a href="#" class="default-btn" id="btnSendQuote">
                                            Enviar solicitud de pedido
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </section>
    <!-- End Cart Area -->
@stop
