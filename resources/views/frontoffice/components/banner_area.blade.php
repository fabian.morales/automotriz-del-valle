<!-- Start Page Banner -->
<div class="page-banner-area {{ $class }}" @if(isset($styles) && !empty($styles)) {{ $styles }} @endif>
    <div class="d-table">
        <div class="d-table-cell">
            <div class="container">
                <div class="page-banner-content">
                    <h2>{{ $title }}</h2>
                    <ul>
                        <li>
                            <a href="{{ route("front::home") }}">Inicio</a>
                        </li>
                        <li>{{ $title }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Page Banner -->