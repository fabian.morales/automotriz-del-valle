<section class="widget widget_maxon_posts_thumb">
    <h3 class="widget-title">Posts Populares</h3>

    @foreach($popularPosts as $post)
    <article class="item">
        <a href="#" class="thumb">
            <span class="fullimage cover bg1" role="img" style="background: url({{ asset($post->getImageUrl('tiny')) }}) center center no-repeat; background-size: cover;"></span>
        </a>
        <div class="info">
            <span>{{ $post->created_at->format('j F, Y') }}</span>
            <h4 class="title usmall"><a href="{{ route("front::blog::post", ["id" => $post->id, "slug" => $post->slug]) }}">{{ $post->title }}</a></h4>
        </div>
    </article>
    @endforeach
    </article>
</section>