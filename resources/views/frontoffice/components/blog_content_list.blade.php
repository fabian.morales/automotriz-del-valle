<div class="row">
    @if (!empty($posts))

    @foreach ($posts as $post)
    <div class="col-lg-4 col-md-6">
        <div class="single-blog">
            <div class="blog-image">
                <a href="{{ route("front::blog::post", ["id" => $post->id, "slug" => $post->slug]) }}">
                    <img src="{{ asset($post->getImageUrl('thumbnail')) }}" alt="{{ $post->title }}">
                </a>
                
                <div class="tag">{{ $post->created_at->format('j F, Y') }}</div>
            </div>
            <div class="blog-content">
                <span>Creado por <a href="#">{{ $post->author->name }}</a></span>
                <h3>
                    <a href="{{ route("front::blog::post", ["id" => $post->id, "slug" => $post->slug]) }}">{{ $post->title }}</a>
                </h3>
                <a href="{{ route("front::blog::post", ["id" => $post->id, "slug" => $post->slug]) }}" class="blog-btn">Leer m&aacute;s</a>
            </div>
        </div>
    </div>
    @endforeach

    @endif
</div>