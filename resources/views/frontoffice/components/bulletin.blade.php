<!-- Start Newsletter Area -->
<div class="newsletter-area ptb-100">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="newsletter-content">
                    <!--span>Suscribirse al bolet&iacute;n</span-->
                    <h3>Suscribirse al bolet&iacute;n</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>

                    <form class="newsletter-form" action="{{ route('front::bulletin') }}">
                        @csrf
                        <input type="email" class="input-newsletter" placeholder="Ingrese el correo" id="bulletin-email" name="email" required autocomplete="off" />
                        <button id="btnBulletinSend" type="submit" class="captcha-submit">Suscribirse</button>
                        @if(App::environment(['staging', 'production']))
                            {!! Captcha::renderButton() !!}
                        @endif
                    </form>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="newsletter-image wow fadeInRight" data-wow-delay=".3s">
                    <img src="{{ asset('front/assets/img/newsletter/newsletter.png') }}" alt="Bolet&iacute;n">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Newsletter Area -->
