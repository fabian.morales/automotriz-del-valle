<!-- Start Coming Soon Area -->
<section class="coming-soon-area ptb-100">
    <div class="container">
        <div class="col-lg-12">
            <div class="coming-soon-title">
                <h3>Somos importadores directos</h3>
                <h4>Distribuimos e importamos diferentes marcas de repuestos, principalmente de la marca GRAVAL.</h4>

                <div class="coming-soon-btn">
                    <a href="https://wa.link/w953b2" target="_blank" class="text-uppercase default-btn">Comuníquese con nosotros para más información</a>
                </div>

{{--                <div class="coming-soon-content">--}}
{{--                    <div id="timer" class="flex-wrap d-flex justify-content-center">--}}
{{--                        <div id="days" class="align-items-center flex-column d-flex"></div>--}}
{{--                        <div id="hours" class="align-items-center flex-column d-flex"></div>--}}
{{--                        <div id="minutes" class="align-items-center flex-column d-flex"></div>--}}
{{--                        <div id="seconds" class="align-items-center flex-column d-flex"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
</section>
<!-- End Coming Soon Area -->
