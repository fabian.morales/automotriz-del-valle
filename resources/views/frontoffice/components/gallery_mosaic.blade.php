<div class="container_full">
    <div class="row no-gutter">
        @if(!empty($gallery->photos))
        
        @foreach($gallery->photos as $photo)
        <div class="col-sm-6 col-md-3">
            <div class="item photo">
                <a href="{{ asset($photo->getOriginalImageUrl()) }}" data-featherlight>
                    <img src="{{ asset($photo->getOriginalImageUrl()) }}" title="{{ $photo->description }}" alt="{{ $photo->title }}" />
                </a>
            </div>
        </div>
        @endforeach
        
        @endif
    </div>
</div>