<ul class="middle-header-optional">
    <!--li>
        <a href="wishlist.html"><i class="flaticon-heart"><span>0</span></i> Wishlist</a>
    </li-->
    <li>
        <a href="{{ route('front::cart::list') }}">
            <i class="flaticon-shopping-cart">
                <span id="count-cart">{{ \App\Helpers\CartHelper::getTotalCount() }}</span></i>
            Su pedido
        </a>
    </li>
    @php
    /*<li>
        <a href="my-account.html" class="user-btn"><i class='flaticon-enter'></i>Login / Register</a>
    </li>*/
    @endphp
</ul>
