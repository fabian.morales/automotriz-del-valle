<ul class="navbar-nav m-auto">
    @foreach($menu as $item)
    @include('frontoffice.components.main_menu_item', ['item' => $item, 'first' => true])
    @endforeach
</ul>