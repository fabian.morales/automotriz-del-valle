<li class="nav-item">
    <a href="{{ $item->link }}" class="nav-link">
        {{ $item->title }} 
        @if(!empty($item->children))
        <i class='bx @if($first) bx-chevron-down @else bx-chevron-right @endif'></i>
        @endif
    </a>
    @if(!empty($item->children))
    <ul class="dropdown-menu">
        @foreach($item->children as $child)
        @include('frontoffice.components.main_menu_item', ['item' => $child, 'first' => false])
        @endforeach
    </ul>
    @endif
</li>