<!-- Start Main Slides Area -->
<div class="main-slides">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12"></div>

            <div class="col-lg-9 col-md-12">
                @if(!empty($gallery->photos))
                <div class="home-slides owl-carousel owl-theme">
                    @foreach($gallery->photos as $photo)
                    <div class="main-slides-item item" style="background: url({{ asset($photo->getOriginalImageUrl()) }}) center center no-repeat; background-size: cover;">
                        <div class="main-slides-content">
                           {{-- <span>{{ $photo->title }}</span>
                           <h1>{{ $photo->description }}</h1> --}}

                            <div class="slides-btn">
                                <br />
                                <br />
                                <br />
                                {{-- <a href="#" class="default-btn">
                                    Ver productos
                                </a>
                                <a class="optional-btn" href="https://wa.link/2dixfr" target="_blank">
                                    Solicitar asesoría online
                                </a> --}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- End Main Slides Area -->
