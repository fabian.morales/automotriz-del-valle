<div class="categories-row">
    @if (!empty($categories))

    @foreach ($categories as $category)
    <div class="categories-item">
        <div class="single-categories">
            <div class="categories-image">
                <a href="{{ route('front::product::category', ['id' => $category->id, 'slug' => $category->slug]) }}">
                    <img src="{{ $category->getImageUrl('thumbnail') }}" alt="{{ $category->name }}">
                </a>
            </div>

            <div class="categories-content">
                <h3>
                    <a href="{{ route('front::product::category', ['id' => $category->id, 'slug' => $category->slug]) }}">{{ $category->name }}</a>
                </h3>
            </div>
        </div>
    </div>
    @endforeach

    @endif
</div>