<div class="navbar-category">
    <div class="category-title">
        <a href="#">Categor&iacute;as</a>
    </div>

    <div class="navbar-category-dropdown">
        <ul class="category-list">
            @foreach($categories as $category)
            <li><a href="{{ route("front::product::category", ["id" => $category->id, "slug" => $category->slug]) }}">{{ $category->name }}</a></li>
            @endforeach
        </ul>
    </div>
</div>