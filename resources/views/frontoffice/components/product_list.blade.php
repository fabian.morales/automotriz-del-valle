<div class="col-lg-9 col-md-12">
    @if (!empty($products))

    @foreach($products as $product)
    <div class="shop-item-box">
        <div class="row align-items-center">
            <div class="col-lg-3 col-sm-3">
                <div class="shop-image">
                    <a href="{{ $product->getLink() }}">
                        <img src="{{ asset($product->getMainImage('thumbnail')) }}" alt="{{ $product->name }}">
                    </a>
                </div>
            </div>

            <div class="col-lg-6 col-sm-6">
                <div class="shop-content">
                    <h3>
                        <a href="{{ $product->getLink() }}">{{ $product->name }}</a>
                    </h3>
                    <div class="rating">
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <!--a href="#" class="rating-count">3 reviews</a-->
                    </div>

                    @if (!empty($product->productFeatures))
                    <ul class="shop-list">
                        @foreach ($product->productFeatures as $feature)
                        <li>{{ $feature->feature->name }}: {{ $feature->value }}</li>
                        @endforeach
                    </ul>
                    @endif
                </div>
            </div>

            <div class="col-lg-3 col-sm-3">
                <ul class="shop-btn-list">
                    @php
                    /*<li>
                        <span>$189.00</span>
                    </li>*/
                    @endphp
                    <li>
                        <a href="{{ $product->getLink() }}">Ver detalles</a>
                    </li>
                    @php
                    /*<li>
                        <a href="wishlist.html">Add To Wishlist</a>
                    </li>
                    <li>
                        <a href="compare.html">Add To Compare</a>
                    </li>*/
                    @endphp
                </ul>
            </div>
        </div>
    </div>
    @endforeach

    @endif
</div>