<div class="middle-header-search">
    <form class="search-form" id="search-form" method="post" action="{{ route("front::search::products") }}">
        @csrf
        <label>
            <span class="screen-reader-text">Buscar:</span>
            <div class="typeahead-wrapper">
                <input type="search" class="search-field" id="search-products" placeholder="Buscar" name="text_search" value="{{ $textSearch }}" autocomplete="off" data-autocomplete="{{ route("front::product_names") }}">
            </div>
        </label>

        <button type="submit">
            <i class='bx bx-search-alt'></i>
        </button>
    </form>
</div>