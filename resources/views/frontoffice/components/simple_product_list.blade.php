<div class="row">
@if (!empty($products))

@foreach ($products as $product)
<div class="col-lg-3 col-sm-6">
    <div class="single-products">
        <div class="products-image">
            <a href="{{ $product->getLink() }}"><img src="{{ asset($product->getMainImage('thumbnail')) }}" alt="{{ $product->name }}"></a>
            <div class="tag">En venta</div>
            <!--ul class="products-action">
                <li>
                    <a href="cart.html">
                        <i class="flaticon-shopping-cart"></i>
                    </a>
                </li>
                <li>
                    <a href="wishlist.html"><i class="flaticon-heart"></i></a>
                </li>
            </ul-->
        </div>

        <div class="products-content">
            <h3>
                <a href="{{ $product->getLink() }}">{{ $product->name }}</a>
            </h3>
            <ul class="rating">
                <li><i class='bx bxs-star'></i></li>
                <li><i class='bx bxs-star'></i></li>
                <li><i class='bx bxs-star'></i></li>
                <li><i class='bx bxs-star'></i></li>
                <li><i class='bx bx-star'></i></li>
            </ul>
            @php /*<span>$ {{ $product->price }}</span>*/ @endphp
        </div>
    </div>
</div>
@endforeach

@endif
</div>