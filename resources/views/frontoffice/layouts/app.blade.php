<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@inject('widget', 'App\Services\WidgetsService')
@php
    $isHome = Request::is('/');
@endphp
<head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MLWXRDF');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    {!! SEOMeta::generate() !!}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset("front/assets/css/bootstrap.min.css") }}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset("front/assets/css/animate.min.css") }}">
    <!-- Meanmenu CSS -->
    <link rel="stylesheet" href="{{ asset("front/assets/css/meanmenu.css") }}">
    <!-- Boxicons CSS -->
    <link rel="stylesheet" href="{{ asset("front/assets/css/boxicons.min.css") }}">
    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="{{ asset("front/assets/css/flaticon.css") }}">
    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ asset("front/assets/css/owl.carousel.min.css") }}">
    <!-- Owl Theme Default CSS -->
    <link rel="stylesheet" href="{{ asset("front/assets/css/owl.theme.default.min.css") }}">
    <!-- Odometer CSS -->
    <link rel="stylesheet" href="{{ asset("front/assets/css/odometer.min.css") }}">
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{ asset("front/assets/css/nice-select.min.css") }}">
    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="{{ asset("front/assets/css/magnific-popup.min.css") }}">
    <!-- Imagelightbox CSS -->
    <link rel="stylesheet" href="{{ asset("front/assets/css/imagelightbox.min.css") }}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset("front/assets/css/style.css") }}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset("front/assets/css/responsive.css") }}">
    <link rel="stylesheet" href="{{ asset("front/assets/css/custom.css") }}"> --}}

    {{-- <link rel="preload" href="{{ asset('front/assets/css/app.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link href="{{ asset('front/assets/css/app.css') }}" rel="stylesheet" media="screen" /></noscript>

    <link rel="preload" href="{{ asset('front/assets/css/app-mobile.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link href="{{ asset('front/assets/css/app-mobile.css') }}" rel="stylesheet" media="screen" /></noscript>

    <link rel="preload" href="{{ asset('front/assets/css/app-print.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link href="{{ asset('front/assets/css/app-print.css') }}" rel="stylesheet" media="print" /></noscript> --}}

    <link href="{{ asset('front/assets/css/app.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('front/assets/css/app-mobile.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('front/assets/css/app-print.css') }}" rel="stylesheet" media="print" />

    <link rel="icon" type="image/png" href="{{ asset("front/assets/img/favicon.png") }}">
    <link rel="icon" type="image/x-icon" href="{{ asset("front/assets/img/favicon.ico") }}">

    @section('css')
    @show

    @if(App::environment(['staging', 'production']))

    @endif
</head>
<body data-scroll-animation="true">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MLWXRDF"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <!-- Start Preloader Area -->
    <div class="preloader">
        <div class="loader">
            <div class="sbl-half-circle-spin">
                <div></div>
            </div>
        </div>
    </div>
    <!-- End Preloader Area -->

    <!-- Start Top Header Area -->
    <div class="top-header-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12">
                    <ul class="top-header-information">
                        <li>
                            <i class="flaticon-pin"></i>
                            Cra 1H # 60 - 40, Cali - Valle del Cauca
                        </li>
                        <li>
                            <i class="flaticon-clock"></i>
                            Horario de atención: Lunes a Viernes 8:00am a 6:00pm, Sábados 8:00am a 3:00pm
                        </li>
                    </ul>
                </div>

                <div class="col-lg-6 col-md-12">
                @php
                    /*<ul class="top-header-optional">
                        <li>Currency: <b>USD</b></li>

                        <li>
                            <div class="dropdown language-switcher d-inline-block">
                                <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span>Language <i class='bx bx-chevron-down'></i></span>
                                </button>

                                <div class="dropdown-menu">
                                    <a href="#" class="dropdown-item d-flex align-items-center">
                                        <img src="assets/img/english.png" class="shadow-sm" alt="flag">
                                        <span>English</span>
                                    </a>
                                    <a href="#" class="dropdown-item d-flex align-items-center">
                                        <img src="assets/img/arab.png" class="shadow-sm" alt="flag">
                                        <span>العربيّة</span>
                                    </a>
                                    <a href="#" class="dropdown-item d-flex align-items-center">
                                        <img src="assets/img/germany.png" class="shadow-sm" alt="flag">
                                        <span>Deutsch</span>
                                    </a>
                                    <a href="#" class="dropdown-item d-flex align-items-center">
                                        <img src="assets/img/portugal.png" class="shadow-sm" alt="flag">
                                        <span>Português</span>
                                    </a>
                                    <a href="#" class="dropdown-item d-flex align-items-center">
                                        <img src="assets/img/china.png" class="shadow-sm" alt="flag">
                                        <span>简体中文</span>
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>*/
                @endphp
                </div>
            </div>
        </div>
    </div>
    <!-- End Top Header Area -->

    <!-- Start Middle Header Area -->
    <div class="middle-header-area">
        <div class="container">
            <div class="row align-items-center">
                {{-- @if($isHome) --}}
                <div class="col-lg-3">
                    <div class="middle-header-logo">
                        <a href="{{ route("front::home") }}"><img src="{{ asset("front/assets/img/logo.png") }}" alt="logo"></a>
                    </div>
                </div>
                {{-- @endif --}}

                <div class="col-lg-4 offset-md-1">
                {{-- <div class="@if($isHome) col-lg-4 @else col-lg-6 @endif"> --}}
                    @include('frontoffice.components.search_form', ['textSearch' => Request::get('text_search', '') ])
                </div>

                <div class="col-lg-4">
                {{-- <div class="@if($isHome) col-lg-5 @else col-lg-6 @endif"> --}}
                    @include('frontoffice.components.header_widgets')
                </div>
            </div>
        </div>
    </div>
    <!-- End Middle Header Area -->

    <!-- Start Navbar Area -->
    <div class="navbar-area">
        <div class="main-responsive-nav">
            <div class="container">
                <div class="main-responsive-menu">
                    <div class="logo">
                        <a href="{{ route("front::home") }}">
                            <img src="{{ asset("front/assets/img/logo.png") }}" alt="image">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-navbar">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light">
                    @if ($isHome)
                    @include("frontoffice.components.product_category_menu", ["categories" => $productCategories])

                    <a class="navbar-brand" href="{{ route("front::home") }}">
                        <img src="{{ asset("front/assets/img/logo_grupo_blanco.png") }}" alt="image">
                    </a>
                    @else
                    <a class="navbar-brand" href="{{ route("front::home") }}">
                        <img src="{{ asset("front/assets/img/logo.png") }}" alt="image">
                    </a>
                    @endif

                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                        @include("frontoffice.components.main_menu", ["menu" => $widget->makeMenu()])

                        <div class="others-option d-flex align-items-center">
                            <div class="option-item">
                                <span>
                                    Asesoría Online
                                    <a href="https://wa.link/w953b2" target="_blank">
                                        <i class="bx bxl-whatsapp"></i>
                                        313 391 45 51
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

        <div class="others-option-for-responsive">
            <div class="container">
                <div class="dot-menu">
                    <div class="inner">
                        <div class="circle circle-one"></div>
                        <div class="circle circle-two"></div>
                        <div class="circle circle-three"></div>
                    </div>
                </div>

                <div class="container">
                    <div class="option-inner">
                        <div class="others-option d-flex align-items-center">
                            <span>
                                    Asesoría Online
                                    <a href="https://wa.link/w953b2" target="_blank">
                                        <i class="bx bxl-whatsapp"></i>
                                        313 391 45 51
                                    </a>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Navbar Area -->

    @if(session('message_error'))
    @component('frontoffice.components.alert')
        @slot('title')
        {{ __("Error") }}
        @endslot

        @slot('class')
        danger
        @endslot

        @slot('message')
        {{ session('message_error') }}
        @endslot
    @endcomponent
    @endif

    @if(session('message_success'))
    @component('frontoffice.components.alert')
        @slot('title')
        {{ __("Aviso") }}
        @endslot

        @slot('class')
        success
        @endslot

        @slot('message')
        {{ session('message_success') }}
        @endslot
    @endcomponent
    @endif

    @if(session('status'))
    @component('frontoffice.components.alert')
        @slot('title')
        {{ __("Aviso") }}
        @endslot

        @slot('class')
        success
        @endslot

        @slot('message')
        {{ session('status') }}
        @endslot
    @endcomponent
    @endif

    @if(session('status.error'))
    @component('frontoffice.components.alert')
        @slot('title')
        {{ __("Error") }}
        @endslot

        @slot('class')
        danger
        @endslot

        @slot('message')
        {{ session('status.error') }}
        @endslot
    @endcomponent
    @endif

    @yield('content')

    <!-- Start Footer Area -->
    <section class="footer-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-sm-6">
                    <div class="single-footer-widget">
                        <a href="{{ route("front::home") }}">
                            <img src="{{ asset("front/assets/img/logo_grupo_blanco.png") }}" alt="image">
                        </a>

                        <p>Somos una empresa especializada en la venta, distribución y comercialización de partes y repuestos de aire acondicionado para la línea automotriz.</p>

                        <ul class="footer-social">
                            <li>
                                <a href="https://www.facebook.com/Grupoautomotrizdelvalle/" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/aireautomotrizvalle/" target="_blank">
                                    <i class='bx bxl-instagram'></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://wa.link/w953b2" target="_blank">
                                    <i class='bx bxl-whatsapp'></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 col-sm-6">
                    &nbsp;
                </div>

{{--                <div class="col-lg-3 col-sm-6">--}}
{{--                    <div class="single-footer-widget pl-5">--}}
{{--                        <h2>Quick Links</h2>--}}

{{--                        <ul class="quick-links">--}}
{{--                            <li>--}}
{{--                                <i class='bx bxs-chevrons-right'></i>--}}
{{--                                <a href="#">About Company</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class='bx bxs-chevrons-right'></i>--}}
{{--                                <a href="#">Services</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class='bx bxs-chevrons-right'></i>--}}
{{--                                <a href="#">Shop</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class='bx bxs-chevrons-right'></i>--}}
{{--                                <a href="#">FAQ</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class='bx bxs-chevrons-right'></i>--}}
{{--                                <a href="#">Blog</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class='bx bxs-chevrons-right'></i>--}}
{{--                                <a href="#">Gallery</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <div class="col-lg-3 col-sm-6">
                    <div class="single-footer-widget">
                        <h2>Información de contacto</h2>

                        <ul class="footer-contact-info">
                            <li>
                                <i class='bx bxs-phone'></i>
                                <span>Teléfono</span>
                                <a href="tel:+5723748695">(2) 374 86 95</a>
                            </li>
                            <li>
                                <i class='bx bxl-whatsapp'></i>
                                <span>Celular</span>
                                <a href="tel:+573133914551">313 391 45 51</a>
                            </li>
                            <li>
                                <i class='bx bx-envelope'></i>
                                <span>Email</span>
                                <a href="mailto:info@grupoautomotrizdelvalle.com">info@grupoautomotrizdelvalle.com</a>
                            </li>
                            <li>
                                <i class='bx bx-map'></i>
                                <span>Dirección</span>
                                Cra 1H # 60 - 40, Cali - Valle del cauca
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Footer Area -->

    <!-- Start Copy Right Area -->
    <div class="copyright-area">
        <div class="container">
            <div class="copyright-area-content">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6">
                        <p>
                            Copyright @ {{ date('Y') }} {{ config('app.name') }}. Todos los derechos reservados
                        </p>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <ul>
                            <li>
                                <a href="{{ route('front::tyc') }}">T&eacute;rminos y condiciones</a>
                            </li>
                            <li>
                                <a href="{{ route('front::privacy_policy') }}">Pol&iacute;tica de privacidad</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Copy Right Area -->

    <!-- Start Go Top Area -->
    <div class="go-top">
        <i class='bx bx-up-arrow-alt'></i>
    </div>
    <!-- End Go Top Area -->

    <div class="d-inline d-md-none whatsapp-button">
        <div>
            <a href="https://wa.link/w953b2" target="_blank">
                <img class="d-inline" src="{{ asset('front/assets/img/whatsapp-logo-64.png') }}" alt="" />
                <medium class="d-line">Compre con un experto</medium>
            </a>
        </div>
    </div>

    <div id="modal-container" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="modal-message"></p>
                </div>
            </div>
        </div>
    </div>

    @section('modals')
    @show

    <!-- Jquery Slim JS -->
    <script src="{{ asset("front/assets/js/jquery.min.js") }}"></script>
    <!-- Popper JS -->
    <script src="{{ asset("front/assets/js/popper.min.js") }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset("front/assets/js/bootstrap.min.js") }}"></script>
    <!-- Meanmenu JS -->
    <script src="{{ asset("front/assets/js/jquery.meanmenu.js") }}"></script>
    <!-- Owl Carousel JS -->
    <script src="{{ asset("front/assets/js/owl.carousel.min.js") }}"></script>
    <!-- Magnific Popup JS -->
    <script src="{{ asset("front/assets/js/jquery.magnific-popup.min.js") }}"></script>
    <!-- Imagelightbox JS -->
    <script src="{{ asset("front/assets/js/imagelightbox.min.js") }}"></script>
    <!-- Odometer JS -->
    <script src="{{ asset("front/assets/js/odometer.min.js") }}"></script>
    <!-- Jquery Nice Select JS -->
    <script src="{{ asset("front/assets/js/jquery.nice-select.min.js") }}"></script>
    <!-- Jquery Appear JS -->
    <script src="{{ asset("front/assets/js/jquery.appear.min.js") }}"></script>
    @php
    /*<!-- Ajaxchimp JS -->
    <script src="{{ asset("front/assets/js/jquery.ajaxchimp.min.js") }}"></script>
    <!-- Form Validator JS -->
    <script src="{{ asset("front/assets/js/form-validator.min.js") }}"></script>
    <!-- Contact JS -->
    <script src="{{ asset("front/assets/js/contact-form-script.js") }}"></script>*/
    @endphp
    <!-- Wow JS -->
    <script src="{{ asset("front/assets/js/wow.min.js") }}"></script>
    <!-- Custom JS -->
    <script src="{{ asset("front/assets/js/typehead/typeahead.bundle.min.js") }}"></script>
    <script src="{{ asset("front/assets/js/typehead/bloodhound.min.js") }}"></script>
    <script src="{{ asset("front/assets/js/main.js") }}"></script>
    <script src="{{ asset("front/assets/js/app.js") }}"></script>

    @section('js')
    @show
</body>
</html>
