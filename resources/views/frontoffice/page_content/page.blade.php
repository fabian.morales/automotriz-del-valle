@extends('frontoffice.layouts.app')

@section('content')

@component('frontoffice.components.banner_area')
    @slot('title')
    {{ $page->title }}
    @endslot

    @slot('class')
    content-page
    @endslot
@endcomponent

<div class="container ptb-100">
    <div class="row">
        {!! $content !!}
    </div>
</div>
@stop

