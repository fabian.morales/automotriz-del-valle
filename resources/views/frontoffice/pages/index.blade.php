@extends('frontoffice.layouts.app')

{{-- @section('js')
@if(App::environment(['staging', 'production']))
{!! Captcha::renderJs() !!}
@endif
@stop --}}

@section('content')
@inject('widget', 'App\Services\WidgetsService')
{!! $widget->renderGallery("galeria_01", "main_slider") !!}

<!-- Start Support Area -->
<section class="support-area">
    <div class="container">
        <div class="custom-row">
            <div class="custom-item">
                <div class="single-support">
                    <div class="icon">
                        <i class="flaticon-free-delivery"></i>
                    </div>

                    <div class="support-content">
                        <h3>Domicilio gratis en cali</h3>
{{--                        <span>Por compras mayores a $30.000</span>--}}
                    </div>
                </div>
            </div>

            <div class="custom-item">
                <div class="single-support">
                    <div class="icon">
                        <i class="flaticon-return-of-investment"></i>
                    </div>

                    <div class="support-content">
                        <h3>Envíos a nivel nacional</h3>
{{--                        <span>World Wide</span>--}}
                    </div>
                </div>
            </div>

            <div class="custom-item">
                <div class="single-support">
                    <div class="icon">
                        <i class="flaticon-online-payment"></i>
                    </div>

                    <div class="support-content">
                        <h3>Importadores directos</h3>
{{--                        <span>World Wide</span>--}}
                    </div>
                </div>
            </div>

            <div class="custom-item">
                <div class="single-support">
                    <div class="icon">
                        <i class="flaticon-online-support"></i>
                    </div>

                    <div class="support-content">
                        <h3>Asesoría técnica online</h3>
{{--                        <span>World Wide</span>--}}
                    </div>
                </div>
            </div>

            <div class="custom-item">
                <div class="single-support">
                    <div class="icon">
                        <i class="flaticon-award"></i>
                    </div>

                    <div class="support-content">
                        <h3>Productos Garantizados</h3>
{{--                        <span>World Wide</span>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Support Area -->

<!-- Start Products Area -->
<section class="products-area pt-100 pb-70">
    <div class="container">
        <div class="section-title">
            <h2>Productos</h2>
            <p>Encuentre aquí todo lo que necesita para el sistema de aire acondicionado para la línea automotriz (Vehículos de carga pesada, Maquinaria Agrícola, Volquetas, Camiones, Camionetas).</p>
        </div>

        <div class="tab products-list-tab">
            <ul class="tabs">
                @if(sizeof($featuredProducts))
                <li>
                    <a href="#">Destacados</a>
                </li>
                @endif
                @if(sizeof($popularProducts))
                <li>
                    <a href="#">Populares</a>
                </li>
                @endif
            </ul>

            <div class="tab_content">
                @if(sizeof($featuredProducts))
                <div class="tabs_item">
                    @include('frontoffice.components.simple_product_list', ['products' => $featuredProducts])
                </div>
                @endif

                @if(sizeof($popularProducts))
                <div class="tabs_item">
                    @include('frontoffice.components.simple_product_list', ['products' => $popularProducts])
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
<!-- End Products Area -->

@include('frontoffice.components.event')

{{--<!-- Start Categories Area -->--}}
{{--<section class="categories-area pt-100 pb-100">--}}
{{--    <div class="container">--}}
{{--        <div class="categories-box">--}}
{{--            <div class="categories-title">--}}
{{--                <h2>Categorias destacadas</h2>--}}
{{--            </div>--}}

{{--            @include('frontoffice.components.product_category_list', ['categories' => $popularProductCategories])--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
{{--<!-- End Categories Area -->--}}

{{--@include('frontoffice.components.bulletin')--}}

<!-- Start Blog Area -->
<section class="blog-area pt-100 pb-100">
    <div class="container">
        <div class="section-title">
            <h2>Últimas notas del blog</h2>
            <p>Entérese de noticias, tips y novedades relacionadas con el sector. </p>
        </div>

        @include("frontoffice.components.blog_content_list", ["posts" => $posts])
    </div>
</section>
<!-- End Blog Area -->

@stop
