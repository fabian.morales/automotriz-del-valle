@extends('frontoffice.layouts.app')

@section('content')

@component('frontoffice.components.banner_area')
    @slot('title')
    {{ $category->name }}
    @endslot

    @slot('class')
    products
    @endslot

    @slot('styles')
    style="background-image: url({{ asset($category->getImageUrl()) }})"
    @endslot
@endcomponent

<!-- Start Shop Area -->
<section class="shop-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="shop-category">
                    <div class="category-title">
                        <a href="#">Categor&iacute;as</a>
                    </div>

                    <div class="shop-category-menu">
                        <ul class="category-list">
                            @foreach($categories as $cat)
                                <li><a href="{{ route("front::product::category", ["id" => $cat->id, "slug" => $cat->slug]) }}">{{ $cat->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                @php
                /*<div class="shop-sidebar">
                    <div class="brand-sidebar-item">
                        <h3>Brand</h3>

                        <ul class="brand-input-checkbox">
                            <li class="checkbox">
                                <input type="checkbox" class="input">
                                <label class="label">Brandix</label>
                            </li>

                            <li class="checkbox">
                                <input type="checkbox" class="input">
                                <label class="label">Red Gate</label>
                            </li>

                            <li class="checkbox">
                                <input type="checkbox" class="input">
                                <label class="label">Sunset</label>
                            </li>

                            <li class="checkbox">
                                <input type="checkbox" class="input">
                                <label class="label">Spector</label>
                            </li>

                            <li class="checkbox">
                                <input type="checkbox" class="input">
                                <label class="label">TransTech</label>
                            </li>

                            <li class="checkbox">
                                <input type="checkbox" class="input">
                                <label class="label">Turbo Electric</label>
                            </li>
                        </ul>
                    </div>

                    <div class="discount-sidebar-item">
                        <h3>With Discount</h3>

                        <ul class="discount-input-checkbox">
                            <li class="checkbox">
                                <input type="checkbox" class="input">
                                <label class="label">Any</label>
                            </li>

                            <li class="checkbox">
                                <input type="checkbox" class="input">
                                <label class="label">No</label>
                            </li>

                            <li class="checkbox">
                                <input type="checkbox" class="input">
                                <label class="label">Yes</label>
                            </li>
                        </ul>
                    </div>
                </div>*/
                @endphp
            </div>
            <div class="col-lg-9 col-md-12">
                @include("frontoffice.components.simple_product_list", ["products" => $products])
            </div>
        </div>

        <div class="row">
        {!! $products->links() !!}
        </div>
    </div>
</section>
<!-- End Shop Area -->

@stop