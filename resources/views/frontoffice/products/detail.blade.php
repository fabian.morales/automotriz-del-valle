@extends('frontoffice.layouts.app')

@section('js')
<script src="{{ asset("front/assets/js/lightslider/js/lightslider.min.js") }}"></script>
<script>
(($) => {
    $(() => {
        $(".carousel").lightSlider({
            item: 1,
            loop: true,
        });
    });
})(jQuery);
</script>
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset("front/assets/js/lightslider/css/lightslider.min.css") }}">
@endsection

@section('content')

@component('frontoffice.components.banner_area')
    @slot('title')
    {{ $product->name }}
    @endslot

    @slot('class')
    products
    @endslot
@endcomponent

<!-- Start Products Details Area -->
<section class="products-details-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="products-details-desc">
                    <div class="row align-items-center">
                        <div class="col-lg-7 col-md-6">
                            <div class="main-products-image">
                                <ul class="carousel">
                                @forelse($product->images as $image)
                                    <li><img src="{{ asset($image->getImageUrl('medium')) }}" alt="{{ $product->name }}"></li>
                                @empty
                                    <li><img src="{{ asset($product->getMainImage('thumbnail')) }}" alt="{{ $product->name }}"></li>
                                @endforelse
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-5 col-md-6">
                            <form method="post" id="quote_form" action="{{ route('front::cart::add') }}">
                                <input type="hidden" id="producto_id" name="product_id" value="{{ $product->id }}" />
                                <input type="hidden" id="combination_hash" name="combination_hash" value="" />
                                <div class="product-content">
                                    <h3>{{ $product->name }}</h3>

                                    <div class="product-review">
                                        <div class="rating">
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                        </div>
                                    </div>

                                    @php
                                    /*<div class="price">
                                        <span class="old-price">$150.00</span>
                                        <span class="new-price">$75.00</span>
                                    </div>*/
                                    @endphp
                                    <p>
                                    @if (!empty($product->categories))

                                    @foreach ($product->categories as $category)
                                    <a href="{{ route('front::product::category', ['id' => $category->id, 'slug' => $category->slug]) }}">{{ $category->name }}</a>
                                    @if(!$loop->last)
                                    <span>|</span>
                                    @endif
                                    @endforeach

                                    @endif
                                    </p>

                                    <ul class="products-info">
                                        <!--li><span>Availability:</span> <a href="#">In stock</a></li-->
                                        <li><span>SKU:</span> <a href="#">{{ $product->sku }}</a></li>
                                    </ul>

                                    @if(!empty($product->attributes))

                                    @foreach($product->attributes as $attribute)
                                        @if ($attribute->type == 'color')
                                        @php
                                            $selectedValue = "";
                                        @endphp
                                        <div class="products-color-switch">
                                            <p class="available-color"><span>{{ $attribute->name }}</span> :
                                                @foreach ($attribute->productValues as $value)
                                                    @if($loop->first)
                                                        @php
                                                        $selectedValue = $value->id;
                                                        @endphp
                                                    @endif
                                                    <a href="#" data-id="{{ $value->id }}" rel="color-switch" data-input="attribute_switch_{{ $attribute->id }}" style="background: {{ $value->value }};" @if($loop->first) class="active" @endif></a>
                                                @endforeach
                                                <input id="attribute_switch_{{ $attribute->id }}" type="hidden" name="attribute_value[{{ $attribute->id }}]" value="{{ $selectedValue }}" />
                                            </p>
                                        </div>
                                        @elseif ($attribute->type == 'radio')
                                        <div class="products-color-switch">
                                            <p class="available-color"><span>{{ $attribute->name }}</span> :
                                                @foreach ($attribute->productValues as $value)
                                                <label>{{ $value->label }} <input id="attribute_radio_{{ $attribute->id }}_{{ $value->id }}" type="radio" value="{{ $value->id }}" name="attribute_value[{{ $attribute->id }}]" @if($loop->first) checked @endif /></label>
                                                @endforeach
                                            </p>
                                        </div>
                                        @else
                                        <div class="products-color-switch">
                                            <p class="available-color"><span>{{ $attribute->name }}</span> :
                                                <select id="attribute_select_{{ $attribute->id }}" name="attribute_value[{{ $attribute->id }}]">
                                                @foreach ($attribute->productValues as $value)
                                                    <option value="{{ $value->id }}">{{ $value->label }}</option>
                                                @endforeach
                                                </select>
                                            </p>
                                        </div>
                                        @endif
                                    @endforeach

                                    @endif

                                    <div class="product-quantities">
                                        <span>Cantidad:</span>

                                        <div class="input-counter">
                                            <span class="minus-btn">
                                                <i class='bx bx-minus'></i>
                                            </span>
                                            <input type="text" name="quantity" value="1" min="1">
                                            <span class="plus-btn">
                                                <i class='bx bx-plus'></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="product-add-to-cart">
                                        <button type="submit" class="default-btn" id="btnQuote">
                                            <i class="flaticon-shopping-cart"></i>
                                            A&ntilde;adir
                                            <span></span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="products-details-tabs">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description">Descripci&oacute;n</a></li>
                        @if(!empty($product->productFeatures) && sizeof($product->productFeatures))
                        <li class="nav-item"><a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews">Ficha t&eacute;cnica</a></li>
                        @endif
                        <!--li class="nav-item"><a class="nav-link" id="information-tab" data-toggle="tab" href="#information" role="tab" aria-controls="information">Shipping Information</a></li-->
                    </ul>

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="description" role="tabpanel">
                            <h3>Descripci&oacute;n</h3>
                            <br />
                            {!! $product->description !!}
                        </div>

                        @if(!empty($product->productFeatures) && sizeof($product->productFeatures))
                        <div class="tab-pane fade" id="reviews" role="tabpanel">
                            <div class="products-reviews">
                                <h3>Ficha t&eacute;cnica</h3>
                                <ul>
                                @foreach ($product->productFeatures as $pFeature)
                                    <li><strong>{{ $pFeature->feature->name }}</strong>: {{ $pFeature->value }}</li>
                                @endforeach
                                </ul>
                                @php
                                /*<div class="row">
                                    <div class="side">
                                        <div>5 star</div>
                                    </div>
                                    <div class="middle">
                                        <div class="bar-container">
                                            <div class="bar-5"></div>
                                        </div>
                                    </div>
                                    <div class="side right">
                                        <div>70%</div>
                                    </div>
                                    <div class="side">
                                        <div>4 star</div>
                                    </div>
                                    <div class="middle">
                                        <div class="bar-container">
                                            <div class="bar-4"></div>
                                        </div>
                                    </div>
                                    <div class="side right">
                                        <div>20%</div>
                                    </div>
                                    <div class="side">
                                        <div>3 star</div>
                                    </div>
                                    <div class="middle">
                                        <div class="bar-container">
                                            <div class="bar-3"></div>
                                        </div>
                                    </div>
                                    <div class="side right">
                                        <div>5%</div>
                                    </div>
                                    <div class="side">
                                        <div>2 star</div>
                                    </div>
                                    <div class="middle">
                                        <div class="bar-container">
                                            <div class="bar-2"></div>
                                        </div>
                                    </div>
                                    <div class="side right">
                                        <div>3%</div>
                                    </div>
                                    <div class="side">
                                        <div>1 star</div>
                                    </div>
                                    <div class="middle">
                                        <div class="bar-container">
                                            <div class="bar-1"></div>
                                        </div>
                                    </div>
                                    <div class="side right">
                                        <div>2%</div>
                                    </div>
                                </div>*/
                                @endphp
                            </div>

                            @php
                            /*<div class="products-review-form">
                                <h3>Customer Reviews</h3>

                                <div class="review-title">
                                    <div class="rating">
                                        <i class='bx bxs-star'></i>
                                        <i class='bx bxs-star'></i>
                                        <i class='bx bxs-star'></i>
                                        <i class='bx bxs-star'></i>
                                        <i class='bx bxs-star'></i>
                                    </div>

                                    <a href="#" class="default-btn">
                                        Write a Review
                                        <span></span>
                                    </a>
                                </div>

                                <div class="review-comments">
                                    <div class="review-item">
                                        <div class="rating">
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                        </div>
                                        <h3>Good</h3>
                                        <span><strong>Admin</strong> on <strong>Sep 21, 2019</strong></span>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                                    </div>

                                    <div class="review-item">
                                        <div class="rating">
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                        </div>
                                        <h3>Good</h3>
                                        <span><strong>Admin</strong> on <strong>Sep 21, 2019</strong></span>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                                    </div>

                                    <div class="review-item">
                                        <div class="rating">
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                        </div>
                                        <h3>Good</h3>
                                        <span><strong>Admin</strong> on <strong>Sep 21, 2019</strong></span>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                                    </div>
                                </div>

                                <div class="review-form">
                                    <h3>Write a Review</h3>

                                    <form>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <input type="text" id="name" name="name" placeholder="Enter your name" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <input type="email" id="email" name="email" placeholder="Enter your email" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12">
                                                <div class="form-group">
                                                    <input type="text" id="review-title" name="review-title" placeholder="Enter your review a title" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12">
                                                <div class="form-group">
                                                    <textarea name="review-body" id="review-body" cols="30" rows="6" placeholder="Write your comments here" class="form-control"></textarea>
                                                </div>
                                            </div>

                                            <div class="col-lg-12 col-md-12">
                                                <button type="submit" class="default-btn">
                                                    Submit Review
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>*/
                            @endphp
                        </div>
                        @endif

                        @php
                        /*<div class="tab-pane fade show" id="information" role="tabpanel">
                            <ul class="information-list">
                                <li>Address: <span>4848 Hershell Hollow Road, Bothell, WA 89076</span></li>
                                <li>Phone: <a href="tel:+15143214567">+1 (514) 321-4567</a></li>
                                <li>Email: <a href="mailto:hello@maxon.com">hello@maxon.com</a></li>
                            </ul>
                        </div>*/
                        @endphp
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-12">
                <aside class="widget-area">
                    <!--section class="widget widget_search">
                        <h3 class="widget-title">Search</h3>

                        <form class="search-form">
                            <label>
                                <span class="screen-reader-text">Search for:</span>
                                <input type="search" class="search-field" placeholder="Search...">
                            </label>
                            <button type="submit">
                                <i class='bx bx-search-alt'></i>
                            </button>
                        </form>
                    </section-->

                    @if(!empty($categories))
                    <section class="widget widget_categories">
                        <h3 class="widget-title">Categor&iacute;as</h3>

                        <ul>
                            @foreach($categories as $category)
                            <li>
                                <a href="{{ route('front::product::category', ['id' => $category->id, 'slug' => $category->slug]) }}">{{ $category->name }} <i class='bx bx-chevron-right'></i></a>
                            </li>
                            @endforeach
                        </ul>
                    </section>
                    @endif

                    @if(!empty($popularPosts) && sizeof($popularPosts))
                        @include('frontoffice.components.blog_content_aside_list', ["posts" => $popularPosts])
                    @endif
                </aside>
            </div>
        </div>
    </div>
</section>
<!-- End Products Details Area -->

@if(!empty($popularProducts) && sizeof($popularProducts))
<!-- Start Top Products Area -->
<section class="top-products-area bg-color pt-100 pb-70">
    <div class="container">
        <div class="section-title">
            <h2>Productos populares</h2>
            <p>Encuentre aquí todo lo que necesita para el sistema de aire acondicionado para la línea automotriz (Vehículos de carga pesada, Maquinaria Agrícola, Volquetas, Camiones, Camionetas).</p>
        </div>

        @include('frontoffice.components.simple_product_list', ['products' => $popularProducts])

    </div>
</section>
<!-- End Top Products Area -->
@endif

@endsection

@section('modals')
@php
/*<div id="modal-quote-container" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-quote-title">Cotizar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="quote_form" action="{{ route('front::order') }}" method="post">
                    <input type="hidden" id="producto_id" name="product_id" value="{{ $product->id }}" />
                    <input type="hidden" id="combination_hash" name="combination_hash" value="" />
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 col-md-6 form-group">
                            <!--label for="nombre" class="col-form-label-sm">Nombre</label-->
                            <input type="text" id="name" name="name" placeholder="Nombre" class="form-control" data-error="Debes ingresar tu nombre" required />
                        </div>
                        <div class="col-sm-12 col-md-6 form-group">
                            <!--label for="ciudad" class="col-form-label-sm">Ciudad</label-->
                            <input type="text" id="city_name" name="city_name" placeholder="Ciudad" class="form-control" data-error="Debes ingresar la ciudad" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-6 form-group">
                            <!--label for="correo" class="col-form-label-sm">Correo</label-->
                            <input type="email" id="email" name="email" placeholder="Correo" class="form-control" data-error="Debes ingresar tu correo" required />
                        </div>
                        <div class="col-sm-12 col-md-6 form-group">
                            <!--label for="celular" class="col-form-label-sm">Celular</label-->
                            <input type="text" id="phone" name="phone" placeholder="Celular" class="form-control" data-error="Debes ingresar tu número de celular" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <!--label for="mensaje" class="col-form-label-sm">Mensaje</label-->
                            <textarea id="message" name="message" placeholder="Mensaje" rows="5" class="form-control"  data-error="Debes ingresar el mensaje" required></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label for="whatsapp" class="col-form-label-sm"><input type="checkbox" name="whatsapp" id="whatsapp" value="Y" /> &iquest;Usas WhatsApp?</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label for="accept_terms" class="col-form-label-sm"><input type="checkbox" name="accept_terms" id="accept_terms" value="Y"  data-error="Debes aceptar los términos y condiciones" required /> Aceptar <a href="{{ route('front::tyc') }}" target="_blank">t&eacute;rminos y condiciones</a></label>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <a id="btnSendQuote" class="btn btn-primary btn-cotizacion btn-icon-split float-left" href="">
                            <span class="icon text-white-50">
                                <i class="fas fa-paper-plane"></i>
                            </span>
                            <span class="text">Enviar</a>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>*/
@endphp
@stop
