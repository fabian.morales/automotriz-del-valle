@extends('frontoffice.layouts.app')

@section('content')

@component('frontoffice.components.banner_area')
    @slot('title')
    B&uacute;squeda
    @endslot

    @slot('class')
    products
    @search
@endcomponent

<!-- Start Shop Area -->
<section class="shop-area ptb-100">
    <div class="container">
        @include("frontoffice.components.simple_product_list", ["products" => $products])
        <div class="row">
            {!! $products->links() !!}
        </div>
    </div>
</section>
<!-- End Shop Area -->

@stop