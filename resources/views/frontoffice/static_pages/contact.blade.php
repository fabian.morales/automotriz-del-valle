@extends('frontoffice.layouts.app')

@section('content')

@component('frontoffice.components.banner_area')
    @slot('title')
    Cont&aacute;ctanos
    @endslot

    @slot('class')
    contact
    @endslot
@endcomponent

<!-- Start Contact Info Area -->
<section class="contact-info-area pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="contact-info-box">
                    <div class="icon">
                        <i class='bx bx-envelope'></i>
                    </div>

                    <h3>Email Here</h3>
                    <p><a href="mailto:hello@maxon.com">hello@maxon.com</a></p>
                    <p><a href="mailto:support@maxon.com">support@maxon.com</a></p>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="contact-info-box">
                    <div class="icon">
                        <i class='bx bx-map'></i>
                    </div>

                    <h3>Location Here</h3>
                    <p>175 5th Ave Premium Area, New York, NY 10010, United States</p>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="contact-info-box">
                    <div class="icon">
                        <i class='bx bxs-phone-call'></i>
                    </div>

                    <h3>Call Here</h3>
                    <p><a href="tel:1234567890">+123 456 7890</a></p>
                    <p><a href="tel:2414524526">+241 452 4526</a></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Contact Info Area -->

<!-- Start Contact Area -->
<section class="contact-area pb-100">
    <div class="container">
        <div class="section-title">
            <h2>Ready To Get Started?</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>

        <div class="contact-form">
            <form id="contactForm" class="captcha-form" method="post" action="{{ route("front::contact::post") }}">
                @csrf
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" name="name" class="form-control" required data-error="Debes ingresar tu nombre">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <label>Correo</label>
                            <input type="email" name="email" id="email" class="form-control" required data-error="Debes ingresar tu correo">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <label>Tel&eacute;fono</label>
                            <input type="text" name="phone" id="phone" required data-error="Debes ingresar tu tel&eacute;fono" class="form-control">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <label>Asunto</label>
                            <input type="text" name="subject" id="subject" class="form-control" required data-error="Debes ingresar el asunto">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <div class="form-group">
                            <label>Mensaje</label>
                            <textarea name="message" class="form-control" id="message" cols="30" rows="6" required data-error="Desbes escribir tu mensaje"></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <button type="submit" class="default-btn captcha-submit" id="btnSendContact">
                            Enviar
                        </button>

                        @if(App::environment(['staging', 'production']))
                            {!! Captcha::renderButton() !!}
                        @endif

                        <div id="msgSubmit" class="h3 text-center hidden"></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- End Contact Area -->

<!-- Start Map Area -->
<div id="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3022.9476519598093!2d-73.99185268459418!3d40.74117737932881!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c259a3f81d549f%3A0xb2a39bb5cacc7da0!2s175%205th%20Ave%2C%20New%20York%2C%20NY%2010010%2C%20USA!5e0!3m2!1sen!2sbd!4v1588746137032!5m2!1sen!2sbd"></iframe>
</div>
<!-- End Map Area -->

@stop
