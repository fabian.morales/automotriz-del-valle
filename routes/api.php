<?php

use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SearchController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::any('/categories', [ProductCategoryController::class, 'index'])->name('api::category::list');
Route::get('/products', [ProductController::class, 'index'])->name('api::product::list');
Route::get('/categories/{id}-{slug}/products', [ProductCategoryController::class, 'showProducts'])->name('api::product::category');
Route::any('/search/products', [SearchController::class, 'productNames'])->name('front::product_names');

