<?php

use App\Http\Controllers\Admin\AttributeController;
use App\Http\Controllers\Admin\BlogCategoryController;
use App\Http\Controllers\Admin\BlogContentController as AdminBlogContentController;
use App\Http\Controllers\Admin\BulletinController as AdminBulletinController;
use App\Http\Controllers\Admin\ContactController as AdminContactController;
use App\Http\Controllers\Admin\FeatureController;
use App\Http\Controllers\Admin\GalleryController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\OrderController as AdminOrderController;
use App\Http\Controllers\Admin\PageContentController as AdminPageContentController;
use App\Http\Controllers\Admin\ProductCategoryController as AdminProductCategoryController;
use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\BlogContentController;
use App\Http\Controllers\BulletinController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PageContentController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\StaticPageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('front::home');
Route::get('/nosotros', [StaticPageController::class, 'showAboutUs'])->name('front::aboutus'); 
Route::get('/terminos-y-condiciones', [StaticPageController::class, 'showTerms'])->name('front::tyc'); 
Route::get('/politica-de-privacidad', [StaticPageController::class, 'showPrivacyPolicy'])->name('front::privacy_policy'); 
Route::get('/contacto', [ContactController::class, 'showForm'])->name('front::contact'); 
Route::post('/contacto', [ContactController::class, 'save'])->name('front::contact::post'); 

Route::post('/order', [OrderController::class, 'registerOrder'])->name('front::order');

Route::group(['prefix' => 'carrito'], function () {
    Route::any('/add', [CartController::class, 'add'])->name('front::cart::add');
    Route::any('/update', [CartController::class, 'update'])->name('front::cart::update');
    Route::post('/delete/{id}', [CartController::class, 'delete'])->name('front::cart::delete');
    Route::get('/list', [CartController::class, 'list'])->name('front::cart::list');
});

Route::post('/newsletter', [BulletinController::class, 'save'])->name('front::bulletin');
Route::any('/search', [SearchController::class, 'search'])->name('front::search::products');

//Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name("logout");

Route::group(['prefix' => 'productos'], function () {
    Route::get('/{id}-{slug}', [ProductCategoryController::class, 'showProducts'])->name('front::product::category');
    Route::get('/{categoryId}-{categorySlug}/{id}-{slug}', [ProductController::class, 'showProduct'])->name('front::product');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function () {
    Route::get('/', function () {
        return view('backoffice.pages.home');
    })->name('admin::index');

    Route::group(['prefix' => 'feature'], function() {
        Route::get('/', [FeatureController::class, 'list'])->name('admin::feature::list');
        Route::get('/create', [FeatureController::class, 'showForm'])->name('admin::feature::create');
        Route::get('/edit/{id}', [FeatureController::class, 'edit'])->name('admin::feature::edit');
        Route::post('/save', [FeatureController::class, 'save'])->name('admin::feature::save');
        Route::get('/delete/{id}', [FeatureController::class, 'delete'])->name('admin::feature::delete');
    });

    Route::group(['prefix' => 'attribute'], function() {
        Route::get('/', [AttributeController::class, 'list'])->name('admin::attribute::list');
        Route::get('/create', [AttributeController::class, 'showForm'])->name('admin::attribute::create');
        Route::get('/edit/{id}',[AttributeController::class, 'edit'])->name('admin::attribute::edit');
        Route::post('/save', [AttributeController::class, 'save'])->name('admin::attribute::save');
        Route::get('/delete/{id}', [AttributeController::class, 'delete'])->name('admin::attribute::delete');

        Route::get('/{id}/values', [AttributeController::class, 'getValuesList'])->name('admin::attribute_value::list');
        Route::get('/values/{id}', [AttributeController::class, 'showValue'])->name('admin::attribute_value::show');
        Route::post('/values/save', [AttributeController::class, 'saveValue'])->name('admin::attribute_value::save');
        Route::get('/values/delete/{id}', [AttributeController::class, 'deleteValue'])->name('admin::attribute_value::delete');
    });

    Route::group(['prefix' => 'product'], function() {
        Route::get('/', [AdminProductController::class, 'list'])->name('admin::product::list');
        Route::get('/create', [AdminProductController::class, 'showForm'])->name('admin::product::create');
        Route::get('/edit/{id}', [AdminProductController::class, 'edit'])->name('admin::product::edit');
        Route::post('/save', [AdminProductController::class, 'save'])->name('admin::product::save');
        Route::get('/delete/{id}', [AdminProductController::class, 'delete'])->name('admin::product::delete');
        Route::post('/images/{id}', [AdminProductController::class, 'saveImage'])->name('admin::product::add_image')->where('id', '[0-9]+');
        Route::post('/images/{id}/delete', [AdminProductController::class, 'deleteImages'])->name('admin::product::delete_image');
        Route::get('/images/{id}/regenerate', [AdminProductController::class, 'generateProductImages'])->name('admin::product::gen_image')->where('id', '[0-9]+');
        Route::get('/images/regenerate', [AdminProductController::class, 'generateAllProductsImages'])->name('admin::product::all_gen_image');
        Route::get('/massive', [AdminProductController::class, 'showMassiveUploadForm'])->name('admin::product::massive');
        Route::post('/massive/upload', [AdminProductController::class, 'massiveProductUpload'])->name('admin::product::massive_upload');

        Route::get('/{id}/combinations', [AdminProductController::class, 'getCombinationList'])->name('admin::product_combination::list');
        Route::post('/combinations/save', [AdminProductController::class, 'saveCombination'])->name('admin::product_combination::save');
        Route::get('/combinations/delete/{id}', [AdminProductController::class, 'deleteCombination'])->name('admin::product_combination::delete');

        Route::group(['prefix' => 'category'], function() {
            Route::get('/', [AdminProductCategoryController::class, 'list'])->name('admin::product_category::list');
            Route::get('/{id}', [AdminProductCategoryController::class, 'list'])->name('admin::product_category::show')->where('id', '[0-9]+');
            Route::get('/create', [AdminProductCategoryController::class, 'showForm'])->name('admin::product_category::create');
            Route::get('/edit/{id}', [AdminProductCategoryController::class, 'edit'])->name('admin::product_category::edit');
            Route::post('/save', [AdminProductCategoryController::class, 'save'])->name('admin::product_category::save');
            Route::get('/delete/{id}', [AdminProductCategoryController::class, 'delete'])->name('admin::product_category::delete');
        });
    });

    Route::group(['prefix' => 'page-content'], function() {
        Route::get('/', [AdminPageContentController::class, 'list'])->name('admin::page_content::list');
        Route::get('/create', [AdminPageContentController::class, 'showForm'])->name('admin::page_content::create');
        Route::get('/edit/{id}', [AdminPageContentController::class, 'edit'])->name('admin::page_content::edit');
        Route::post('/save', [AdminPageContentController::class, 'save'])->name('admin::page_content::save');
        Route::get('/delete/{id}', [AdminPageContentController::class, 'delete'])->name('admin::page_content::delete');
    });

    Route::group(['prefix' => 'blog-content'], function() {
        Route::group(['prefix' => 'category'], function() {
            Route::get('/', [BlogCategoryController::class, 'list'])->name('admin::blog_category::list');
            Route::any('/data', [BlogCategoryController::class, 'dataTable'])->name('admin::blog_category::data');
    
            Route::get('/create', [BlogCategoryController::class, 'create'])->name('admin::blog_category::create');
            Route::post('/store', [BlogCategoryController::class, 'store'])->name('admin::blog_category::store');
    
            Route::get('/edit/{id}', [BlogCategoryController::class, 'edit'])->name('admin::blog_category::edit');
            Route::post('/update/{id}', [BlogCategoryController::class, 'update'])->name('admin::blog_category::update');

            Route::get('/delete/{id}', [BlogCategoryController::class, 'delete'])->name('admin::blog_category::delete');
        });

        Route::get('/', [AdminBlogContentController::class, 'list'])->name('admin::blog_post::list');
        Route::any('/data', [AdminBlogContentController::class, 'dataTable'])->name('admin::blog_post::data');

        Route::get('/create', [AdminBlogContentController::class, 'create'])->name('admin::blog_post::create');
        Route::post('/store', [AdminBlogContentController::class, 'store'])->name('admin::blog_post::store');

        Route::get('/edit/{id}', [AdminBlogContentController::class, 'edit'])->name('admin::blog_post::edit');
        Route::post('/update/{id}', [AdminBlogContentController::class, 'update'])->name('admin::blog_post::update');

        Route::get('/delete/{id}', [AdminBlogContentController::class, 'delete'])->name('admin::blog_post::delete');
    });

    Route::group(['prefix' => 'order'], function() {
        Route::get('/', [AdminOrderController::class, 'list'])->name('admin::order::list');
        Route::get('/edit/{id}', [AdminOrderController::class, 'edit'])->name('admin::order::edit');
        Route::post('/save', [AdminOrderController::class, 'save'])->name('admin::order::save');
        Route::get('/delete/{id}', [AdminOrderController::class, 'delete'])->name('admin::order::delete');
    });

    Route::group(['prefix' => 'gallery'], function() {
        Route::get('/', [GalleryController::class, 'list'])->name('admin::gallery::list');
        Route::get('/create', [GalleryController::class, 'showForm'])->name('admin::gallery::create');
        Route::get('/edit/{id}', [GalleryController::class, 'edit'])->name('admin::gallery::edit');
        Route::post('/save', [GalleryController::class, 'save'])->name('admin::gallery::save');
        Route::get('/delete/{id}', [GalleryController::class, 'delete'])->name('admin::gallery::delete');

        Route::group(['prefix' => 'photo'], function() {
            Route::post('/save', [GalleryController::class, 'savePhoto'])->name('admin::gallery_photo::save');
            Route::get('/edit/{id}', [GalleryController::class, 'editPhoto'])->name('admin::gallery_photo::edit');
            Route::get('/delete/{id}', [GalleryController::class, 'deletePhoto'])->name('admin::gallery_photo::delete');
        });
    });

    Route::group(['prefix' => 'menu'], function() {
        Route::get('/', [MenuController::class, 'list'])->name('admin::menu::list');
        Route::any('/data', [MenuController::class, 'dataTable'])->name('admin::menu::data');

        Route::get('/create', [MenuController::class, 'create'])->name('admin::menu::create');
        Route::post('/store', [MenuController::class, 'store'])->name('admin::menu::store');

        Route::get('/edit/{id}', [MenuController::class, 'edit'])->name('admin::menu::edit');
        Route::post('/update/{id}', [MenuController::class, 'update'])->name('admin::menu::update');

        Route::get('/delete/{id}', [MenuController::class, 'delete'])->name('admin::menu::delete');
    });

    Route::group(['prefix' => 'bulletin'], function() {
        Route::get('/', [AdminBulletinController::class, 'list'])->name('admin::bulletin::list');
        Route::get('/download', [AdminBulletinController::class, 'downloadList'])->name('admin::bulletin::download');
    });

    Route::group(['prefix' => 'contact'], function() {
        Route::get('/', [AdminContactController::class, 'list'])->name('admin::contact::list');
        Route::get('/download', [AdminContactController::class, 'downloadList'])->name('admin::contact::download');
    });
});

Route::group(['prefix' => 'blog'], function () {
    Route::get('/', [BlogContentController::class, 'showHome'])->name('front::blog::home');
    Route::get('/categoria/{id}-{slug}', [BlogContentController::class, 'showCategory'])->name('front::blog::category');
    Route::get('/articulo/{id}-{slug}', [BlogContentController::class, 'showPost'])->name('front::blog::post');
});

Route::any('/{key}', [PageContentController::class, 'showPage'])->name('front::page_content'); 
