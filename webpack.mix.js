const mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.postCss('resources/css/app.css', 'public/front/assets/css', [
    require('postcss-purgecss-laravel')({
        safelist: [
            'lSSlideOuter',
            'lSPager',
            'lSpg',
            'btn-loading',
            'products-color-switch',
            'nice-select',
            'modal-backdrop',
            'twitter-typeahead',
            'tt-menu',
            'tt-suggestion',
            'tt-selectable',
            'owl-loaded',
            'owl-drag',
            'owl-stage-outer',
            'owl-stage',
            'owl-item',
            'owl-nav',
            'owl-prev',
            'owl-next',
            'owl-dots',
            'owl-dot',
            'item',
            'mean-bar',
            'mean-container',
            'meanmenu-reveal',
            'meanclose',
            'mean-nav',
            'mean-expand',
            'mean-clicked',
        ],
    }),
    require('postcss-combine-media-query'),
    require('postcss-extract-media-query')({
        extractAll: false,
        output: {
            path: path.join(__dirname, 'public/front/assets/css'),
            name: 'app-[query].[ext]',
        },
        queries: {
            'only screen and (max-width: 300px)': 'mobile',
            'only screen and (max-width: 399px)': 'mobile',
            'only screen and (max-width: 499px)': 'mobile',
            'only screen and (max-width: 599px)': 'mobile',
            'only screen and (max-width: 767px)': 'mobile',
            'screen and (max-width: 767px)': 'mobile',
            'only screen and (max-width: 768px)': 'mobile',
            '(min-width: 768px)': 'desktop',
            'only screen and (min-width: 768px)': 'desktop',
            'all and (min-width: 768px)': 'desktop',
            'only screen and (max-width: 960px)': 'desktop',
            'only screen and (max-width: 991px)': 'desktop',
            'only screen and (min-width: 992px)': 'desktop',
            '(min-width: 992px)': 'desktop',
            'only screen and (max-width: 1023px)': 'desktop',
            'only screen and (max-width: 1140px)': 'desktop',
            'only screen and (min-width: 1200px)': 'desktop',
            '(min-width: 1200px)': 'desktop',
            'only screen and (max-width: 1200px)': 'desktop',
            'print': 'print',
        },
    }),
    require('cssnano')({
        preset: ['default', {discardComments: { "removeAll": true }}],
    }),
 ]
)
.options({
 processCssUrls: false
});
